clear all
%
% General input per module
[par] = wave_input;
[par] = flow_input(par);
[par] = sed_input(par);
% Grid and bathymetry
[s] = grid_bathy;
% Directional distribution wave energy
[s] = wave_dist (s,par);
% Initialisations
[s,par] = wave_init (s,par);
[s] = flow_init (s,par);
[s] = sed_init (s);

nt=par.nt;

for it=1:nt;
 %
    % Wave boundary conditions
    [s] = wave_bc (s,par,it);
    % Flow boundary conditions
    [s] = flow_bc (s,par,it);
    % Wave timestep
    [s] = wave_timestep (s,par);
    % Flow timestep
    [s] = flow_timestep (s,par);
    % Suspended transport
    [s]=transus(s,par);
    % Bed level update
    [s]=bed_update(s,par);
    % Output
    output(it,s,par)
   

end