function [s]=wave_timestep(s,par);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Copyright (C) 2007 UNESCO-IHE, WL|Delft Hydraulics and Delft University %
% Dano Roelvink, Ap van Dongeren, Dirk-Jan Walstra, Ad Reniers            %
%                                                                         %
% d.roelvink@unesco-ihe.org                                               %
% UNESCO-IHE Institute for Water Education                                %
% P.O. Box 3015                                                           %
% 2601 DA Delft                                                           %
% The Netherlands                                                         %
%                                                                         %
% This library is free software; you can redistribute it and/or           %
% modify it under the terms of the GNU Lesser General Public              %
% License as published by the Free Software Foundation; either            %
% version 2.1 of the License, or (at your option) any later version.      %
%                                                                         %
% This library is distributed in the hope that it will be useful,         %
% but WITHOUT ANY WARRANTY; without even the implied warranty of          %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        %
% Lesser General Public License for more details.                         %
%                                                                         %
% You should have received a copy of the GNU Lesser General Public        %
% License along with this library; if not, write to the Free Software     %
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307     %
% USA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Input
%
Tm01   = par.Tm01;       
gamma  = par.gamma;
alpha  = par.alpha;
n      = par.n;
delta  = par.delta;      % fraction of wave height to add to water depth in dispersion 
rhog8  = par.rhog8;
wci    = par.wci;
dt     = par.dt;
dx     = s.dx;
dy     = s.dy;
dtheta = s.dtheta;
ntheta = s.ntheta;
costh  = s.costh;
sinth  = s.sinth;
h      = max(s.h,par.hmin);
e      = s.e;
Fx     = s.Fx;
Fy     = s.Fy;
cgx    = s.cgx;
cgy    = s.cgy;
ctheta = s.ctheta;
thet   = s.thet;
sigt   = s.sigt;
H      = s.H;
% 
%  flow velocities for wave current interaction 
%
if wci
   uu = s.uu;
   vv = s.vv;
else
   uu = zeros(size(s.uu));  % no wci
   vv = zeros(size(s.vv));  % no wci
end
%
% Dispersion relation
%
sigm = max(mean(sigt,3),0.01);
[k,c,cg]=dispersion(h+delta*H,2*pi./sigm);
%
% Slopes of water depth
%
[dhdx,dhdy] = slope (h+delta*H,dx,dy);
[dudx,dudy] = slope (uu,dx,dy);
[dvdx,dvdy] = slope (vv,dx,dy);
%
% Propagation speeds in x,y and theta space
%
    for itheta=1:ntheta
        cgx(:,:,itheta)=cg*costh(itheta)+uu;
        cgy(:,:,itheta)=cg*sinth(itheta)+vv;
        ctheta(:,:,itheta)= ...
         sigm./sinh(2.*k.*(h+delta*H)).*(dhdx*sinth(itheta)-dhdy*costh(itheta)) + ...
         costh(itheta).*(sinth(itheta).*dudx - costh(itheta).*dudy) + ...
         sinth(itheta).*(sinth(itheta).*dvdx - costh(itheta).*dvdy);
    end    
    
%
% transform to wave action
%
e = e./sigt;
%
% Upwind Euler timestep propagation
%
e=e-dt*(advecx(e.*cgx)/dx+advecy(e.*cgy)/dy+advectheta(e.*ctheta)/dtheta);
e=max(e,0.00001);
%
% transform back to wave energy
%
e = e.*sigt;
%
% Energy integrated over wave directions,Hrms
%
E=sum(e,3)*dtheta;
H=sqrt(E/rhog8);
%
% calculate change in intrinsic frequency

    tm = mean(e.*thet,3)./mean(e,3);
    km = k;
    kmx = km.*cos(tm);
    kmy = km.*sin(tm);
    wm = sigm+kmx.*uu+kmy.*vv;
    kmx = kmx -dt*advecwx(wm)./dx;
    kmx(:,s.ny+1) = kmx(:,s.ny);   % lateral bc

    kmy = kmy -dt*advecwy(wm)./dy;
%   kmy = kmy +dx*advecwy(kmx)/dy;  % based on irrotational wave vector field (less stable)
    kmy(:,s.ny+1) = kmy(:,s.ny);   % lateral bc

    km = sqrt(kmx.^2+kmy.^2);
    sigm = sqrt(par.g.*km.*tanh(km.*(h+delta*H)));
    
    for itheta=1:ntheta
     sigt(:,:,itheta) = max(sigm,0.01);
    end
%
% Total dissipation
if par.break=='B1998';
    [D,Qb,Hb]=baldock(h+delta*H,Tm01,k,E,gamma);
elseif par.break=='R1993';
    [D,Qb]=roelvink(h+delta*H,Tm01,E,gamma,alpha,n);
else
    par.break
end
%
% Distribution of dissipation over directions and frequencies
%
 for itheta=1:ntheta;
    d(:,:,itheta)=e(:,:,itheta).*D./E;
 end
%
wet=h+delta*H>par.hmin;
for itheta=1:ntheta
    wete(:,:,itheta)=wet;
end
%
% Euler step dissipation
%
e(wete)=e(wete)-dt*d(wete);
e=max(e,0.00001);
e(~wete)=0.00001;
%
% Compute mean wave direction
%
thetamean=mean(e.*thet,3)./mean(e,3);
%
%
% Energy integrated over wave directions,Hrms
%
E=sum(e,3)*dtheta;
H=sqrt(E/rhog8);
%
% Radiation stresses and forcing terms
%
n=cg./c;
Sxx=(n.*sum((1+(cos(thet)).^2).*e,3)-.5*sum(e,3))*dtheta;
Syy=(n.*sum((1+(sin(thet)).^2).*e,3)-.5*sum(e,3))*dtheta;
Sxy=n.*sum(sin(thet).*cos(thet).*e,3)*dtheta;

Fx(1:end-1,2:end-1)=-(Sxx(2:end,2:end-1)-Sxx(1:end-1,2:end-1))/dx - ...
    .25/dy*(Sxy(1:end-1,3:end)   + Sxy(2:end,3:end) - ...
    Sxy(1:end-1,1:end-2) - Sxy(2:end,1:end-2));
Fy(2:end-1,1:end-1)=-(Syy(2:end-1,2:end)-Syy(2:end-1,1:end-1))/dy - ...
    .25/dx*(Sxy(3:end,1:end-1)   + Sxy(3:end,2:end) - ...
    Sxy(1:end-2,1:end-1) - Sxy(1:end-2,2:end));
Fx(:,1)=Fx(:,2);
Fy(:,1)=Fy(:,2);
Fx(:,end)=Fx(:,end-1);
Fy(:,end)=Fy(:,end-1);
urms=2*pi*H/Tm01./sinh(2*k.*h)/sqrt(2);
%ust=E.*k./sigm./par.rho./h;
ust=E.*k./sigm./par.rho./max(h,.001);

%
% Output
%
s.k         = k;
s.cg        = cg;
s.c         = c;
s.e         = e;
s.E         = E;
s.H         = H;
s.urms      = urms;
s.thetamean = thetamean;
s.D         = D;
s.Fx        = Fx;
s.Fy        = Fy;
s.sigt      = sigt;
s.ust       = ust;
s.tm        = tm;