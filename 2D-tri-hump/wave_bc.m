function [par,s] = wave_bc (s,par,it);
Tlong     = par.Tlong;
Llong     = par.Llong;
dt        = par.dt;
dx        = s.dx;
dy        = s.dy;
ny        = s.ny;
ntheta    = s.ntheta;
y         = s.y;
e         = s.e;
thetamean = s.thetamean;
e01       = s.e01;
% added for bound long wave comp Ad 28 march 2006
dtheta    = par.dtheta*pi/180;
g         = par.g;
theta0    = par.theta0;
h         = s.h;
c         = s.c;
cg        = s.cg;
t         = par.t;

for j=1:ny+1;
    e(1,j,:)=e01;  % constant wave energy
end


instat=0;
if it>1
    %
    % Lateral boundary at y=0;
    %
    if instat
        fac1=dy*abs(tan(thetamean(2:end,2)))/dx;
    else
        fac1=0;
    end
    fac1=min(fac1,1);fac1=max(fac1,0);
    fac2=1-fac1;
       for itheta=ntheta/2:ntheta
       e(2:end,1,itheta)=e(1:end-1,2,itheta).*fac1+e(2:end,2,itheta).*fac2;
      end
    %
    % lateral; boundary at y=ny*dy
    %
    if instat
        fac1=dy*abs(tan(thetamean(2:end,end-1)))/dx;
    else
        fac1=0;
    end
    fac1=min(fac1,1);fac1=max(fac1,0);
    fac2=1-fac1;
     for itheta=1:ntheta/2
        e(2:end,end,itheta)= ...
            e(1:end-1,end-1,itheta).*fac1+e(2:end,end-1,itheta).*fac2;
     end
end
s.e   = e;
s.ui  = 0;