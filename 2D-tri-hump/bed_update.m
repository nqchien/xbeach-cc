function [s]=bed_update(s,par);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Copyright (C) 2007 UNESCO-IHE, WL|Delft Hydraulics and Delft University %
% Dano Roelvink, Ap van Dongeren, Dirk-Jan Walstra, Ad Reniers            %
%                                                                         %
% d.roelvink@unesco-ihe.org                                               %
% UNESCO-IHE Institute for Water Education                                %
% P.O. Box 3015                                                           %
% 2601 DA Delft                                                           %
% The Netherlands                                                         %
%                                                                         %
% This library is free software; you can redistribute it and/or           %
% modify it under the terms of the GNU Lesser General Public              %
% License as published by the Free Software Foundation; either            %
% version 2.1 of the License, or (at your option) any later version.      %
%                                                                         %
% This library is distributed in the hope that it will be useful,         %
% but WITHOUT ANY WARRANTY; without even the implied warranty of          %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        %
% Lesser General Public License for more details.                         %
%                                                                         %
% You should have received a copy of the GNU Lesser General Public        %
% License along with this library; if not, write to the Free Software     %
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307     %
% USA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update bed level using continuity eq.
zb=s.zb;
h=s.h;
zs=s.zs;
Su=s.Su;
Sv=s.Sv;
dx=s.dx;
dy=s.dy;
nx=s.nx;
ny=s.ny;
dzmax=1;
hmin=par.hmin;
sedero=s.sedero;
dt=par.dt;
dzbdt=zeros(size(zb));
morfac = par.morfac;
wetu=s.wetu;
%
dzbdt(2:nx,2:ny)=morfac*(-(Su(2:nx,2:ny)-Su(1:nx-1,2:ny))/dx ...
    -(Sv(2:nx,2:ny)-Sv(2:nx,1:ny-1))/dy);
zb(2:nx,2:ny)=zb(2:nx,2:ny)+dzbdt(2:nx,2:ny)*dt;
sedero(2:nx,2:ny)=sedero(2:nx,2:ny)+dzbdt(2:nx,2:ny)*dt;
%
% bed boundary conditions
%
zb(:,1) = zb(:,2);
sedero(:,1) = sedero(:,2);
zb(:,ny+1) = zb(:,ny);
sedero(:,ny+1) = sedero(:,ny);
%
% Avalanching
%
dzbx=zeros(size(zb));
dzby=zeros(size(zb));
dzbx(1:end-1,:)=(zb(2:end,:)-zb(1:end-1,:))/dx;
dzby(:,1:end-1)=(zb(:,2:end)-zb(:,1:end-1))/dy;
for j=1:ny+1
    for i=2:nx-1;
        if h(i,j)>0.1;
            dzmax=0.3;
        else
            dzmax=1;
        end           
        if abs(dzbx(i,j))>dzmax;
            dzb=sign(dzbx(i,j))*(abs(dzbx(i,j))-dzmax)*dx;
            dzb=sign(dzb)*min(abs(dzb),0.005);
            zb(i,j)  =zb(i,j)+dzb;
            sedero(i,j) = sedero(i,j) + dzb;
            zs(i,j)=zs(i,j)+dzb;
            zb(i+1,j)=zb(i+1,j)-dzb;
            sedero(i+1,j)=sedero(i+1,j)-dzb;
            zs(i+1,j)=zs(i+1,j)-dzb;
        end
    end
end
for i=1:nx
    for j=2:ny-1;
        if h(i,j)>0.1;
            dzmax=0.3;
        else
            dzmax=1;
        end           
        if abs(dzby(i,j))>dzmax
            dzb=sign(dzby(i,j))*(abs(dzby(i,j))-dzmax)*dx;
            dzb=sign(dzb)*min(abs(dzb),0.005);
            zb(i,j)  =zb(i,j)+dzb;
            sedero(i,j) = sedero(i,j) + dzb;
            zs(i,j)=zs(i,j)+dzb;
            zb(i,j+1)=zb(i,j+1)-dzb;
            sedero(i,j+1)=sedero(i,j+1)-dzb;
            zs(i,j+1)=zs(i,j+1)-dzb;
        end
    end
end

s.zb=zb;
s.sedero=sedero;
s.zs=zs;
s.h=h;