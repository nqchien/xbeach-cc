function [s]= sb_vr(s,par);
%
% Soulsby van Rijn sediment transport formula
% Ad Reniers april 2006
%
% z is defined positive upward 
% x is defined positive toward the shore
%  Formal parameters:
%  ------------------
%
%   Var. I/O  Type Dimensions
%   -------------------------
%
D50    = par.D50;
D90    = par.D90;
h      = max(s.h,par.hmin);
u      = s.ue;   % use eulerian velocities
v      = s.ve;   % use eulerian velocities
vmag2  = u.^2+v.^2;
urms2  = s.urms.^2;
rhos   = par.rhos; 
C      = par.C;
g      = par.g;

% calculate treshold velocity Ucr

dster=25296.*D50;

if (D50<=0.0005) 
  Ucr=0.19.*D50.^0.1.*log10(4.*h./D90);
elseif (D50<0.002) 
  Ucr=8.5*D50.^0.6.*log10(4.*h./D90);
else
  disp(' D50 > 2mm, out of validity range')
  return
end
      
% drag coefficient
z0 = 0.006;
Cd=(0.40./(log(h/z0)-1.)).^2;
%Cd = g/C.^2;   % consistent with flow modelling 

% transport parameters

Asb=0.005*h.*(D50./h/(1.65*g.*D50)).^1.2;                 % bed load coefficent
Ass=0.012.*D50*dster^(-0.6)/(1.65*g.*D50)^1.2;          % suspended load coeffient
term1=(vmag2  + 0.018./Cd.*urms2).^0.5;                  % nearbed-velocity
      
[it] = find(term1>Ucr );
term2 = zeros(size(term1));
term2(it) = (term1(it)-Ucr(it)).^2.4;
ceq =(Asb+Ass).*term2;   
% add steetzels effect
%cs = max(0.01*(s.D./par.rho).^(2/3)./h./h,0.);
%cs = max(0.0001*(s.D./par.rho),0.);
ceq = min(ceq,.2);% equilibrium concentration
%figure(3)
%plot(s.x,ceq,'r')
%pause(.1)

% add slope effect
    [dhdx,dhdy] = slope(-s.zb,s.dx,s.dy);

% combine into single slope 

    uandv = abs(u) + abs(v);
    iu = find(uandv>0.01);
    b = zeros(size(uandv));
    b(iu) = u(iu)./uandv(iu).*dhdx(iu)+v(iu)./uandv(iu).*dhdy(iu);
   
    fslope = max(0,1+10.*b);

%ceq = ceq.*fslope;
s.ceq = ceq./h;

