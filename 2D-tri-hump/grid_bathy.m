function [s] = grid_bathy
h0=20;
hmin = 0.001;
nx=50;
ny=25;
dx=40;
dy=100;
%
% Hump dimensions
%
height=3;
R=300;
x0=1700;
y0=1200;
%
% Set original water depth
%
slope = 0.01
for j=1:ny+1;
    for i=1:nx+1;
        x(i,j)=(i-1)*dx;
        y(i,j)=(j-1)*dy;
%       h(i,j)=max(h0*(1-(i-1)/nx),.1);
        % zb(i,j)=-h0*(1-(i-1)/(nx-5));
        zb(i,j)= -h0 + (i - 1) * dx * slope;
    end;
end
%h=h-height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
zb=zb+height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
% zb(:,15:40) = 2;
%zb=zb+height.*exp(-((x-x0).^2)/R^2);
% check with horizontal bottom
% h=max(h,h0);
%
% Output
%
s.x    = x;
s.y    = y;
s.dx   = dx;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb    = zb;