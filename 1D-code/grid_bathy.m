function [s, sh, ZBINIT] = grid_bathy(par)

h0 = 4;
hmin = 0.001;
nx = 40;
ny = 40;
s.dxStandard = 10.;

dx = zeros(nx+1,ny+1) + s.dxStandard;     % dx is now an array

dy = 10;

% shore_shape = input('Shape of shore - Straight (default), [H]umped, [G]roined : ', 's');
% bank_elev = input('Bank elevation? [A number] / Inf (default) for none: ');
shore_shape = '';
bank_elev = Inf;

%
% Set original water depth
%
% Pre-allocation
x = zeros(nx+1,ny+1);
y = zeros(nx+1,ny+1);
zb = zeros(nx+1,ny+1);
a = ones(nx+1, ny+1);

% Slope
slope_t = 0.01;	% transversal slope
for j=1:ny+1;
    for i=1:nx+1;
        x(i,j) = (i-1) * dx(i,j);          % a = 1
        y(i,j) = (j-1) * dy;
        % h(i,j)=max(h0*(1-(i-1)/nx),.1);
        % zb(i,j)=-h0*(1-(i-1)/(nx-5));
		zb(i,j) = -h0 + x(i,j) * slope_t;
    end;
end

if strcmp(upper(shore_shape), 'G');
% Groin (rectangular)
	highest = zb(end,1);
	zb (21:30, 19:21) = 0.5;
end;

zb(31:end,:) = 0.5;
ysh = 0:dy:ny*dy;
xsh = zeros(size(ysh)) + 300 - s.dxStandard/2;
iiwet = zeros(1,ny+1)+30;
iidry = zeros(1,ny+1)+31;

if 0
% if strcmp(upper(shore_shape), 'H');
	% Hump dimensions
	% height=3;
	% R=180;
	% x0=200;
	% y0=500;
	% h=h-height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	% zb=zb+height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	% zb=zb+height.*exp(-((x-x0).^2)/R^2);
	xbase = 300;
	hhump = 50;
	k = 3 * dy;
	ymid = (ny * dy) / 2;
	ysh = 0:dy:ny*dy;
	xsh = xbase - hhump * exp(-(ysh - ymid).^2 / k^2);
% 	disp(xsh);
	for j = 1:ny+1
		for i = 1:nx
			if x(i,j) > xsh(j)
				zb(i,j) = 0.5;
			else
				zb(i,j) = -1 - (xsh(j) - x(i,j)) * slope_t;
			end
        end
        idx = floor(xsh(j) / s.dxStandard);
        s.dx(idx,j) = mod(xsh(j), s.dxStandard);
	end
% end;
end


% check with horizontal bottom
% h=max(h,h0);

% if bank_elev ~= Inf;
	% zb(31:end,:) = bank_elev; 	% bank
	% zb(:,18:22) = -5; trench
% end;

% Longitudinal slope
zb = zb - par.i * y;
ZBINIT = zb;
% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.a    = a;

% Initial position of shoreline (additional code by Chien)
% Best to be predefined
% Here we take the intersection between bed and water surface
% THIS CODE USES INITIAL ZS, WHICH IS NOT OFFICIALLY DEFINED

if 0
yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
% yy = yy - s.dy/2;

xx = [];           % cross-shore coordinates, can vary
iiwet = [];     % list of positions of wet cells
ny = s.ny;
for i = 1 : ny+1
    % simple approximation
%     iwet = numel(find(zb(:,i) < par.zs0));
%     xi = x(iwet+1,i);
    % interpolation
    le = 1; 	ri = ny;
    while zb(le,i)<0; le=le+1; end;
    while zb(ri,i)>0; ri=ri-1; end;
    xi = interp1(zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0 - par.i * s.y(1,i)); % account for slope
    iwet = length(find(zb(:,i)<0 - par.i * s.y(1,i)));       % index of wet cell = number of cells with zb < zs

    xx = horzcat(xx, [xi]);
    iiwet = horzcat(iiwet, [iwet]);
end
iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward
end

% figure(1); clf; plot(xx, yy, 'g*-', 'linewidth',2);

% Setting up the sh struct
% sh is the structure containing data of shoreline particularly

% % For the hydrodynamic part only;
% % triangular hump
% xx = [100 100 100 100 100 100 90 80 70 60 50 60 70 80 90 100 100 100 100 100 100]; 
% xx = xx - s.dxStandard/2;
% these are spatial properties of shoreline

% % for hump
% sh.xL0 = [];	sh.yL0 = [];
% for i = 1:s.ny;
	% sh.xL0 = horzcat(sh.xL0, [xx(i) xx(i)]);
	% sh.yL0 = horzcat(sh.yL0, [yy(i) yy(i+1)]);
% end;

% for island 
% sh.xL0 = [5 6 6 7 7 8 8 7 7 6 6 5 5] * s.dxStandard - 3 * s.dxStandard/2;
% sh.yL0 = [9 9 8 8 9 9 12 12 14 14 12 12 9] * s.dy - 3 * s.dy / 2;

% Alternately, we can define the coastline coordinates sh.x here

% for interpolated shoreline
% sh.x0 = xx;
% sh.y0 = yy;
% sh.x = xx;
% sh.y = yy;

% for predefined shoreline
sh.x0 = xsh;
sh.y0 = ysh;
sh.x = xsh;
sh.y = ysh;
sh.xhump = [];
sh.vhump = [];
sh.iiwet = iiwet;
sh.iidry = iidry;

% End added