function [a] = read_data(filename, ncols);

% read_data(filename, ncols);
% read an ASCII file named *filename* with *ncols* columns of number.

if strcmp(filename, ''); filename = 'max_retreat.txt'; end;
if ncols < 1; ncols = 1; end;
fid = fopen(filename);
str = '';
for i = 1:ncols; str = [str '%g']; end;
a = fscanf(fid, str);
N = length(a) / ncols;
a = reshape(a, ncols, N)';
