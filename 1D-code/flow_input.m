function [par] = flow_input(par);
par.C=65;
par.eps=1e-1;
par.g=9.81;
par.umin=.1;
par.zs0=0;
par.tstart=0;
par.tint=1;
par.tstop=2450;
par.CFL = 0.4;

% Added
par.v0 = 1.;
par.i = 1.E-4;
