classdef TestTauC < matlab.unittest.TestCase
    %TestTauC Test the function tau_c 
    %   to calculate the shear stress of mixed soil
    %   following Julian and Torres (2006) Geomorphology 76
    
    properties
        Property1
    end
    
    methods (Test)
        function test_tau_c_SC_zero(testCase)
            %test_tau_c_SC_zero Test the case SC=0
            %   i.e. for pure sand
            tau_c_calc = tau_c(0);
            tau_c_expt = 0.1;
            testCase.verifyEqual(tau_c_calc, tau_c_expt, 'AbsTol', 1e-5);
        end

        function test_tau_c_SC_medium(testCase)
            %test_tau_c_SC_max Test the case SC = 50 percent
            %   i.e. for silt-clay sample
            tau_c_calc = tau_c(50);
            tau_c_expt = 13.07;
            testCase.verifyEqual(tau_c_calc, tau_c_expt, 'AbsTol', 1e-5);
        end
        
        function test_tau_c_SC_max(testCase)
            %test_tau_c_SC_max Test the case SC = 100 percent
            %   i.e. for silt-clay sample
            tau_c_calc = tau_c(100);
            tau_c_expt = 22.49;
            testCase.verifyEqual(tau_c_calc, tau_c_expt, 'AbsTol', 1e-5);
        end
    end
end

