function [it,s]=output(it,s,par,sh)

if exist('OCTAVE_VERSION') == 0    % running in Matlab
	figure(1);
	xplot = s.x - s.dxStandard / 2;
	yplot = s.y - s.dy/2.;
	pcolor(xplot, yplot, s.zb); shading flat; 
	% contour(s.x,yplot,s.zb); 
	hold on;
% 	plot(sh.x0, sh.y0, 'k');
% 	plot(sh.xL, sh.yL, 'k'); 
    if mod(par.t, 30)==0
    	plot(sh.x, sh.y, 'k');
    end
	% caxis([-5 0]);colorbar;
	axis equal; % axis tight;
	% axis ([345 400 520 550]);
	% axis ([220 360 300 700]);
	% axis ([270 410 300 700]);
	% axis tight;
% 	hold on;
% 	quiver(s.x,s.y,s.u,s.v);
% 	drawnow;
% 	hold off;
	% the velocity field changes, so hold off has to be used so that
	% new velocities will not overlap old ones
	drawnow;
	% fprintf('%3d %6.1f%6.2f%6.1f%6.2f\n', par.t, sh.x(11), sh.v(11), sh.x(31), sh.v(31));
	% fprintf('%3d %6.1f %5.2f %6.2f%6.2f%6.2f%6.2f%6.2f%6.2f%6.2f\n', par.t, sh.x(21), sh.v(21), s.dx(35:41,21));
% 	if mod(par.t,10) == 0
% 		plot(sh.x, sh.y, 'k');
% 	end
	% plot(sh.x0, sh.y0, 'k', 'linewidth', 2);
else    % running in Octave
	disp('now in Octave');
end
