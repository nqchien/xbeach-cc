function [dhdx,dhdy] = slope (h,dx,dy,s)

dhdx=zeros(size(h));
dhdy=zeros(size(h));

dhdx(2:end-1,:) = (h(3:end,:) - h(1:end-2,:))/2 ./ dx(2:end-1,:);
dhdx(1,:) = (h(2,:) - h(1,:)) ./ dx(1,:);
dhdx(end,:) = (h(end,:) - h(end-1,:)) ./ dx(end,:);

dhdy(:,2:end-1) = (h(:,3:end) - h(:,1:end-2))/2/dy;
dhdy(:,1) = (h(:,2) - h(:,1)) / dy;
dhdy(:,end) = (h(:,end) - h(:,end-1)) / dy;
