function [par] = flow_input(par)
% Specifies the flow parameters for the simulation
% Input: `par`, the parameter structure
% Returns: `par`, the modified parameter structure
% 
par.C = 70;
par.eps = 1e-1;
par.g = 9.81;
par.umin = .1;
par.zs0 = -0.4;
par.CFL = 0.5;
par.v0 = 1.;
par.i = 1E-3;
