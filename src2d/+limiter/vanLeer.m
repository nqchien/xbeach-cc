function ret = vanLeer(a,b);
% Implement the van Leer limiter for fluxes
% a and b are the left and right fluxes
% ret is the van Leer limiter

eps = 1E-6;
a = a + eps;
b = b + eps;
ret = (a .* abs(b) +  abs(a) .* b) ./ (abs(a) + abs(b));
