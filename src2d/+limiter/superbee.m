function ret = superbee(a, b);
% Implement the superbee limiter

s = sign(b);
ret = s .* max(min(2*abs(b), s .* a), min(abs(b), 2 * s .* a));
ret = max(0, ret);
