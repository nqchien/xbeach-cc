function ret = hyperbee(a, b);
% Implements the hyperbee limiter for flux calculation.

s = sign(b);
ret = 2 * s * max(0, min(abs(b), s * a));
