function ret = minmod(a, b);
% Implement the minmod limiter for fluxes
%
s = sign(b);
ret = s .* max(0, min(abs(b), s .* a));
