function [s,par] = wave_init(s,par)
hmin     = par.hmin;
zs0      = par.zs0;
theta    = s.theta;
x        = s.x;
dx       = s.dx;
zb       = s.zb;
h        = max(zs0-s.zb,hmin);
nx       = s.nx;
ny       = s.ny;
ntheta   = s.ntheta;
Tlong    = par.Tlong;
theta0   = par.theta0;
Tm01     = par.Tm01;
g        = par.g;
%
% Initial condition
%
e = ones(nx+1,ny+1,ntheta)*0;
s.thetamean = zeros(size(x));
s.Fx        = zeros(size(x));
s.Fy        = zeros(size(x));
s.H         = zeros(size(x));
s.cgx       = zeros(size(e));
s.cgy       = zeros(size(e));
s.ctheta    = zeros(size(e));
thet        = zeros(size(e));
sigt        = zeros(size(e));
% added for bound long wave bc Ad 27 march 2006
ui          = zeros(size(x(1,:)));

for itheta=1:ntheta
    thet(:,:,itheta) = theta(itheta);
end

% introduce intrinsic frequencies for wave action

for itheta=1:ntheta
    sigt(:,:,itheta) = 2*pi/Tm01;
end

sigm = mean(sigt,3);
[k,c,cg]=dispersion(h,2*pi./sigm);

if theta0==0
    theta0=1e-6
end
Llong=Tlong*cg(1,1)/sin(theta0);
s.e       = e;
s.thet    = thet;
s.sigt    = sigt;
s.k       = k;
s.c       = c;
s.cg      = cg;
par.Llong = Llong; 
s.sigm    = sigm;
par.dt    = 0.8/sqrt(g*max(max(h)));
