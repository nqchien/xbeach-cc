function [xG,yG] = centroid_tri(cA, cB, cC);

xA = cA(1);	yA = cA(2);
xB = cB(1);	yB = cB(2);
xC = cC(1);	yC = cC(2);

xG = (xA + xB + xC) / 3;
yG = (yA + yB + yC) / 3;
