function [par] = wave_input()
% Specify parameters for wave modelling
% Returns: par (global structure)
% 
par.Hrms  = 1;
par.Tm01  = 8;
par.dir0  = -10;
par.m     = 4;
par.hmin  = 0.001;
par.Tlong = 31.6228;
par.gamma = .65;
par.alpha = 1.;
par.n     = 10;
par.delta = 0.0;
par.rho   = 1025;
par.g     = 9.81;
par.rhog8 = 1/8*par.rho*par.g;
par.omega = 2*pi/par.Tm01;
par.sigma = par.omega;
par.thetamin = -90;
par.thetamax = 90;
par.dtheta   = 10;
par.theta0   = par.dir0*pi/180;
par.wci      = 0;
par.break    = 'R1993'; % or 'B1998';
