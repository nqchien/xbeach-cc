% Main routine xbeach
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Copyright (C) 2007 UNESCO-IHE, WL|Delft Hydraulics and Delft University %
% Dano Roelvink, Ap van Dongeren, Dirk-Jan Walstra, Ad Reniers            %
%                                                                         %
% d.roelvink@unesco-ihe.org                                               %
% UNESCO-IHE Institute for Water Education                                %
% P.O. Box 3015                                                           %
% 2601 DA Delft                                                           %
% The Netherlands                                                         %
%                                                                         %
% (GNU GPL License)                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all

% General input per module
[par] = wave_input;
par.scenario = "BEND";   % alternative: "BEND"
[par] = flow_input(par);
[par] = sed_input(par);

[s] = grid_bathy(par);      % Grid and bathymetry
[s] = wave_dist(s,par);    % Directional distribution wave energy

% Initialisations
[s, par] = wave_init(s, par);
[s] = flow_init(s, par);
[s, par] = sed_init(s, par);

par.tstart = 0;
par.tint = 2;
par.tstop = 30;

par.nt = 60;
par.t = 0;
par.dt = 0.2;
par.wave = false;       % Disable wave computation

par.tnext = par.tint;
it = 0;

while par.t < par.tstop
    [par,s] = wave_bc(s,par,it);   % Wave boundary conditions
    [s] = flow_bc(s,par);          % Flow boundary conditions
    if par.wave
        [s] = wave_timestep(s, par);
    end
    [s, par] = flow_timestep(s, par);
    if mod(par.t, par.tint) == 0
        output(it, par, s);
    end
end
plotting.grid_flow_field()
