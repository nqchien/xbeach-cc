function [s,par] = flow_timestep(s,par) 
%
% Flow solver accounting for the presence of cut cells
% Input: 
%	`par`: a parameter struct
% 	`s`: structure with grid parameters
% Returns:
%   `par` and `s` modified
%
    x       = s.x;
    y       = s.y;
    a       = s.a;
    dx      = s.dx;
	dx0	    = s.dxStandard;
    dy      = s.dy;
    uu      = s.uu;
    vv      = s.vv;
    qx      = s.qx;
    qy      = s.qy;
    zb      = s.zb;
    zs      = s.zs;
    Fx      = s.Fx;
    Fy      = s.Fy;
    delta_u = s.delta_u;
    delta_v = s.delta_v;
    type    = s.type;
    Th      = s.Th;
	eps = 1E-4;
    INT_MAX = intmax();
	
    % add mass flux
	ust = zeros(size(s.h));
	tm = zeros(size(s.h));
    
    g = par.g;
    C = par.C;
    rho = par.rho;
    umin = par.umin;
    hmin = par.hmin;
    eps = par.eps;    
    t = par.t;
    tint = par.tint;
    tnext = par.tnext;
    CFL = par.CFL;

    vu = zeros(size(x));
    vsu = zeros(size(x));
    usu = zeros(size(x));
    vsv = zeros(size(x));
    usv = zeros(size(x));
    uv = zeros(size(x));
    vmagu = zeros(size(x));
    vmague = zeros(size(x));
    vmagv = zeros(size(x));
    vmagve = zeros(size(x));
    veu = zeros(size(x));
    ueu = zeros(size(x));
    vev = zeros(size(x));
    uev = zeros(size(x));

    u       =zeros(size(x));
    v       =zeros(size(x));
    dzsdx   =zeros(size(x));
    dzsdy   =zeros(size(x));
	dzsdt = zeros(size(x));
    mx      =zeros(size(x));
    mx1     =zeros(size(x));
    mx2     =zeros(size(x));
    my      =zeros(size(x));
    my1     =zeros(size(x));
    my2     =zeros(size(x));
    f1      =zeros(size(x));
    f2      =zeros(size(x));
    ududx   =zeros(size(x));
    vdudy   =zeros(size(x));
    udvdx   =zeros(size(x));
	vdvdy   =zeros(size(x));
	qxm     =zeros(size(x));
	qym     =zeros(size(x));
	us      =zeros(size(x));
	vs      =zeros(size(x));
	ue      =zeros(size(x));
	ve      =zeros(size(x));

    nx = size(x,1)-1;
    ny = size(x,2)-1;

	join = zeros(nx+1,ny+1);
	enlarged = zeros(nx+1,ny+1);
    
	% Momentum eq

    % V-velocities at u-points
    vu(1:nx,2:ny) = (vv(1:nx,1:ny-1)+vv(1:nx,2:ny)+ ...
        vv(2:nx+1,1:ny-1)+vv(2:nx+1,2:ny))./(abs(sign(vv(1:nx,1:ny-1))) +...
        abs(sign(vv(1:nx,2:ny))) + abs(sign(vv(2:nx+1,1:ny-1))) + ...
        abs(sign(vv(2:nx+1,2:ny)))+eps);
    % how about boundaries?
	vu(:,1) = vu(:,2);
	vu(:,ny+1) = vu(:,ny);
	
    if ~par.wave  % waves are not accounted for
		ust(:) = 0; 
		Fx(:) = 0;
		Fy(:) = 0;
	end
    % V-stokes velocities at U point
    vsu(1:nx,2:ny)=0.5*(ust(1:nx,2:ny).*sin(tm(1:nx,2:ny))+...
        ust(2:nx+1,2:ny).*sin(tm(2:nx+1,2:ny)));
    vsu(:,1)=vsu(:,2);
    vsu(:,ny+1) = vsu(:,ny);
    % U-stokes velocities at U point
    usu(1:nx,2:ny)=0.5*(ust(1:nx,2:ny).*cos(tm(1:nx,2:ny))+...
        ust(2:nx+1,2:ny).*cos(tm(2:nx+1,2:ny)));
    usu(:,1)=usu(:,2);
    usu(:,ny+1)=usu(:,ny);
    
    veu = vu - vsu; % V-euler velocities at u-point
    ueu = uu - usu; % U-euler velocties at u-point
    vmagu = sqrt(uu.^2 + vu.^2); % Velocity magnitude at u-points
    vmageu = sqrt(ueu.^2 + veu.^2); % Eulerian velocity magnitude at u-points

    % U-velocities at v-points
    uv(2:nx,1:ny) = (uu(1:nx-1,1:ny)+uu(2:nx,1:ny)+ ...
        uu(1:nx-1,2:ny+1)+uu(2:nx,2:ny+1))./(abs(sign(uu(1:nx-1,1:ny)))+...
        abs(sign(uu(2:nx,1:ny))) + abs(sign(uu(1:nx-1,2:ny+1)))+...
        abs(sign(uu(2:nx,2:ny+1)))+eps);
    uv(1,:) = uv(2,:);      % boundary
    uv(:,ny+1) = uv(:,ny);  % boundary

     % V-stokes velocities at V point
    vsv(2:nx,1:ny) = 0.5*(ust(2:nx,1:ny).*sin(tm(2:nx,1:ny))+...
        ust(2:nx,2:ny+1).*sin(tm(2:nx,2:ny+1)));
    vsv(:,1) = vsv(:,2);
    vsv(:,ny+1) = vsv(:,ny);
    % U-stokes velocities at V point
    usv(2:nx,1:ny) = 0.5*(ust(2:nx,1:ny).*cos(tm(2:nx,1:ny))+...
        ust(2:nx,2:ny+1).*cos(tm(2:nx,2:ny+1)));
    usv(:,1) = usv(:,2);
    usv(:,ny+1) = usv(:,ny);

    
    vev = vv - vsv; % V-euler velocities at V-point
    uev = uv - usv; % U-euler velocties at V-point
    vmagv = sqrt(uv.^2 + vv.^2);    % Velocity magnitude at v-points
    vmagev = sqrt(uev.^2 + vev.^2); % Eulerian velocity magnitude at v-points
	
    % Water level slopes
    dzsdx(1:nx,:) = (zs(2:nx+1,:)-zs(1:nx,:))./(x(2:nx+1,:)-x(1:nx,:));
    dzsdy(:,1:ny) = (zs(:,2:ny+1)-zs(:,1:ny))./(y(:,2:ny+1)-y(:,1:ny));
	
	% Upwind method implemented through weight factors mx1 and mx2
    % Water depth
    h = zs-zb;
    h = max(h,hmin);
    weth = h>hmin;
	du = zeros(size(uu));
	du(1:end-1,:) = delta_u(2:end,:);
	du(end,:) = du(end-1,:);
    % X-direction
    mx = sign(uu);
    mx(abs(uu)<umin) = 0;
    mx1 = (mx+1)/2;
    mx2 = -(mx-1)/2;
    f1(1:nx,:) = h(1:nx,:);
    f2(1:nx,:) = h(2:nx+1,:);
    % Water depth in u-points for continuity equation: upwind
	hu = zeros(nx+1,ny+1);
    hu = mx1.*f1 + mx2.*f2;
	hu(nx+1,:) = h(nx+1,:);	% Eastern end
    % Water depth in u-points for momentum equation: mean
    hum = max(0.5*(f1+f2), hmin);
    
    % Advection terms (momentum conserving method)
	f1 = f1 * 0; % initialization
    f2 = f2 * 0;
	qx = uu .* hu;
	
    f1(2:nx,:) = .5*(qx(2:nx,:)+qx(1:nx-1,:))...
        .* (uu(2:nx,:) - uu(1:nx-1,:)) ./ dx(2:nx,:);
    f2(1:nx-1,:) = f1(2:nx,:);
    ududx = (1./hum) .* (mx1.*f1 + mx2.*f2);
	
    a1 = (du(:,1:ny-1) ~= 0);
	a3 = (du(:,3:ny+1) ~= 0);
    vdudy(:,2:ny) = vu(:,2:ny) .* (a3.*(uu(:,3:ny+1)-uu(:,2:ny)) + ...
			a1.*(uu(:,2:ny)-uu(:,1:ny-1))) ./ ...
			((a1.*(du(:,1:ny-1)/2 + du(:,2:ny)/2) ...
			+ a3.*(du(:,2:ny)/2 + du(:,3:ny+1)/2)) ...
			* dy + eps);
    % Wetting and drying criterion (only for momentum balance)
    wetu = (hu>eps) & (du > 0);
    % Store velocity at inlet boundary (given by boundary condition)
    ur = uu(1,2:12);
    % Compute automatic timestep
	for i = 1:nx+1
        for j = 1:ny+1
            if Th(i,j) == 0
                Th(i,j) = INT_MAX;
            end
        end
    end
	dt = CFL * min(min(sqrt(Th * s.dxStandard * dy) ./ ...
			(max(vmagu,vmageu) + sqrt(g*h))));
	
    for i = 1:nx+1
        for j = 1:ny+1
            if Th(i,j) > 1000
                Th(i,j) = 0; 
            end
        end
    end
    t = t+dt;
    if t >= tnext
        dt = dt - (t - tnext);
        t = tnext;
        tnext = tnext + tint;
    end       
    
    % Explicit Euler step momentum u-direction
    
	xu = x;
    xu(1:end-1,:) = 0.5*(x(1:end-1,:) + x(2:end,:));
	advecu = zeros(nx+1,ny+1);
	fricu = zeros(nx+1,ny+1);
	gravu = zeros(nx+1,ny+1);
	waveu = zeros(nx+1,ny+1);
	advecu(wetu) = -ududx(wetu)-vdudy(wetu);
	gravu(wetu) = -g*dzsdx(wetu);
	fricu(wetu) = -g/C^2./hu(wetu).*vmageu(wetu).*ueu(wetu);
	waveu(wetu) = - Fx(wetu)/rho./hu(wetu);
	uu(wetu) = uu(wetu) - dt*(g*dzsdx(wetu)                       ...
                              + g/C^2./hu(wetu).*vmageu(wetu).*ueu(wetu) ...
                              - Fx(wetu)/rho./hu(wetu) );
	uu(~wetu) = 0;
	uu(end,:) = uu(end-1,:);
	
    uu(1,2:12) = ur;    % Restore inlet boundary condition
    
	% Y-direction
    
    my = sign(vv);
    my(abs(vv)<umin) = 0;
    my1 = (my+1)/2;
    my2 = -(my-1)/2;
    f1 = f1 * 0; 
    f2 = f2 * 0;
    f1(:,1:ny) = h(:,1:ny);
    f2(:,1:ny) = h(:,2:ny+1);
    % Water depth in v-points for continuity equation: upwind
    hv = my1.*f1 + my2.*f2;
    % Water depth in v-points for momentum equation: mean
    hvm = 0.5*(f1 + f2);
	
    % Advection terms (momentum conserving method)
	dv = zeros(size(vv));
	dv(:,1:end-1) = delta_v(:,2:end);
	dv(:,end) = dv(:,end-1);
	a3 = (dv(3:nx+1,:) ~= 0);
	a1 = (dv(1:nx-1,:) ~= 0);
    udvdx(2:nx,:) = uv(2:nx,:) .* (a3.*(vv(3:nx+1,:) - vv(2:nx,:)) + ...
                    a1.*(vv(2:nx,:) - vv(1:nx-1,:))) ./ ...
                    ((a1.*(dv(1:nx-1,:)/2 + dv(2:nx,:)/2) ...
                    + a3.*(dv(2:nx,:)/2 + dv(3:nx+1,:)/2)) ...
                    * dx0 + eps);
	f1 = f1*0;
    f2 = f2*0;
	qy = vv .* hv;
	
    f1(:,2:ny) = 0.5 * (qy(:,2:ny) + qy(:,1:ny-1))...
		.*(vv(:,2:ny) - vv(:,1:ny-1)) ./ dy;
    f2(:,1:ny-1) = f1(:,2:ny);
    vdvdy = my1 .* f1 + my2 .* f2;
    % Wetting and drying criterion (only for momentum balance)
    wetv = (hv>eps) & (dv > 0);
    
    % Explicit Euler step momentum v-direction
    
    advecv = zeros(nx+1,ny+1);
	fricv = zeros(nx+1,ny+1);
	gravv = zeros(nx+1,ny+1);
	wavev = zeros(nx+1,ny+1);
	advecv(wetv) = -udvdx(wetv) - vdvdy(wetv);
	gravv(wetv) = -g * dzsdy(wetv);
	fricv(wetv) = -g/C^2./hv(wetv).*vmagev(wetv).*vev(wetv);
	wavev(wetv) = - Fy(wetv)/rho./hv(wetv)  ;
	vv(wetv) = vv(wetv) - dt * (g*dzsdy(wetv)                         ...
                                + g/C^2./hv(wetv).*vmagev(wetv).*vev(wetv) ...
                                - Fy(wetv)/rho./hv(wetv) );
	vv(~wetv) = 0;
    vv(end,:) = vv(end-1,:);
    du = zeros(size(uu));
	dv = zeros(size(vv));
	du(1:end-1,:) = delta_u(2:end,:);
	du(end,:) = du(end-1,:);
	dv(:,1:end-1) = delta_v(:,2:end);
	dv(:,end) = dv(:,end-1);
	qx = uu .* hu .* du;
    qy = vv .* hv .* dv;
    
    % U and V in cell centre; for output and sediment stirring
    u(2:nx,:)=(.5 + .5 * (uu(1:nx-1,:).*uu(2:nx,:) == 0)).*(uu(1:nx-1,:)+uu(2:nx,:));
    u(1,:) = uu(1,:);
    v(:,2:ny)=(.5 + .5 * (vv(:,1:ny-1).*vv(:,2:ny)== 0)).*(vv(:,1:ny-1)+vv(:,2:ny));
    v(:,1) = vv(:,1);
    
    % Ue and Ve in cell centre; for output and sediment stirring
    ue(2:nx,:) = .5*(ueu(1:nx-1,:)+ueu(2:nx,:));
    ue(1,:) = ueu(1,:);
    ve(:,2:ny) = .5*(vev(:,1:ny-1)+vev(:,2:ny));
    ve(:,1) = vev(:,1);
    
    % Update water level using continuity eq.
    Thdzsdt = zeros(nx+1,ny+1);
	for i = 2 : nx
		for j = 2 : ny
			if Th(i, j) ~= 0
				Thdzsdt(i, j) = -(qx(i,j)-qx(i-1,j))./ dx(i,j) - ...
								(qy(i,j)-qy(i,j-1))/dy;
			end
		end
	end

	for i = 1:nx
		for j = 1:ny
			if (join(i,j) == 0) && (Th(i,j) ~= 0)
				if enlarged(i,j) == 4
					zs(i,j) = zs(i,j) + (Thdzsdt(i,j) + Thdzsdt(i,j-1)) ./ (Th(i,j)+Th(i,j-1)) * dt;
				elseif enlarged(i,j) == 2
					zs(i,j) = zs(i,j) + (Thdzsdt(i,j) + Thdzsdt(i,j+1)) ./ (Th(i,j)+Th(i,j+1)) * dt;
				else
					zs(i,j) = zs(i,j) + Thdzsdt(i,j) ./ Th(i,j) * dt;
				end
			end
		end
	end

    % Modify
    s.uu      = uu;
	s.vv      = vv; 
	s.ueu    = ueu;
	s.vev    = vev; 
	s.qx      = qx;
	s.qy      = qy; 
	s.vmagu   = vmagu; 
	s.vmagv   = vmagv;
	s.vmageu = vmageu;
	s.vmagev = vmagev;
	s.u       = u;
	s.v       = v; 
	s.ue      = ue;
	s.ve      = ve; 
	s.zs      = zs;
	s.dzsdx = dzsdx;
	s.dzsdy = dzsdy;
	s.Thdzsdt = Thdzsdt;
	s.join = join;
	s.enlarged = enlarged;
	s.advecu = advecu;
	s.advecv = advecv;
	s.fricu = fricu;
	s.fricv = fricv;
	s.gravu = gravu;
	s.gravv = gravv;
	s.waveu = waveu;
	s.wavev = wavev;
	s.ududx = ududx;
	s.vdudy = vdudy;
	s.udvdx = udvdx;
	s.vdvdy = vdvdy;
	s.qx = qx;
	s.qy = qy;
	s.hold    = h;
	s.h       = max(zs-zb,hmin);
	s.wetu    = wetu;
	s.wetv    = wetv;
	s.hu      = hu;
	s.hv      = hv;
	par.dt    = dt;
	par.t     = t;
	par.tnext =tnext;
end