function oops(N)

% OOPS delete the last object plotted on the axes.
% OOPs does not work for title and labels; to erase these,
% use "title('')" or "xlabel('')"

if nargin==0  N = 1; end
h = get(gca, 'children');
delete(h(1:N));