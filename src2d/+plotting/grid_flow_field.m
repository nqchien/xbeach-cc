% grid_flow_field.m
% Draw the grid and flow field

clf;
hold on;

xmin = -s.dxStandard / 2;
xmax = xmin + (s.nx+1) * s.dxStandard;
ymin = -s.dy / 2;
ymax = ymin + (s.ny+1) * s.dy;

% Horizontal grid
for i = xmin:s.dxStandard:xmax
    plot([i i], [ymin ymax], 'cyan');
end

% Vertical grid
for i = ymin:s.dy:ymax
    plot([xmin xmax], [i i], 'cyan'); 
end

xlabel('X (m)')
ylabel('Y (m)')
box on

if par.scenario == "CHANNEL"
    % Boundary / Diagonal channel
    xbound = -5:10:1005;
    ybound1 = (xbound + 5) * tan(22.8*pi/180) + 5;
    ybound2 = ybound1 + 100;
    plot(xbound, ybound1, 'linewidth', 2);
    plot(xbound, ybound2, 'linewidth', 2);
    quiver(s.x, s.y, s.u .* (s.zb < 0), s.v .* (s.zb < 0), 'b');
    axis equal tight;
elseif par.scenario == "BEND"
    margin = 9.5 * s.b;
    grSize = s.dy;
    xo = -grSize/2 : 1 : 150 - grSize/2;
    xi = -grSize/2 : 1 : 125 - grSize/2;
    plot(margin + xo, margin + sqrt(150^2 - (xo+grSize/2).^2) - grSize/2, ...
        'k', 'linewidth', 2);
    plot(margin + xi, margin + sqrt(125^2 - (xi+grSize/2).^2) - grSize/2, ...
        'k', 'linewidth', 2);
    quiver(s.x , s.y, s.u .* (s.zb < 0), s.v .* (s.zb < 0), 'b');
    axis equal;
    xlim([50 200]);
    ylim([50 200]);
end
