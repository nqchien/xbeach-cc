function [ret] = plot2y(t, x11, v11);

figure;
clf;
% subplot(211);
[ax1,x1,v1] = plotyy(t, x11, t, v11);
set(v1,'linestyle','--')
set(get(ax1(2),'Ylabel'),'String','v (m/s)')
set(get(ax1(1),'Ylabel'),'String','x (m)')
xlabel('Time (s)')
legend('x','location','northwest')
set(ax1(2),'YLim',[0 0.6])
set(ax1(1),'YTick',[350:5:380])
set(ax1(2),'YTick',[0:0.1:0.55])

% subplot(212);
% [ax2,x2,v2] = plotyy(t, x31, t, v31);
% set(v2,'linestyle','--')
% set(get(ax2(2),'Ylabel'),'String','v (m/s)')
% set(get(ax2(1),'Ylabel'),'String','x (m)')
% xlabel('Time (s)')
% legend('x','location','northwest')
% set(ax2(1),'YLim',[330 335])
% set(ax2(2),'YLim',[-0.1 0.1])
% set(ax2(1),'YTick',[330:1:335])
% set(ax2(2),'YTick',[-0.1 0 0.1])
