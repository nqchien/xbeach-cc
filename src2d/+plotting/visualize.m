function retval = visualize(s, sh, par);

figure(2); clf;
subplot(411); plot(1:41, sh.x); ylabel('x (m)');
subplot(412); plot(1:41, sh.dx(35:37,:)); ylabel('dx (m)');
subplot(413); plot(1:41, sh.v'); ylabel('v (m/s)');
subplot(414); plot(1:41, sh.Dnb); ylabel('\Delta n_B (m)');
% hold on;
retval = 1;
