function [s] = flow_init(s, par)
% Initialize the flow conditions
% to be applied at t = 0 (start of simulation)
% Input: `s`, the flow field structure
%        `par`, parameters of simulation
% Returns: `s`
%
hmin = par.hmin;
zs0 = par.zs0;
zb = s.zb;
s.zs = zeros(s.nx+1,s.ny+1) + zs0 - par.i * s.x;
ny = s.ny;
s.zs = max(zb, s.zs);
s.h = s.zs - zb;

s.dzsdt = zeros(size(zb));
s.dzbdt = zeros(size(zb));
s.uu = zeros(size(zb));
s.vv = zeros(size(zb));
vnorm = par.C * sqrt(s.h * par.i);

if par.scenario == "CHANNEL"
    alph = 22.8 * pi / 180;
    s.uu = vnorm * cos(alph);
    s.vv = vnorm * sin(alph);
elseif par.scenario == "BEND"
    margin = 2 * s.b;
    for i = 1:s.nx+1
        for j = 1:s.ny+1
            if (i > margin) && (j > margin)
                ang = atan((j - margin) / (i - margin));
                s.uu(i,j) = vnorm(i,j) .* sin(ang);
                s.vv(i,j) = vnorm(i,j) .* (-cos(ang));
            elseif i <= margin
                s.uu(i,j) = vnorm(i,j);
            elseif j <= margin
                s.vv(i,j) = -vnorm(i,j);
            end
        end
    end
end
    
s.qx = zeros(size(zb));
s.qy = zeros(size(zb));
s.sedero = zeros(size(zb));

s.wetv = zeros(size(zb));