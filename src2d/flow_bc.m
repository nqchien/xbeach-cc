function [s] = flow_bc (s, par)
% Apply the flow boundary condition
% 
zs  = s.zs;
h = s.h;
i = par.i;
C = par.C;

if par.scenario == "CHANNEL"
    le = 2:12;
    ri = 44:55;
    alph = 22.8 * pi / 180;
    q0 = 1.;
    vnorm = q0 ./ h(1,2:12);
    s.uu(1,2:12) = vnorm * cos(alph);
    s.vv(1,2:12) = vnorm * sin(alph);
    zs(1,:) = zs(2,:) + (s.dxStandard * cos(alph)) * i;
    s.zs(end,44:55) = s.zb(end,44:55) + 0.618;
    s.vv(:,1)= 0.;
    s.vv(:,end) = 0.;
elseif par.scenario == "BEND"
    q0 = .0617;
    vnorm = q0 ./ h(1,36:40);
    s.uu(1, 36:40) = vnorm;
    s.vv(1, 36:40) = 0;
    s.zs(1, 36:40) = s.zs(2, 36:40) - s.dxStandard * par.i;
    s.zs(36:40 ,1) = s.zb(36:40, 1) + 0.116;
    s.vv(36:40, 1) = -vnorm;
    s.uu(36:40, 1) = 0;
end