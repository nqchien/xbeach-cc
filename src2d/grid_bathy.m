function [s] = grid_bathy(par)
% Create grid and populate bathymetry
% CHANNEL - Inclined (22.8 deg) channel case (Rosatti 2005)
% BEND - Round bend (quarter-circle) case (Rosatti 2005)
% Input: 
%	`par`: a parameter struct
% Returns:
% 	`s`: structure with grid parameters

if par.scenario == "CHANNEL"
    zbinit = 0.5;   % background bed level for the domain
    h0 = 1.5;       % depth of channel relative to the background
    nx = 100;
    ny = 55;
    s.dxStandard = 10.;

    dx = zeros(nx+1,ny+1) + 10.;
    dy = 10.;
    x = cumsum(dx) - s.dxStandard;
    y = repmat(0:dy:ny*dy, nx+1, 1);

    s.xplot = x;
    s.yplot = y;
    delta_u = zeros(nx+1, ny+1);
    delta_v = zeros(nx+1, ny+1);
    Th = zeros(nx+1, ny+1);
    type = zeros(nx, ny);			% 0, 1, 3, 4 or 5
    subtype = zeros(nx, ny);		% 1, 2, 3, 4

    % Set up delta matrix for a 22.8 degrees channel
    alph = double(22.8 * pi /180);
    y1 = 1 + (0:100) * tan(alph);
    y2 = y1 + 10;
    d1 = 1 - mod(y1,1);
    d2 = mod(y2,1);
    for i = 1 : nx+1
        j1 = floor(y1(i)) + 1;
        j2 = j1 + 10;
        delta_u(i, j1) = d1(i);
        delta_u(i, j2) = d2(i);
        for j = j1+1 : j2-1
            delta_u(i,j) = 1;
        end
    end
    for i = 1:nx
        for j = 2:ny
            yavg = (j-1);
            if (y1(i) < yavg) && (yavg <= y2(i)) && (y1(i+1) <= yavg) && (yavg < y2(i+1))
                delta_v(i,j) = 1;
            elseif (y1(i) < yavg) && (yavg < y2(i)) && (y1(i+1) > yavg)
                delta_v(i,j) = d1(i) / (y1(i+1) - y1(i));
            elseif (y2(i) < yavg) && (y1(i+1) < yavg) && (yavg <= y2(i+1))
                delta_v(i,j) = d2(i+1) / (y2(i+1) - y2(i));
            else
                delta_v(i,j) = 0;
            end
        end
    end

    % Determine cell types (common part)
    for i = 1:nx
        for j = 1:ny
            de = delta_u(i+1,j);
            dw = delta_u(i,j);
            dn = delta_v(i,j+1);
            ds = delta_v(i,j);
            d = [dw de ds dn];
            Sx = dw + de;
            Sy = dn + ds;
            Dx = abs(dw - de);
            Dy = abs(dn - ds);
            Th(i,j) = (Sx * Sy + Sx * Dy + Sy * Dx - Dx * Dy) / 4;
            A = find(d == 0);
            B = find((d > 0) & (d < 1));
            C = find(d == 1);
            switch length(A)
                case 2
                    type(i,j) = 3;
                    if de * ds > 0
                        subtype(i,j) = 31;
                    elseif de * dn > 0
                        subtype(i,j) = 32;
                    elseif dn * dw > 0
                        subtype(i,j) = 33;
                    elseif dw * ds > 0
                        subtype(i,j) = 34;
                    end
                case 1
                    type(i,j) = 4;
                    if dw * dn * de > 0
                        subtype(i,j) = 41;
                    elseif dn * dw * ds > 0
                        subtype(i,j) = 42;
                    elseif dw * ds * de > 0
                        subtype(i,j) = 43;
                    elseif ds * de * dn > 0
                        subtype(i,j) = 42;
                    end
                    if length(B) ~= 1 && length(B) ~= 2
                        type(i,j) = 1; % Flow cell
                    end
                case 0
                    type(i,j) = 5;
                    if (ds < 1) && (de < 1)
                        subtype(i,j) = 51;
                    elseif (de < 1) && (dn < 1)
                        subtype(i,j) = 52;
                    elseif (dn < 1) && (dw < 1)
                        subtype(i,j) = 53;
                    elseif (dw < 1) && (ds < 1)
                        subtype(i,j) = 54;
                    end
                    if length(B) == 2
                        % Th(i,j) = 1 - 0.5 * (1 - d(B(1))) * (1 - d(B(2)));
                    else
                        type(i,j) = 1;	% Flow cell
                    end
                otherwise
                    type(i,j) = 0;	% Solid cell
            end
        end
    end

    % determining centroids
    dx0 = s.dxStandard;
    for i = 1:nx
        for j = 2:ny
            de = delta_u(i+1,j);
            dw = delta_u(i,j);
            dn = delta_v(i,j+1);
            ds = delta_v(i,j);
            if type(i,j) == 3
                if subtype(i,j) == 33	% southern bank
                    x(i,j) = x(i,j) - dx0/2 + dx0 * delta_v(i,j+1)/3;
                    y(i,j) = y(i,j) + dy/2 - dy * delta_u(i,j)/3;
                elseif subtype(i,j) == 31
                    x(i,j) = x(i,j) + dx0/2 - dx0 * delta_v(i,j)/3;
                    y(i,j) = y(i,j) - dy/2 + dy * delta_u(i+1,j)/3;
                end
            elseif type(i,j) == 4
                if subtype(i,j) == 41
                    x(i,j) = x(i,j) + dx0/2 - (2*delta_u(i,j) + delta_u(i+1,j))/(3*(delta_u(i,j)+delta_u(i+1,j)))*dx0;
                    y(i,j) = y(i,j) + dy/2 - dy * (delta_u(i,j)+delta_u(i+1,j)) / 4;
                elseif subtype(i,j) == 43
                    x(i,j) = x(i,j) - dx0/2 + (delta_u(i,j) + 2*delta_u(i+1,j))/(3*(delta_u(i,j)+delta_u(i+1,j)))*dx0;
                    y(i,j) = y(i,j) - dy/2 + dy * (delta_u(i,j)+delta_u(i+1,j)) / 4;
                end
            elseif type(i,j) == 5
                if subtype(i,j) == 51	% southern bank
                    x(i,j) = x(i,j) - dx0 * (1/2 - ds/3) * (0.5 * de * ds)/(1 - de * ds / 2);
                    y(i,j) = y(i,j) + dy * (1/2 - de/3) * (0.5 * de * ds)/(1 - de * ds / 2);
                elseif subtype(i,j) == 53	% northern bank
                    x(i,j) = x(i,j) + dx0 * (1/2 - dn/3) * (0.5 * dn * dw)/(1 - dn * dw / 2);
                    y(i,j) = y(i,j) - dy * (1/2 - dw/3) * (0.5 * dn * dw)/(1 - dn * dw / 2);
                end
            end
        end
    end
    
elseif par.scenario == "BEND"
    zbinit = -2;
    h0 = 4;
    hmin = 0.001;
    s.dxStandard = 5;

    ri = 25;
    ro = 30;
    b = 5;
    nx = 40;
    ny = nx;
    margin = 2 * b;
    rightbank = 35;
    leftbank = 41;

    delta_u = ones(nx+1, ny+1);
    delta_v = ones(nx+1, ny+1);
    Th = ones(nx+1, ny+1);
    type = zeros(nx, ny);			% 0, 1, 3, 4 or 5
    
    dx = zeros(nx+1,ny+1) + s.dxStandard;
    dy = 5.;
    x = cumsum(dx) - s.dxStandard;
    y = repmat(0:dy:ny*dy, nx+1, 1);

    s.xplot = x;
    s.yplot = y;    
end



if par.scenario == "CHANNEL"
    zb = zeros(nx+1,ny+1) + zbinit;  % background bed level for the domain
    a = ones(nx+1, ny+1);
    for i = 1 : nx+1
        jstart = floor(y1(i)) + 1;
        jstop = floor(y2(i)) + 2;
        if (i <= nx) && (type(i,jstop) == 0)
            jstop = jstop - 1;
        end
        % bed level of the sloped channel for water flowing from W to E
        for j = jstart : jstop
            zb(i,j) = (zbinit - h0) - par.i * x(i,jstart) / cos(alph) - par.i * (j-jstart) * dy * sin(alph);
        end
    end
elseif par.scenario == "BEND"
    zb = zeros(nx+1,ny+1) + 1.;
    a = ones(nx+1, ny+1);
    dxStandard = s.dxStandard;
    for i = 1 : nx
        for j = 1 : ny
            if (i <= margin) && (rightbank < j) && (j < leftbank)
                zb(i,j) = -1 - par.i * i * dxStandard;
            elseif (j <= margin) && (rightbank < i) && (i < leftbank)
                zb(i,j) = -1 - par.i * margin * dxStandard - ...
                        par.i * (pi/2) * 0.5 * (ri + ro) * dy - ...
                        par.i * (margin - j) * dy;
            elseif (i > margin) && (j > margin)
                r2 = (i-margin)^2 + (j-margin)^2;
                if (ri^2 <= r2) && (r2 <= ro^2)
                    ang = pi/2 - atan((j-margin)/(i-margin));
                    zb(i,j) = -1 - par.i * margin * dxStandard - par.i * ang * 0.5 * (ri + ro) * dy;
                end
            end
        end
    end
    zb(:,ny+1) = 1.;
    zb(nx+1,:) = 1.;
end

% Modify structure
s.x = x;
s.y = y;
s.dx = dx;
s.dy = dy;
s.nx = nx;
s.ny = ny;
s.zb = zb;
s.zbinit = zb;
s.a = a;
s.Th = Th;
s.type = type;
s.delta_u = delta_u;
s.delta_v = delta_v;

if par.scenario == "CHANNEL"
    s.subtype = subtype;
elseif par.scenario == "BEND"
    s.b = b;
    s.dxStandard = dxStandard;
end