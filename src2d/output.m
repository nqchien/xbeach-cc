function [] = output(it, par, s)
% On-the-fly visualization

if exist('OCTAVE_VERSION') == 0    % running in Matlab
	figure(1);
    title("Time level: " + it);
	xplot = s.x - s.dxStandard / 2;
	yplot = s.y - s.dy / 2;
	colormap(summer); 
    pcolor(xplot, yplot, s.h .* (s.h < 1)); 
    shading flat;
	hold on;
	caxis([-5 0]); colorbar;
	quiver(s.x, s.y, s.u, s.v);
    if par.scenario == "BEND"
        axis equal;
        xlim([50 200]);
        ylim([50 200]);
    end
	drawnow;
	hold off;
else    % running in Octave - no plotting
	disp('now in Octave');
end