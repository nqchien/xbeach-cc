% BANK calculate the retreat of bank and plot them as
% multiple polylines over time

dx = [0,53,50,55,57,52,56,58,51,59,54];
x=zeros(1,length(dx))+cumsum(dx);
dy = 50;
y = [20.0, 20.6, 20.4, 20.9, 20.1, ...
    20.5, 20.8, 20.7, 20.2, 20.3, 21.0] ;
Dt = 3600;
u = [0.40, 0.40, 0.44, 0.47, 0.48, 0.41, ...
    0.46, 0.49, 0.42, 0.43, 0.50];
S0 = zeros(11) + 4E-4;
kV = zeros(11) + 1E-4;
ks = zeros(11) + 1;
zb = zeros(11) - 1;
zbank = zeros(11) + 2;
%newly added
vcrit = zeros(11) + 0.14;
E = zeros(11) + 0.05;
G = zeros(11) + 0.1;
% alpha, eta, L, Awet, DAwet, DV, C, Dzb,
% Vp are matrices containing 10 elements.
clf
for i=0:500
    u=8./y;  % huh ?
    for k=2:11
        kp(k) = exp(-y(k)/dy);
        L(k)=sqrt((y(k-1)-y(k))^2+dx(k)^2);
        alpha(k)= atan((y(k-1)-y(k))/dx(k));
        cosa = cos(alpha(k));        
        Awet(k)= (y(k-1)+y(k))/2 * dx(k);
        Vp(k)= (u(k-1)+u(k))/2/cosa;
        Dnb(k) = 0;
        if Vp(k) > vcrit(k)
            Dnb(k) = E(k)*(Vp(k)^2 - vcrit(k)^2)/(vcrit(k)^2);
        end
        Hfb(k) = zbank(k) - 0;   % 0 is the water level
        hw(k) = 0 - zb(k);
        hwc(k) = 0.8 * hw(k);   % assumed
        if hw(k) > hwc(k);
            Dnb(k) = Dnb(k) + G(k)*(hw(k) - hwc(k))/ hwc(k);
        end
        DV(k) = Dnb(k) / cosa * (zbank(k) - zb(k)) * dx(k);
%         DV(k)=kV(k)*ks(k)*kp(k)* Vp(k) * L(k) * Dt; % change here
        C(k) = 2 * Dnb(k) / cosa;
        % C(k)=2*DV(k)/((zbank(k)-zb(k))*dx(k));
        if k==2
            eta(1)=C(2)/2;
            y(1)=y(1)+eta(1);
        end
        eta(k) = C(k) - eta(k-1);
        DAwet(k)=(eta(k-1)+eta(k))*L(k)/2.;
        Dzb(k) = (S0(k) * Dt + DV(k)) / ...
        (Awet(k) + DAwet(k));
        Awet(k) = Awet(k) + DAwet(k);
        y(k) = y(k) + eta(k);
        zb(k) = zb(k) + Dzb(k);
    end
    if mod(i,50) ~= 0
        continue
    end
    plot(x,y);hold on
end
%# append small spatial matrices into
%# spatio-temporal matrices
%Awarr.append(Awet(:11))
%yarr.append(y(:11))
%zbarr.append(zb(:11))
