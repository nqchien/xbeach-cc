function [D,Qb]=roelvink(h,t,E,gamma,alpha,n)

% Dissipation acc. to Roelvink (1993)

g=9.81;
rho=1025;
fac=8/rho/g;
alfa=1;

H=sqrt(fac*E);
Qb=min((1-exp(-(H./gamma./h).^n)),1);
D=Qb*2*alpha/t.*E;
