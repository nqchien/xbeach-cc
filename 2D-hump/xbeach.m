% Main routine xbeach
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Copyright (C) 2007 UNESCO-IHE, WL|Delft Hydraulics and Delft University %
% Dano Roelvink, Ap van Dongeren, Dirk-Jan Walstra, Ad Reniers            %
%                                                                         %
% d.roelvink@unesco-ihe.org                                               %
% UNESCO-IHE Institute for Water Education                                %
% P.O. Box 3015                                                           %
% 2601 DA Delft                                                           %
% The Netherlands                                                         %
%                                                                         %
% This library is free software; you can redistribute it and/or           %
% modify it under the terms of the GNU Lesser General Public              %
% License as published by the Free Software Foundation; either            %
% version 2.1 of the License, or (at your option) any later version.      %
%                                                                         %
% This library is distributed in the hope that it will be useful,         %
% but WITHOUT ANY WARRANTY; without even the implied warranty of          %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        %
% Lesser General Public License for more details.                         %
%                                                                         %
% You should have received a copy of the GNU Lesser General Public        %
% License along with this library; if not, write to the Free Software     %
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307     %
% USA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
%
% General input per module
[par] = wave_input;
[par] = flow_input(par);
[par] = sed_input(par);
% Grid and bathymetry
[s] = grid_bathy;
% Directional distribution wave energy
[s] = wave_dist (s,par);
% Initialisations
[s,par] = wave_init (s,par);
[s] = flow_init (s,par);
[s,par] = sed_init (s,par);
par.nt=12000;
nt=par.nt;
par.t=0;
par.dt=.2;
par.tnext=par.tint;
it=0;
% disp(s.H) %
while par.t<par.tstop;
    % Wave boundary conditions
    [par,s] = wave_bc (s,par,it);
    % Flow boundary conditions
    [s] = flow_bc (s,par,it);
    % Wave timestep
    [s] = wave_timestep (s,par);
    % Flow timestep
    [s,par] = flow_timestep (s,par);
    % Suspended transport
    [s]=transus(s,par);
    % Bed level update
    [s]=bed_update(s,par);
    % Output
    [it,s]=output(it,s,par);
end
% disp(s.H) %