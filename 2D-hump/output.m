function [it,s]=output(it,s,par);
if  mod(par.t,par.tint)==0;
    figure(1);
        % pcolor(s.x,s.y,s.zb); shading interp; grid
        % contour(s.x,s.y,s.H); grid on;
        % caxis([-.5 2]);colorbar;
		% axis equal tight;
		axis([500 2000 0 2500]);
        % hold on;
		quiver(s.x,s.y,s.u,s.v);drawnow;hold off
        % the velocity field changes, so hold off has to be used so that
        % new velocities will not overlap old ones
    drawnow;
end