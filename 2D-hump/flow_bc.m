function [s] = flow_bc (s,par,it);
zs  = s.zs;
uu  = s.uu;
vv  = s.vv;
% added for bound long wave bc Ad 27 march 2006
ui  = s.ui;
hh  = s.h;
%
% Sea boundary at x=0;
%
% zs(1,:)  = 0;  % old boundary
% 
% new boundary (not perfect, ignores direction of incoming free long wave
% and also the difference in velocity response for free and bound long
% waves) Ad 27 march 2006
%
  %
  g=9.81;
  omega=2*pi/par.Tlong;
  dt=par.dt;
  t=par.t;
  dx=s.dx;
  %ui=uin*sin(omega*t);
  ui=0;
  uu(1,:) = 0;
  % uu(1,:)=2*ui -sqrt(9.81./hh(1,:)).*(zs(2,:)-par.zs0);
% vv(:,1) = par.C * sqrt(hh(:,1) * 0.0001);
% vv(:,1) = 0;
% Lateral boundary at y=0;
%
zs(2:end,1)=zs(2:end,2);
vv(2:end,1)=vv(2:end,2);
%
% Lateral boundary at y=ny*dy;
%
zs(2:end,end)=zs(2:end,end-1);
vv(2:end,end)=vv(2:end,end-1);
%
% Land boundary at x=nx*dx;
%
uu(end,:)=0;
%
% Output
%
s.zs  = zs;
s.uu  = uu;
s.vv  = vv;
