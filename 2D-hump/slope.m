function [dhdx,dhdy] = slope (h,dx,dy)
dhdx=zeros(size(h));
dhdy=zeros(size(h));
dhdx(2:end-1,:)=(h(3:end,:)-h(1:end-2,:))/2/dx;
dhdx(1,:)=(h(2,:)-h(1,:))/dx;
dhdx(end,:)=(h(end,:)-h(end-1,:))/dx;
dhdy(:,2:end-1)=(h(:,3:end)-h(:,1:end-2))/2/dy;
dhdy(:,1)=(h(:,2)-h(:,1))/dy;
dhdy(:,end)=(h(:,end)-h(:,end-1))/dy;
