function [s]=transus(s,par)
% Sediment transport; bed load or total transport evaluated in u, v
% points
uu     = s.uu;  
vv     = s.vv;  
wetu   = s.wetu;
wetv   = s.wetv;
u      = s.u;
v      = s.v;
vmag2  = u.^2+v.^2;
A      = par.A;
n      = par.n;
ceq    = A*vmag2.^((n-1)/2);
    % 
    % Upwind method implemented through weight factors mx1 and mx2
    %
    % X-direction
    mx=sign(uu);
    mx(abs(uu)<umin)=0;
    mx1=(mx+1)/2;
    mx2=-(mx-1)/2;
    f1(1:nx,:)=c(1:nx,:);
    f2(1:nx,:)=c(2:nx+1,:);
    % water depth in u-points
    cu=mx1.*f1+mx2.*f2;
    Su=cu.*hu.*uu;
    % Y-direction
    my=sign(vv);
    my(abs(vv)<umin)=0;
    my1=(my+1)/2;
    my2=-(my-1)/2;
    f1=f1*0;f2=f2*0;
    f1(:,1:ny)=c(:,1:ny);
    f2(:,1:ny)=c(:,2:ny+1);
    cv=my1.*f1+my2.*f2;
    Sv=cv.*hv.*vv;
%
s.Su = Su;
s.Sv = Sv;