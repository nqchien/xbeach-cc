function Nt = logistic(K, No, r, t); 
	Nt = K ./ (1 - (1 - No/K)*exp(-r*t)); 
