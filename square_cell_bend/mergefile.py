import os
listfile = os.listdir('.')
bigfile = file('allcode.txt','w')

for filename in listfile:
    if filename[-1] != 'm':
        listfile.remove(filename)
    else:
        bigfile.write(filename.upper()+'\n'*2)
        smallfile = file(filename,'r')
        bigfile.write(smallfile.read())
        bigfile.write('\n'*2+'_'*12+'\n')

bigfile.close()
bigfile = open('allcode.txt','r')

texfile = open('layout.tex','w')
texfile.write(
r"""\documentclass[a4paper,12pt]{report}
%\usepackage{a4wide}
\setlength{\headheight}{0cm}
\setlength{\headsep}{0cm}
\setlength{\topmargin}{-1.5cm}
\setlength{\oddsidemargin} {-1.5cm}
\setlength{\evensidemargin} {-1.5cm}
\setlength{\textheight}{27cm}
\setlength{\textwidth}{19cm}

\begin{document}
\begin{verbatim}
""")
texfile.write(bigfile.read())
texfile.write(
r"""\end{verbatim}
\end{document}
""")
texfile.close()
bigfile.close()

os.execv(r"D:\ProTeXt\MiKTeX\texmf\miktex\bin\pdflatex.exe", 
("layout.tex",)) # second parameter argv[1] must be a tuple

while 1:
    pass    # in order to avoid the error
    