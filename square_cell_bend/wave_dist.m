function [s] = wave_dist (s,par);
dir0     = par.dir0;
Hrms     = par.Hrms;
m        = par.m;
thetamin = par.thetamin;
thetamax = par.thetamax;
dtheta   = par.dtheta;
rhog8    = par.rhog8;
Tm01 = par.Tm01;
%
% Specify theta-grid
%
degrad=pi/180;
thetamin=thetamin*degrad;
thetamax=thetamax*degrad;
dtheta=dtheta*degrad;
theta=[thetamin+dtheta/2:dtheta:thetamax-dtheta/2];
ntheta=length(theta);
%
% Directional distribution
%
theta0=dir0*degrad;
dist=(cos(theta-theta0)).^m;
dist(abs(theta-theta0)>pi/2)=0;
E=rhog8*Hrms^2;
costh=cos(theta);
sinth=sin(theta);

% energy density distribution

factor = (dist/sum(dist))'/dtheta;
e01    = factor*E;
e01    = max(e01,0.00001);
%
% Output
%
s.theta   = theta;
s.ntheta  = ntheta;
s.dtheta  = dtheta;
s.e01     = e01;
s.costh   = costh;
s.sinth   = sinth;
par.theta0=theta0;

