function [D,Qb,Hb]=baldock(h,t,k,E,gamma)

% Dissipation acc. to Baldock et al. 1998

g=9.81;
rho=1025;
fac=8/rho/g;
alfa=1;

H=sqrt(fac*E);

kh=k.*h;
tkh=tanh(kh);

% Wave dissipation acc. to Baldock et al. 1998
Hb = (0.88./k).*tanh(gamma.*kh/0.88);
Qb = exp(-(Hb./H).^2);
D = 0.25*alfa*Qb*rho*(1/t)*g.*(Hb.^2+H.^2);
