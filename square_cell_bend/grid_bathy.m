function [s, sh, ZBINIT] = grid_bathy(par)

% Bended channel according to Rosatti (2005)
ZBINIT = -2;
h0 = 4;
hmin = 0.001;
dxStandard = 5;

ri = 25;
ro = 30;
b = 5;
nx = 40;
ny = nx;
margin = 2 * b;
rightbank = 35;
leftbank = 41;

dx = zeros(nx+1,ny+1) + dxStandard;     % dx is now an array
dy = 5.;
x = cumsum(dx) - dxStandard;
y = repmat(0:dy:ny*dy, nx+1, 1);

s.xplot = x;
s.yplot = y;

% channel
zb = zeros(nx+1,ny+1) + 1.;

% nm = 5;
% nm = floor(margin/dxStandard);	% added for joints
% nm = 25;	% added for joints
for i = 1 : nx
	for j = 1 : ny
		if (i <= margin) & (rightbank < j) & (j < leftbank)
			zb(i,j) = -1 - par.i * i * dxStandard;
		elseif (j <= margin) & (rightbank < i) & (i < leftbank)
			zb(i,j) = -1 - par.i * margin * dxStandard - ...
					par.i * (pi/2) * 0.5 * (ri + ro) * dy - ...
					par.i * (margin - j) * dy;
		elseif (i > margin) & (j > margin)
			r2 = (i-margin)^2 + (j-margin)^2;
			if (ri^2 <= r2) & (r2 <= ro^2)
				ang = pi/2 - atan((j-margin)/(i-margin));
				zb(i,j) = -1 - par.i * margin * dxStandard - par.i * ang * 0.5 * (ri + ro) * dy;
			end
		end
	end
end
zb(:,ny+1) = 1.;
zb(nx+1,:) = 1.;

% End added

% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dxStandard = dxStandard;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.b = b;
s.ZBINIT = zb;

% Initial position of shoreline (additional code by Chien)
% Best to be predefined
% Here we take the intersection between bed and water surface
% THIS CODE USES INITIAL ZS, WHICH IS NOT OFFICIALLY DEFINED

if 0
	yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
	xx = [];           % cross-shore coordinates, can vary
	iiwet = [];     % list of positions of wet cells
	ny = s.ny;
	for i = 1 : ny+1
		le = 1; 	ri = ny;
		while s.zb(le,i)<0; le=le+1; end;
		while s.zb(ri,i)>0; ri=ri-1; end;
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0.05 - 0.1 * (i - 1)/ny); % par.zs0 for simple case
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0); % par.zs0 == 0
		xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0 - par.i * s.y(1,i)); % account for slope
	%   xi = interp1(s.zb(:,i),s.x(:,i),0.0,'nearest'); 
		% position of shoreline interpolate where zb = 0 ; this interpolation
		% function does not seem to work well
		% iwet = length(find(s.zb(:,i)<0));       % index of wet cell = number of cells with zb < 0
		iwet = length(find(s.zb(:,i)<0 - par.i * s.y(1,i)));       % index of wet cell = number of cells with zb < zs
		xx = horzcat(xx, [xi]);
		iiwet = horzcat(iiwet, [iwet]);
	end
	iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward

	% figure(1); clf; plot(xx, yy, 'g*-', 'linewidth',2);

	% Setting up the sh struct
	% sh is the structure containing data of shoreline particularly

	% these are spatial properties of shoreline
	sh.xL0 = [];	sh.yL0 = [];
	for i = 1:s.ny;
		sh.xL0 = horzcat(sh.xL0, [xx(i) xx(i)]);
		sh.yL0 = horzcat(sh.yL0, [yy(i) yy(i+1)]);
	end;

	sh.x0 = xx;
	sh.y0 = yy;

	% Alternately, we can define the coastline coordinates sh.x here
	sh.x = xx;
	sh.y = yy;
	sh.iiwet = iiwet;
	sh.iidry = iidry;
end;
sh.x = 0;
% End added