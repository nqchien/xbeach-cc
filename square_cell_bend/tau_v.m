function ret = tau_v(v);

% tau_v(v) calculates the shear stress exert on bank corresponding
% to a near-bank velocity v according to Lane's formula:
% tau = 0.75 * rho * g * u^2 / C^2
% with Chezy coefficient C = 65 m^0.5 / s.

ret = 0.75 * 9810 * v^2 / (65^2);
