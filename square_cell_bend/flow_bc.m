function [s] = flow_bc (s, par)     % (s,par,it)
% zs  = s.zs;
dy = s.dy;

ui = 0;
h = s.h;		i = par.i;	C = par.C;

% Diagonal, narrow channel
% SW
% s.vv(1,1) = C * sqrt (h(1, 1) * i) / sqrt(2);
% s.uu(1,1) = C * sqrt (h(1, 1) * i) / sqrt(2);
% NE
% s.vv(end,end-1) = C * sqrt (h(end,end-1) * i) / sqrt(2);
% s.uu(end,end-1) = C * sqrt (h(end,end-1) * i) / sqrt(2);

% Bended channel
% with joints  31:35 instead of 26:30
q0 = .0617;
vnorm = q0 ./ h(1,36:40);
s.uu(1, 36:40) = vnorm;
s.vv(1, 36:40) = 0;
s.zs(1, 36:40) = s.zs(2, 36:40) - s.dxStandard * par.i;

% zs(1, 36:40) = zs(2, 36:40) + s.dxStandard * par.i;
% zs(36:40 ,1) = zs(36:40, 2) - dy * par.i;
s.zs(36:40 ,1) = s.zb(36:40, 1) + 0.116;
% zs(36:40 ,1) = zs(36:40, 2) + s.dxStandard * par.i;

vnorm = C * sqrt(h(36:40, 1) * par.i);
s.vv(36:40, 1) = -vnorm;
% s.vv(36:40, 1) = s.vv(36:40,2);
s.uu(36:40, 1) = 0;

% s.zs  = zs;

% 175 cells, dx = 0.1 m
% q0 = .0617 * dy;
% vnorm = q0 ./ h(1,151:175);
% s.uu(1, 151:175) = vnorm;
% s.vv(1, 151:175) = 0;

% vnorm = C * sqrt(h(151:175, 1) * i);
% s.vv(151:175, 1) = -vnorm;
% s.uu(151:175, 1) = 0;

% zs(1, 151:175) = zs(2, 151:175) + s.dxStandard * i;
% zs(151:175 ,1) = zs(151:175, 2) - dy * i;

% s.zs  = zs;

