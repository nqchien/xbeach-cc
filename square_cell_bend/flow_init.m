function [s]=flow_init(s,par)
hmin = par.hmin;
zs0 = par.zs0;
% h0 = par.h0;
zb = s.zb;
% h = zeros(size(zb)) + h0 .* sign(s.type);
% s.h=max(zs0-zb,0);	% old code
% s.zs=max(zb,zs0);	% old code
% Added code
% for j = 1:ny+1;
	% s.zs(1:36, j) = 0.1 - (j - 1) * 0.1 / ny;
% end;
s.zs = zeros(s.nx+1,s.ny+1) + zs0; % - par.i * s.x;
% s.zs = s.zb + 0.116 * (s.type > 0); 
ny = s.ny;
% s.zs(1:36, :) = repmat(linspace(0.05,-0.05,ny+1), 36, 1);
s.zs = max(zb, s.zs);
s.h = s.zs - zb;

% End added
%s.zs=ones(size(zb))*zs0;
s.dzsdt=zeros(size(zb));
s.dzbdt=zeros(size(zb));
s.uu = zeros(size(zb));
s.vv = zeros(size(zb));
% s.vv = par.C * sqrt(s.h * par.i);
vnorm = par.C * sqrt(s.h * par.i);

% with joints
margin = 2 * s.b;
for i = 1:s.nx+1
	for j = 1:s.ny+1
		if (i > margin) & (j > margin);
			ang = atan((j - margin) / (i - margin));
			s.uu(i,j) = vnorm(i,j) .* sin(ang);
			s.vv(i,j) = vnorm(i,j) .* (-cos(ang));
		elseif i <= margin
			s.uu(i,j) = vnorm(i,j);
		elseif j <= margin
			s.vv(i,j) = -vnorm(i,j);
		end
	end
end

% s.uu(:) = 0;
% s.vv(:) = 0;

% s.zs = zb + sign(s.type) * h0;
% s.h = h;
% s.uu = vnorm * cos(alph);
% s.vv = vnorm * sin(alph);
% s.uu = par.C * sqrt(s.h * par.i);
% s.vv = s.vv .* (s.h > 0);	% if h = 0 then v = 0
% for i = 1:s.ny+1
    % s.vv(21:36,i) = linspace(v0,v0/2,16)';
% end
s.qx=zeros(size(zb));
s.qy=zeros(size(zb));
s.sedero=zeros(size(zb));
