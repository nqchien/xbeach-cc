plot(xt,s.zbt(:,1),'k--',s.xp,s.zp(:,1),'k','linewidth',1);hold on;
plot(xt,s.zbt(:,181),'b--',s.xp,s.zp(:,2),'b','linewidth',1);hold on;
plot(xt,s.zbt(:,361),'r--',s.xp,s.zp(:,3),'r','linewidth',1);hold on;
plot(xt,s.zbt(:,721),'m--',s.xp,s.zp(:,5),'m','linewidth',1);hold on;
plot(xt,s.zbt(:,1441),'g--',s.xp,s.zp(:,9),'g','linewidth',2);hold on;
legend('0 hr comp.','0 hr meas.','1 hr comp.','0 hr meas.','2 hrs comp.','2 hr meas.','4 hrs comp.','4 hr meas.','8 hrs comp.','8 hr meas.')