clf;
hold on;

xmin = -s.dxStandard / 2;
xmax = xmin + (s.nx+1) * s.dxStandard;
ymin = -s.dy / 2;
ymax = ymin + (s.ny+1) * s.dy;

% Horizontal grid
for i = xmin:s.dxStandard:xmax; plot([i i], [ymin ymax], 'cyan');end;

% Vertical grid;
for i = ymin:s.dy:ymax; plot([xmin xmax], [i i], 'cyan'); end;

% Boundary

% Bend channel
margin = 2 * s.b;
grSize = s.dy;
% xo = -0.25:0.1:14.76;
% xi = -0.25:0.1:12.26;
xo = -grSize/2 : 1 : 150 - grSize/2;
xi = -grSize/2 : 1 : 125 - grSize/2;
plot(margin + xo, margin + sqrt(150^2 - (xo+grSize/2) .^ 2) - grSize/2, 'k', 'linewidth', 2);
plot(margin + xi, margin + sqrt(125^2 - (xi+grSize/2).^2) - grSize/2, 'k', 'linewidth', 2);
quiver(s.x , s.y, s.u, s.v);
axis equal tight;

xlabel('X (m)')
ylabel('Y (m)')
drawnow;