function [s, sh, ZBINIT] = grid_bathy(par)

% Bended channel according to Rosatti (2005)
% finer
ZBINIT = -2;
h0 = 4;
hmin = 0.001;
dxStandard = 5;

ri = 125;
ro = 150;
b = 25;
nx = 80;
ny = nx;
margin = 2 * b;

dx = zeros(nx+1,ny+1) + dxStandard;     % dx is now an array
dy = 5;
x = cumsum(dx) - dxStandard;
y = repmat(0:dy:ny*dy, nx+1, 1);

s.xplot = x;
s.yplot = y;
delta_u = zeros(nx+1, ny+1);
delta_v = zeros(nx+1, ny+1);
Th = zeros(nx+1, ny+1);
type = zeros(nx, ny);		% 3 4 or 5

% Set up delta matrix for quarter circle

for i = 1:nx+1
	for j = 1:ny
		x1 = (i-1) * dxStandard;
		y1 = (j - 1) * dy;
		y2 = j * dy;
		if (x1 >= margin) & (y1 >= margin)	% added for joints
			x1o = x1 - margin;	
			y1o = y1 - margin;	% added for joints
			y2o = y2 - margin;
			if x1o^2 + y2o^2 <= ri^2
				delta_u(i,j) = 0;
			elseif x1o^2 + y1o^2 >= ro^2
				delta_u(i,j) = 0;
			elseif (x1o^2 + y1o^2 < ri^2) & (x1o^2 + y2o^2 > ri^2)
				delta_u(i,j) = (y2o - sqrt(ri^2 - x1o^2))/dy;
			elseif (x1o^2 + y1o^2 < ro^2) & (x1o^2 + y2o^2 > ro^2)
				delta_u(i,j) = (sqrt(ro^2 - x1o^2) - y1o)/dy;
			else
				delta_u(i,j) = 1;
			end
		else
			if (margin + ri < x1) & (x1 < margin + ro)
				delta_u(i,j) = 1;
			elseif (margin + ri <= y1) & (y2 <= margin + ro)
				delta_u(i,j) = 1;
			else
				delta_u(i,j) = 0;
			end
		end
	end
end

for j = 1:ny+1
	for i = 1:nx
		x1 = (i-1) * dxStandard;
		x2 = i * dxStandard;
		y1 = (j-1) * dy;
		if (y1 >= margin) & (x1 >= margin)	% added for joints
			x1o = x1 - margin;
			x2o = x2 - margin;	% added for joints
			y1o = y1 - margin;
			if x2o^2 + y1o^2 <= ri^2
				delta_v(i,j) = 0;
			elseif x1o^2 + y1o^2 >= ro^2
				delta_v(i,j) = 0;
			elseif (x1o^2 + y1o^2 < ri^2) & (x2o^2 + y1o^2 > ri^2)
				delta_v(i,j) = (x2o - sqrt(ri^2 - y1o^2))/dxStandard;
			elseif (x1o^2 + y1o^2 < ro^2) & (x2o^2 + y1o^2 > ro^2)
				delta_v(i,j) = (sqrt(ro^2 - y1o^2) - x1o)/dy;
			else
				delta_v(i,j) = 1;
			end
		else
			if (margin + ri < y1) & (y1 < margin + ro)
				delta_v(i,j) = 1;
			elseif (margin + ri <= x1) & (x2 <= margin + ro)
				delta_v(i,j) = 1;
			else
				delta_v(i,j) = 0;
			end
		end
	end
end

% Determine cell types (common part)
for i = 1:nx;
	for j = 1:ny;
		d = [delta_u(i,j)  delta_u(i+1,j)  delta_v(i,j)  delta_v(i,j+1)];
		A = find(d == 0);
		B = find((d > 0) & (d < 1));
		C = find(d == 1);
		switch length(A)
			case 2
				type(i,j) = 3;
			case 1
				type(i,j) = 4;
				if length(B) == 3
					type(i,j) = 1; % Flow cell
				end;
			case 0
				type(i,j) = 5;
				if length(B) ~= 2
					type(i,j) = 1;	% Flow cell
				end
			otherwise
				type(i,j) = 0;	% Solid cell
		end;
		Sx = d(1) + d(2);
		Sy = d(3) + d(4);
		Dx = abs(d(1) - d(2));
		Dy = abs(d(3) - d(4));
		Th(i,j) = (Sx * Sy + Sx * Dy + Sy * Dx - Dx * Dy) / 4;
	end;
end;

% determining centroids
type(nx+1,:) = 0;
type(:,ny+1) = 0;
for i = 1:nx
	for j = 1:ny
		dn = delta_v(i,j+1);
		ds = delta_v(i,j);
		de = delta_u(i+1,j);
		dw = delta_u(i,j);
		if (type(i,j) == 4) & (type(i, j+1) == 1)	% inner, NW
			x(i,j) = x(i,j) - dxStandard/2 + ...
					(dw + 2*de)/ (3*(dw+de))*dxStandard;
			y(i,j) = y(i,j) + dy/2 - dy * (dw+de) / 4;
		elseif (type(i,j) == 4) & (type(i, j+1) == 0)	% outer, NW
			x(i,j) = x(i,j) + dxStandard/2 - (2*dw + de)/ ...
					(3*(dw+de))*dxStandard;
			y(i,j) = y(i,j) - dy/2 + dy * (dw+de) / 4;
		elseif (type(i,j) == 4) & (type(i+1, j) == 1)	% inner, SE
			x(i,j) = x(i,j) + dxStandard/2 - dxStandard * (dn + ds) / 4;
			y(i,j) = y(i,j) - dy/2 + (2*dn + ds)/ (3*(dn+ds))*dy;
		elseif (type(i,j) == 4) & (type(i+1, j) == 0)	% outer, SE
			x(i,j) = x(i,j) - dxStandard/2 + dxStandard * (dn + ds) / 4;
			y(i,j) = y(i,j) + dy/2 - (dn + 2*ds)/ (3*(dn+ds))*dy;
		elseif (type(i,j) == 3) & (type(i,j+1) ~= 0) % ((type(i,j+1) == 1) || (type(i,j+1) == 3) || (type(i,j+1) == 5))	% inner NW+SE
			x(i,j) = x(i,j) + dxStandard/2 - dxStandard * dn/3;
			y(i,j) = y(i,j) + dy/2 - dy * de/3;
		elseif (type(i,j) == 3) & (type(i,j+1) == 0)	% outer NW+SE
			x(i,j) = x(i,j) - dxStandard/2 + dxStandard * ds/3;
			y(i,j) = y(i,j) - dy/2 + dy * dw/3;
		end
	end
end

% channel
zb = zeros(nx+1,ny+1) + 1.;

nm = floor(margin/dxStandard);	% added for joints

for i = 1 : nx
	for j = 1 : ny
		if type(i,j) > 0
			if i <= nm
				zb(i,j) = -1 - par.i * i * dxStandard;	% added for joints
			elseif j <= nm	% added for joints
				zb(i,j) = -1 - par.i * nm * dxStandard - ...
						par.i * (pi/2) * 0.5 * (ri + ro) - ...
						par.i * (nm - j) * dy;
			else
				ang = pi/2 - atan((j-nm)/(i-nm));
				zb(i,j) = -1 - par.i * nm * dxStandard - par.i * ang * 0.5 * (ri + ro);
			end
		end
	end
end

% Flow in other direction
% for i = 1 : nx
	% for j = 1 : ny
		% if type(i,j) > 0
			% if j <= nm
				% zb(i,j) = -1 - par.i * j * dy;
			% elseif i <= nm
				% zb(i,j) = -1 - par.i * nm * dy - ...
						% par.i * (pi/2) * 0.5 * (ri + ro) - ...
						% par.i * (nm - i) * dxStandard;
			% else
				% ang = atan((j-nm)/(i-nm));
				% zb(i,j) = -1 - par.i * nm * dy - par.i * ang * 0.5 * (ri + ro);
			% end
		% end
	% end
% end

zb(:,ny+1) = 1.;
zb(nx+1,:) = 1.;

% End added

% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dxStandard = dxStandard;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.b = b;
s.ZBINIT = zb;
s.Th = Th;
s.type = type;
s.delta_u = delta_u;
s.delta_v = delta_v;

% Initial position of shoreline (additional code by Chien)
% Best to be predefined
% Here we take the intersection between bed and water surface
% THIS CODE USES INITIAL ZS, WHICH IS NOT OFFICIALLY DEFINED

if 0
	yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
	xx = [];           % cross-shore coordinates, can vary
	iiwet = [];     % list of positions of wet cells
	ny = s.ny;
	for i = 1 : ny+1
		le = 1; 	ri = ny;
		while s.zb(le,i)<0; le=le+1; end;
		while s.zb(ri,i)>0; ri=ri-1; end;
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0.05 - 0.1 * (i - 1)/ny); % par.zs0 for simple case
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0); % par.zs0 == 0
		xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0 - par.i * s.y(1,i)); % account for slope
	%   xi = interp1(s.zb(:,i),s.x(:,i),0.0,'nearest'); 
		% position of shoreline interpolate where zb = 0 ; this interpolation
		% function does not seem to work well
		% iwet = length(find(s.zb(:,i)<0));       % index of wet cell = number of cells with zb < 0
		iwet = length(find(s.zb(:,i)<0 - par.i * s.y(1,i)));       % index of wet cell = number of cells with zb < zs
		xx = horzcat(xx, [xi]);
		iiwet = horzcat(iiwet, [iwet]);
	end
	iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward

	% figure(1); clf; plot(xx, yy, 'g*-', 'linewidth',2);

	% Setting up the sh struct
	% sh is the structure containing data of shoreline particularly

	% these are spatial properties of shoreline
	sh.xL0 = [];	sh.yL0 = [];
	for i = 1:s.ny;
		sh.xL0 = horzcat(sh.xL0, [xx(i) xx(i)]);
		sh.yL0 = horzcat(sh.yL0, [yy(i) yy(i+1)]);
	end;

	sh.x0 = xx;
	sh.y0 = yy;

	% Alternately, we can define the coastline coordinates sh.x here
	sh.x = xx;
	sh.y = yy;
	sh.iiwet = iiwet;
	sh.iidry = iidry;
end;
sh.x = 0;
% End added