function [it,s]=output(it,s,par,sh)

if exist('OCTAVE_VERSION') == 0    % running in Matlab
	figure(1);
	% for i = 1 : s.nx
		% for j = 1 : s.ny
			% [xc, yc] = centroid(s.delta_u(i,j), s.delta_u(i+1,j), s.delta_v(i,j), s.delta_v(i,j+1));
			% s.xplot(i,j) = (i-1) * s.dxStandard + xc * s.dxStandard;
			% s.yplot(i,j) = (j-1)*s.dy + yc * s.dy;
		% end
	% end
	
	% Determine centroids of cut cells for plotting
	
	% xplot = s.x - s.dxStandard / 2;
	% yplot = s.y - s.dy / 2;
	% colormap(summer); pcolor(xplot, yplot, s.h .* (s.h < 1)); shading interp; 
	% contour(s.x,yplot,s.zb); 
	% hold on;
	% plot(sh.x0, sh.y0, 'k');
	% plot(sh.xL, sh.yL, 'k');
	% plot(sh.x, sh.y, 'k');
	% caxis([-5 0]);colorbar;
	% axis equal tight;
	% axis ([345 400 520 550]);
	% axis ([220 360 300 700]);
	% axis ([270 410 300 700]);
	% axis tight;
	% hold on;
	% quiver(s.x, s.y, s.u, s.v);
	% drawnow;
	% hold off;
	% the velocity field changes, so hold off has to be used so that
	% new velocities will not overlap old ones
	% drawnow;
	% fprintf('%3d %6.1f%6.1f\n', par.t, s.u(5,5), s.v(5,5));
	% disp(par.dt);
	% fprintf('%3d %6.1f%6.2f%6.1f%6.2f\n', par.t, s.u(3,10), s.v(3,10), s.u(5,3), s.v(5,3));
	% fprintf('%3d %6.1f %5.2f %6.2f%6.2f%6.2f%6.2f%6.2f%6.2f%6.2f\n', par.t, sh.x(21), sh.v(21), s.dx(35:41,21));
	grid_flow_field;
else    % running in Octave
	disp('now in Octave');
end