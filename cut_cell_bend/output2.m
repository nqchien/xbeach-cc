function [it,s]=output(it,s,par,sh)
    if exist('OCTAVE_VERSION') == 0    % running in Matlab
		fprintf ('%5d', par.t)
		fprintf ('%7.1f', sh.x)
		fprintf ('\n')
            %~ figure(1);
%             pcolor(s.x,s.y,s.zb); shading interp; grid
            %~ hold on;
            %~ contour(s.x,s.y,s.zb); 
            %~ plot(sh.x, sh.y, 'k'); 
            %~ grid on;
            %~ caxis([-5 0]);colorbar;axis equal; axis tight;
%             hold on;
%             quiver(s.x,s.y,s.u,s.v);
%             drawnow;
            %~ hold off;
            % the velocity field changes, so hold off has to be used so that
            % new velocities will not overlap old ones
%         drawnow;
%         disp(s.dx(sh.iiwet(20),20));
    else    % running in Octave
        disp('now in Octave');
    end
