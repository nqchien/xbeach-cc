function [it,s]=output(it,s,par,sh)

if exist('OCTAVE_VERSION') == 0    % running in Matlab
	figure(1);
	% pcolor(s.x,s.y,s.zb); shading interp; grid
	% contour(s.x,s.y,s.zb); 
	hold on;
	plot(sh.x, sh.y, 'k'); 
	% caxis([-5 0]);colorbar;
	axis equal; % axis tight;
	% hold on;
	% quiver(s.x,s.y,s.u,s.v);
	% drawnow;
	% hold off;
	% the velocity field changes, so hold off has to be used so that
	% new velocities will not overlap old ones
	drawnow;
	fprintf('%3d %6.1f%6.2f%6.1f%6.2f\n', par.t, sh.x(10), sh.v(10), sh.x(30), sh.v(30));
else    % running in Octave
	disp('now in Octave');
end