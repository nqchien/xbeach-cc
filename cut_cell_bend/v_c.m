function ret = v_c(tau_c)

% v_c(tau_c) calculates the (critical) near-bank velocity corresponding to
% the (critical) near-bank shear stress using Lane's formula
% tau = 0.75 * rho * g * u^2 / C^2
% with Chezy coefficient C = 65 m^0.5 / s.

ret = 65 * sqrt(tau_c) / sqrt(0.75 * 9810);
