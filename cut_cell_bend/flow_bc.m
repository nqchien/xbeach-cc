function [s] = flow_bc (s, par)     % (s,par,it)
% zs  = s.zs;
dy = s.dy;

ui = 0;
h = s.h;		i = par.i;	C = par.C;

% Diagonal, narrow channel
% SW
% s.vv(1,1) = C * sqrt (h(1, 1) * i) / sqrt(2);
% s.uu(1,1) = C * sqrt (h(1, 1) * i) / sqrt(2);
% NE
% s.vv(end,end-1) = C * sqrt (h(end,end-1) * i) / sqrt(2);
% s.uu(end,end-1) = C * sqrt (h(end,end-1) * i) / sqrt(2);

% Bended channel
% with joints  31:35 instead of 26:30
q0 = .0617;
st = 36;
e = 40;
vnorm = q0 ./ h(1,st:e);
s.uu(1, st:e) = vnorm;
s.vv(1, st:e) = 0;

% vnorm = C * sqrt(h(st:e, 1) * par.i);
% s.vv(st:e, 1) = -vnorm;
% s.vv(st:e, 1) = s.vv(st:e,2);
% s.uu(st:e, 1) = 0;

%% zs(1, st:e) = zs(2, st:e) + s.dxStandard * par.i;
%% zs(st:e ,1) = zs(st:e, 2) - dy * par.i;
s.zs(1, st:e) = s.zs(2, st:e) - s.dxStandard * par.i;
s.zs(st:e ,1) = s.zb(st:e, 1) + 0.116;
%% zs(st:e ,1) = zs(st:e, 2) + s.dxStandard * par.i;

% Flow in the other direction
% vnorm = q0 ./ h(st:e,1);
% s.uu(st:e, 1) = 0;
% s.vv(st:e, 1) = vnorm;
% s.zs(st:e, 1) = s.zs(st:e, 2) + s.dy * par.i;
% s.zs(1, st:e) = s.zb(1, st:e) + 0.116;

% vnorm = C * sqrt(h(st:e, 1) * par.i);
% s.uu(1, st:e) = -vnorm;
% s.vv(1, st:e) = 0;
% s.uu(1, st:e) = s.uu(2, st:e);

% s.zs  = zs;

% 175 cells, dx = 0.1 m
% q0 = .0617 * dy;
% vnorm = q0 ./ h(1,151:175);
% s.uu(1, 151:175) = vnorm;
% s.vv(1, 151:175) = 0;

% vnorm = C * sqrt(h(151:175, 1) * i);
% s.vv(151:175, 1) = -vnorm;
% s.uu(151:175, 1) = 0;

% zs(1, 151:175) = zs(2, 151:175) + s.dxStandard * i;
% zs(151:175 ,1) = zs(151:175, 2) - dy * i;

% s.zs  = zs;

