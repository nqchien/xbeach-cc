function retval = area_tri(cA, cB, cC);

xA = cA(1);	yA = cA(2);
xB = cB(1);	yB = cB(2);
xC = cC(1);	yC = cC(2);

retval = 0.5 * abs(xA*yC - xC*yA + xB*yA - xA*yB + xC*yB - xB*yC);
