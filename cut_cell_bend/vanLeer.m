function ret = vanLeer(a,b);

eps = 1E-6;
a = a + eps;	b = b + eps;
ret = (a .* abs(b) +  abs(a) .* b) ./ (abs(a) + abs(b));
