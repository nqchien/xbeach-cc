%-- Unknown date --%
0 1 -1 0 0 0], ...
[-0.2 0.1 -0.4 0 0 0]')
u = 0.1182 * x + 0.3273 * y - 0.4491;
v = 0.3273 * x - 0.3727 * y - 0.0636;
u = u .* (y <= 0.5*x + 0.3);
v = v .* (y <= 0.5*x + 0.3);
quiver(x,y,u,v)
axis equal tight
xlabel('x / \Deltax'); ylabel('y / \Deltay');
hold on
xbound = [0 1]; ybound = [0.3 0.8];
plot(xbound, ybound, 'k', 'linewidth',2)
plot(xcell, ycell, 'k', 'linewidth',2)
xlabel('x / \Deltax'); ylabel('y / \Deltay');
plot(xcell, ycell, 'k', 'linewidth',2)
xcell = [0 1 1 0 0]; ycell = [0 0 1 1 0];
plot(xcell, ycell, 'k', 'linewidth',2)
figure;
xbound = [0 0.5]; ybound = [0.4 0];
gcf; hold on;
plot(xbound, ybound, 'k')
plot(xcell, ycell, 'k', 'linewidth',2)
quiver(x,y,u,v)
axis equal tight
xlabel('x / \Deltax'); ylabel('y / \Deltay');
linsolve([0 0.7 0 0 1 0; ...
1 0.5 0 0 1 0; ...
0 0 0.75 0 0 1; ...
0 0 0.5 1 0 1; ...
20 -16 25 -20 0 0; ...
0 8 0 10 20 25], ...
[0.4 1 0.7 -0.5 0 0]')
u = -1.5 + 1.1429*x + 2.7143*y;
v = 0.8286 - 0.1714*x - 1.2429*y;
clf
quiver(x,y,u,v)
hold on
plot(xbound, ybound, 'k')
plot(xcell, ycell, 'k', 'linewidth',2)
u = u .* (y >= -0.8 * x + 0.4);
v = v .* (y >= -0.8 * x + 0.4);
quiver(x,y,u,v)
axis equal tight
xlabel('x / \Deltax'); ylabel('y / \Deltay');