function [xc, yc] = centroid(du1, du2, dv1, dv2);

xA = 0;	yA = 0; xB = 0; yB = 0; xC = 0; yC = 0; xD = 0; yD = 0;

if du1 > 0
	xA = -0.5;	yA = 0.5;
	xD = -0.5;	yD = 0.5 - du1;
elseif du1 < 0
	xD = -0.5;	yD = -0.5;
	xA = -0.5;	yA = -0.5 + du1;
end;

if du2 > 0
	xB = 0.5;	yB = 0.5;
	xC = 0.5;	yC = 0.5 - du2;
elseif du2 < 0
	xC = 0.5;	yC = -0.5;
	xB = 0.5;	yB = -0.5 + du2;
end;

if dv1 > 0
	xC = 0.5;	yC = -0.5;
	xD = 0.5 - dv1;	yD = -0.5;
elseif dv1 < 0
	xD = -0.5;	yD = -0.5;
	xC = -0.5 + dv1;	yC = -0.5;
end;

if dv2 > 0
	xB = 0.5;	yB = 0.5;
	xA = 0.5 - dv2;	yA = 0.5;
elseif dv2 < 0
	xA = -0.5;	yA = 0.5;
	xB = -0.5 + dv2;	yB = 0.5;
end;

if (du1^2 + dv1^2 + du2^2 + dv2^2 == 0)
	xA = -0.5;	xB = 0.5;	xC = 0.5;	xD = -0.5;
	yA = 0.5;	yB = 0.5;	yC = -0.5;	yD = -0.5;
end

xc = (xA + xB + xC + xD) / 4;
yc = (yA + yB + yC + yD) / 4;
