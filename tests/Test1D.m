classdef Test1D < matlab.unittest.TestCase
    %Test1D Test multiple functions in the 1D-code folder 
    %   e.g. tau_c, to calculate the shear stress of mixed soil
    %   following Julian and Torres (2006) Geomorphology 76
    
    properties
        OriginalPath
    end

    methods (TestMethodSetup)
        function add1DcodeToPath(testCase)
            testCase.OriginalPath = path;
            addpath(fullfile(pwd, '..', '1D-code'));
        end
    end
    
    methods (TestMethodTeardown)
        function restorePath(testCase)
            path(testCase.OriginalPath);
        end
    end
    
    methods (Test)
        function test_tau_c_SC_zero(testCase)
            %test_tau_c_SC_zero Test the case SC=0
            %   i.e. for pure sand
            tau_c_calc = tau_c(0);
            tau_c_expt = 0.1;
            testCase.verifyEqual(tau_c_calc, tau_c_expt, 'AbsTol', 1e-5);
        end

        function test_tau_c_SC_medium(testCase)
            %test_tau_c_SC_max Test the case SC = 50 percent
            %   i.e. for silt-clay sample
            tau_c_calc = tau_c(50);
            tau_c_expt = 13.07;
            testCase.verifyEqual(tau_c_calc, tau_c_expt, 'AbsTol', 1e-5);
        end
        
        function test_tau_c_SC_max(testCase)
            %test_tau_c_SC_max Test the case SC = 100 percent
            %   i.e. for silt-clay sample
            tau_c_calc = tau_c(100);
            tau_c_expt = 22.49;
            testCase.verifyEqual(tau_c_calc, tau_c_expt, 'AbsTol', 1e-5);
        end
    end
end

