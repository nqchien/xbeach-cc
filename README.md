# README #

The repo contains cut-cell implementation of the model [XBeach](http://xbeach.org) for modelling various coastal morphology processes including dune erosion, wave overwash during storms.

Each folder include a specific test case:

* `1D-code`: one-dimensional shoreline retreat/erosion due to alongshore current
* `2D-hump`: erosion of a Gaussian hump-shaped shoreline
* `2D-hump`: erosion of a triangular promontory
* `cut_cell_hump`: XBeach cut cell implementation for a hump-shaped shoreline erosion
* `cut_cell_bend`: XBeach cut cell implementation for a 90° bend channel
* `cut_cell_channel`: XBeach cut cell implementation for a straight channel aligned diagonal to grid
* `square_cell_bend` and `square_cell_channel`: regular XBeach simulation for the corresponding cases
* `Namdinh`: regular XBeach application for the case of Namdinh shoreline.
