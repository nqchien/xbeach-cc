function [s, sh, ZBINIT] = grid_bathy(par)

h0 = 4;
hmin = 0.001;
nx = 100; % 10;
ny = 55; %110 20;
s.dxStandard = 10.;

dx = zeros(nx+1,ny+1) + s.dxStandard;     % dx is now an array

dy = 10.; %20.

% shore_shape = input('Shape of shore - Straight (default), [H]umped, [G]roined : ', 's');
% bank_elev = input('Bank elevation? [A number] / Inf (default) for none: ');
shore_shape = '';
%
% Set original water depth
%
% Pre-allocation
x = zeros(nx+1,ny+1);
y = zeros(nx+1,ny+1);
zb = zeros(nx+1,ny+1) + 1.;
a = ones(nx+1, ny+1);

alph = 22.8 * pi /180;
y1 = (0:100) * tan(alph);	% 22.8
% y2 = y1 + 10 * dy;
% d1 = (dy - mod(y1,dy)) / dy;
% d2 = mod(y2,dy) / dy;

% Slope
slope_t = 0.01;	% transversal slope
for j=1:ny+1;
    for i=1:nx+1;
        x(i,j) = (i-1) * dx(i,j);          % a = 1
        y(i,j) = (j-1) * dy;
        % h(i,j)=max(h0*(1-(i-1)/nx),.1);
        % zb(i,j)=-h0*(1-(i-1)/(nx-5));
% 		zb(i,j) = -h0 + x(i,j) * slope_t;
    end;
end

for i = 1 : nx+1
	jstart = floor(y1(i)) + 2;
	for j = jstart : jstart+9
		zb(i,j) = -1 - par.i * x(i,jstart) / cos(alph) - par.i * (j-jstart) * dy * sin(alph);
	end
end

% zb(1:10,2:11) = -1 - par.i * x(1:10,2:11);
% zb(112:end,44:53) = -1 - par.i * x(112:end,44:53);

% Triangular hump
% for i = 6:10;
%     zb(i, 11-(i-6):11+(i-6)) = 1;
% end;
% zb(11,:) = 1.;

% Island
% zb(5,9:11) = 1.;
% zb(6,8:13) = 1.;
% zb(7,9:11) = 1.;

% Longitudinal slope
% zb = zb - par.i * x;

if strcmp(upper(shore_shape), 'G');
% Groin (rectangular)
	highest = zb(end,1);
	zb (21:30, 19:21) = 0.5;
end;

if strcmp(upper(shore_shape), 'H');
	% Hump dimensions
	height=3;
	R=180;
	x0=200;
	y0=500;
	% h=h-height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	zb=zb+height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	% zb=zb+height.*exp(-((x-x0).^2)/R^2);
end;

% check with horizontal bottom
% h=max(h,h0);

% if bank_elev ~= Inf;
	% zb(31:end,:) = bank_elev; 	% bank
	% zb(:,18:22) = -5; trench
% end;

% Added  -- simple pool, no erosion
% A hump from cellx = 32 -- 35 and celly = 11 -- 40
% zb = zeros(nx+1, ny+1) - 2.;
% zb(37:end,:) = 1;
% zb(36,15:26) = 1;
% zb(35,17:24) = 1;
% zb(34,20:21) = 1;
% zb(32:33,20:21) = 1;
% dx(36,12) = .9 * s.dxStandard;
% dx(36,13) = .7 * s.dxStandard;
% dx(36,14) = .3 * s.dxStandard;
% dx(35,15) = .8 * s.dxStandard;
% dx(35,16) = .2 * s.dxStandard;
% dx(34,17) = .7 * s.dxStandard;
% dx(34,18) = .3 * s.dxStandard;
% dx(34,19) = .1 * s.dxStandard;
% mirror
% for j = 21:30;
	% dx(34:36, j) = dx(34:36, ny+1-j);
% end;

% zb(37:end,:) = 1;
% dx(36,21) = 5.;
% zb(37,12:29) = -2;
% zb(38,15:26) = -2;
% zb(39,17:24) = -2;
% zb(40,20:21) = -2;
% zb(40,20:21) = -2;
% dx(37,12) = .1 * s.dxStandard;
% dx(37,13) = .3 * s.dxStandard;
% dx(37,14) = .7 * s.dxStandard;
% dx(38,15) = .2 * s.dxStandard;
% dx(38,16) = .8 * s.dxStandard;
% dx(39,17) = .3 * s.dxStandard;
% dx(39,18) = .7 * s.dxStandard;
% dx(39,19) = .9 * s.dxStandard;
% for j = 21:30;
	% dx(37:39, j) = dx(37:39, ny+1-j);
% end;

% End added

ZBINIT = zb;
% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.a    = a;

% Initial position of shoreline (additional code by Chien)
% Best to be predefined
% Here we take the intersection between bed and water surface
% THIS CODE USES INITIAL ZS, WHICH IS NOT OFFICIALLY DEFINED

yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
yy = yy - s.dy/2;
% xx = [];           % cross-shore coordinates, can vary
% iiwet = [];     % list of positions of wet cells
% ny = s.ny;
% for i = 1 : ny+1
%     le = 1; 	ri = ny;
%     while s.zb(le,i)<0; le=le+1; end;
%     while s.zb(ri,i)>0; ri=ri-1; end;
%     % xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0.05 - 0.1 * (i - 1)/ny); % par.zs0 for simple case
%     % xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0); % par.zs0 == 0
%     xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0 - par.i * s.y(1,i)); % account for slope
% %   xi = interp1(s.zb(:,i),s.x(:,i),0.0,'nearest'); 
%     % position of shoreline interpolate where zb = 0 ; this interpolation
%     % function does not seem to work well
%     % iwet = length(find(s.zb(:,i)<0));       % index of wet cell = number of cells with zb < 0
%     iwet = length(find(s.zb(:,i)<0 - par.i * s.y(1,i)));       % index of wet cell = number of cells with zb < zs
%     xx = horzcat(xx, [xi]);
%     iiwet = horzcat(iiwet, [iwet]);
% end
% iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward

% figure(1); clf; plot(xx, yy, 'g*-', 'linewidth',2);

% Setting up the sh struct
% sh is the structure containing data of shoreline particularly

% % For the hydrodynamic part only;
% % triangular hump
% xx = [100 100 100 100 100 100 90 80 70 60 50 60 70 80 90 100 100 100 100 100 100]; 
% xx = xx - s.dxStandard/2;
% these are spatial properties of shoreline

% % for hump
% sh.xL0 = [];	sh.yL0 = [];
% for i = 1:s.ny;
% 	sh.xL0 = horzcat(sh.xL0, [xx(i) xx(i)]);
% 	sh.yL0 = horzcat(sh.yL0, [yy(i) yy(i+1)]);
% end;

% for island 
sh.xL0 = [5 6 6 7 7 8 8 7 7 6 6 5 5] * s.dxStandard - 3 * s.dxStandard/2;
sh.yL0 = [9 9 8 8 9 9 12 12 14 14 12 12 9] * s.dy - 3 * s.dy / 2;
% sh.x0 = xx;
% sh.y0 = yy;

% Alternately, we can define the coastline coordinates sh.x here
% sh.x = xx;
% sh.y = yy;
% sh.iiwet = iiwet;
% sh.iidry = iidry;

% End added