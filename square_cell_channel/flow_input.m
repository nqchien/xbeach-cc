function [par] = flow_input(par);
par.C = 70;
par.eps = 1e-1;
par.g = 9.81;
par.umin = .1;
par.zs0 = -.4;
par.tstart = 0;
par.tint = 2;
par.tstop = 1200;
par.CFL = 0.2;

% Added
par.v0 = 1.;
par.i = 1.E-3;
