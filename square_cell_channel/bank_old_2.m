function [sh,s] = bank(sh,s,a)

% BANK calculate the retreat of bank and plot them as
% multiple polylines over time

x = sh.x;
y = sh.y;
a = s.a;
dx = sh.dx;
dy = sh.dy;
Dt = sh.Dt;
u = sh.u;
zb = sh.zb;
zbank = sh.zbank;

Ngrids = length(x);
S0 = zeros(Ngrids,1) + 4E-4;
kV = zeros(Ngrids,1) + 1E-4;
ks = zeros(Ngrids,1) + 1;
vcrit = zeros(Ngrids,1) + 0.14;
E = zeros(Ngrids,1) + 0.2; % 0.05
G = zeros(Ngrids,1) + 0.1; % sloping beach
kp = zeros(Ngrids,1);
L = zeros(Ngrids-1,1);
alpha = zeros(Ngrids-1,1);
Awet = zeros(Ngrids-1,1);
Vp = zeros(Ngrids-1,1);
Dnb = zeros(Ngrids,1);
Hfb = zeros(Ngrids,1);
hw = zeros(Ngrids,1);
Hc = zeros(Ngrids,1) + 1.;
DV = zeros(Ngrids-1,1);
C = zeros(Ngrids-1,1);
eta = zeros(Ngrids-1,1);
DAwet = zeros(Ngrids-1,1);
Dzb = zeros(Ngrids-1,1);

% alpha, eta, L, Awet, DAwet, DV, C, Dzb,
% Vp are matrices containing Ngrids-1 elements.
% gcf;
%     u=4./y;  %  ?
for k = 2 : Ngrids
    kp(k) = 0.01; %exp(-y(k)/dy);
    L(k)=sqrt((y(k-1)-y(k))^2+dx(k)^2);
    alpha(k)= atan((y(k-1)-y(k))/dx(k));
    cosa = cos(alpha(k));        
    Awet(k)= (y(k-1)+y(k))/2 * dx(k);
    Vp(k)= (u(k-1)+u(k))/2/cosa;
    Dnb(k) = 0;
    if Vp(k) > vcrit(k)
        Dnb(k) = E(k)*(Vp(k)^2 - vcrit(k)^2)/(vcrit(k)^2);
    end
    Hfb(k) = zbank(k) - 0;   % 0 is the water level
    hw(k) = 0 - zb(k);
    if hw(k) + Hfb(k) > Hc(k);
        Dnb(k) = Dnb(k) + G(k)*(hw(k) + Hfb(k) - Hc(k))/ Hc(k);
    end
    DV(k) = Dnb(k) / cosa * (zbank(k) - zb(k)) * dx(k);
%   DV(k)=kV(k)*ks(k)*kp(k)* Vp(k) * L(k) * Dt;
    C(k) = 2 * Dnb(k) / cosa;
    % C(k)=2*DV(k)/((zbank(k)-zb(k))*dx(k));
end
eta(1)=C(2)/2;
y(1)=y(1)+eta(1);
for k = 2 : Ngrids
    eta(k) = C(k) - eta(k-1);
    DAwet(k)=(eta(k-1)+eta(k))*L(k)/2.;
    Dzb(k) = (S0(k) * Dt + DV(k)) / (Awet(k) + DAwet(k));
    Awet(k) = Awet(k) + DAwet(k);
    y(k) = y(k) + eta(k);
    zb(k) = zb(k) + Dzb(k);
end

% hold on; plot(y,x,'k'); % drawnow; hold off; % break here

% WE HAVE TO UPDATE THE sh.dy VALUES BASED ON Dnb (i.e. +/- Dnb)
% then let s.dx = sh.dy

% Update the sh structure
sh.y = y;   sh.zb = zb;   sh.Dnb = Dnb;     sh.eta = eta;
sh.Dzb = Dzb;   sh.DAwet = DAwet;   sh.Awet = Awet;
