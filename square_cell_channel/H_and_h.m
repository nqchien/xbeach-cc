figure(2)
plot(s.H(:,20))
hold on
plot(s.h(:,20),'r')
xlabel('x grid NR')
ylabel('Depth and wave height (m)')
legend('H(m)','h(m)')
