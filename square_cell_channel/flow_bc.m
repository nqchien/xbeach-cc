function [s] = flow_bc (s, par)     % (s,par,it)
zs  = s.zs;
uu  = s.uu;
% added for bound long wave bc Ad 27 march 2006
% no need ?     ui  = s.ui;
hh  = s.h;
%
% Sea boundary at x=0;
%
% zs(1,:)  = 0;  % old boundary
% 
% new boundary (not perfect, ignores direction of incoming free long wave
% and also the difference in velocity response for free and bound long
% waves) Ad 27 march 2006
%
  % no need ?
%   g=9.81;
%   omega=2*pi/par.Tlong;
%   dt=par.dt;
%   t=par.t;
%   dx=s.dx;
  %ui=uin*sin(omega*t);
ui = 0;
%   uu(1,:)=2*ui -sqrt(9.81./hh(1,:)).*(zs(2,:)-par.zs0);

% Added by Chien

% Simple linear velocity distribution
% v0 = par.v0;
% s.vv(1:20,1) = v0;
% s.vv(1:20,end) = v0;
% s.vv(21:36,1) = linspace(v0,v0/2,16)';
% s.vv(21:36,end) = linspace(v0,v0/2,16)';

% for i = 37 : s.nx+1
    % if s.wetv(i,1)==1 ; s.vv(i,1) = v0/2; end;
    % if s.wetv(i,end)==1 ; s.vv(i,end) = v0/2; end;
% end;

% New velocity distribution v = C.sqrt(hi)
q0 = 1.;
alph = 22.8 * pi / 180;
h = s.h;	i = par.i;	C = par.C;
% s.uu(1,:) = C * sqrt(h(1,:) * i);
% s.uu(end,:) = C * sqrt(h(end,:) * i);
vnorm = q0 ./ h(1,2:11);
if h(1,2:11) > par.hmin
	s.uu(1,2:11) = vnorm * cos(alph);
	s.vv(1,2:11) = vnorm * sin(alph);
end

% vnorm = C * sqrt(h(end,44:54) * i);
% if h(end,44:54) > par.hmin
	% s.uu(end,44:53) = q0 ./ h(end,44:53);
	% s.uu(end,44:53) = C * sqrt(h(end,44:53) * i);
	% s.vv(end,44:53) = 0;
	% s.uu(end,44:54) = vnorm * cos(alph);
	% s.vv(end,44:54) = vnorm * sin(alph);
% end

s.vv(:,1) = 0;
s.vv(:,end) = 0;

% s.vv(:, 1) = C * sqrt (h(:, 1) * i);
% s.vv(:, end) = C * sqrt (h(:, end) * i);

% End added

% uu(1,:) = 0;
% Lateral boundary at y=0;	(Neumann)
%
% zs(2:end,1)=zs(2:end,2); % + s.dy * par.i;
% zs(:,1)=zs(:,2); % + s.dy * par.i;
% zs(1,:) = zs(2,:) + (s.dxStandard * cos(alph)) * i;
%
% Lateral boundary at y=ny*dy;
%
% zs(2:end,end)=zs(2:end,end-1); %  - s.dy * par.i;
% zs(:,end)=zs(:,end-1); %  - s.dy * par.i;
% zs(end,44:53) = zs(end-1,43:52) - (s.dxStandard * cos(alph)) * i;	% Neumann
zs(end,44:53) = s.zb(end,44:53) + 0.618;	% Dirichlet

%
% Land boundary at x=nx*dx;
%
% uu(end,:)=0;
%
% Output
%
s.zs  = zs;
