function [s,sh] = shoreline2(s,sh,par)

% SHORELINE defines the position of shoreline in model and 
% calculate the rate of retreat of shoreline

jj = 1 : 1 : s.ny + 1;

size = length(s.wetv);
bankpos = zeros(size,1);
v = zeros(size,1);
zb = zeros(size,1);
zbank = zeros(size,1);
for i = 1:size
%     bankpos(i) = sum(s.wetv(:,i)) + 1;      % index of bank position in x-dir
%     if i == size ; bankpos(i) = bankpos(i-1); end;
%     sh.iiwet(i) = bankpos(i) - 1;         % do not update iiwet here
%     sh.iidry(i) = bankpos(i);
    zb(i) = s.zb(sh.iiwet(i), jj(i));       % bed  level is at wet cell
    zbank(i) = s.zb(sh.iidry(i), jj(i));    % bank level is at dry cell
    v(i) = s.vv(sh.iiwet(i), jj(i));        % velocity "adjacent" to shore
	zs(i) = s.zs(sh.iiwet(i), jj(i));
end

v(end) = par.C * sqrt(s.h(sh.iiwet(end), end) * par.i);

% Adding properties to sh struct
sh.v = v;
sh.dx = s.dx;
sh.dy = s.dy;
sh.Dt = par.dt * par.morfac;

% Calculate shoreline retreat
x = sh.x;
y = sh.y;
dx = sh.dx;
dy = sh.dy;
Dt = sh.Dt;

Ngrids = length(x);         % NR grid cells of coast
S0 = zeros(Ngrids,1);	% sed. transport (4E-4)
vcrit = v_c(1.97 * tau_c(0.1));               % critical velocity for erosion
E = 6E-8;                   % empirical coeff for erosion due to flow
G = 0;                    % empirical coeff for erosion due to collapsion
                            % of high banks
Awet = zeros(Ngrids-1,1);   % wet area of interface cell
vmid = zeros(Ngrids-1,1);   % average velocity at middle of cell
Dnb = zeros(Ngrids,1);      % retreat distance, normal to shore
Hfb = zeros(Ngrids,1);      % freeboard height of coast (above MSL = 0)
hw = zeros(Ngrids,1);       % depth of water right at shoreline
Hc = zeros(Ngrids,1) + 1.;  % (empirical) critical depth (Mosselman)
DV = zeros(Ngrids-1,1);     % change in volume of sediment
DAwet = zeros(Ngrids-1,1);  % change in wet area
Dzb = zeros(Ngrids-1,1);    % change in bed elevation

v(Ngrids+1) = v(Ngrids);
for k = 1 : Ngrids
    Awet(k) = s.dx(sh.iiwet(k),k) * dy;
    vmid(k) = (v(k)+v(k+1))/2;
    if abs(vmid(k)) > vcrit;
        Dnb(k) = E*(vmid(k)^2 - vcrit^2)/(vcrit^2);
    end
    Hfb(k) = zbank(k) - zs(k);   % zs was set to 0 previously
    hw(k) = zs(k) - zb(k);
    if hw(k) + Hfb(k) > Hc(k);
        Dnb(k) = Dnb(k) + G*(hw(k) + Hfb(k) - Hc(k))/ Hc(k);
    end
    Dnb(k) = Dnb(k) * Dt;
    DV(k) = Dnb(k) * (zbank(k) - zb(k)) * dy;
end

for k = 1 : Ngrids
	DAwet(k) = Dnb(k) * dy;
	Dzb(k) = (S0(k) * Dt + DV(k)) / (Awet(k) + DAwet(k));
	Awet(k) = Awet(k) + DAwet(k);
	x(k) = x(k) + Dnb(k);
	zb(k) = zb(k) + Dzb(k);
	% In case of non-resizing case, comment the following lines
	s.dx(sh.iiwet(k),k) = s.dx(sh.iiwet(k),k) + Dnb(k);
    s.dx(sh.iidry(k),k) = s.dx(sh.iidry(k),k) - Dnb(k);
	
	% Approach 1 -- not yet successful
	if s.dx(sh.iiwet(k),k) > s.dxStandard;
		olddxwet = s.dx(sh.iiwet(k),k);
		sh.iiwet(k) = sh.iidry(k);
		sh.iidry(k) = sh.iidry(k) + 1;
		s.dx(sh.iiwet(k)-1,k) = s.dxStandard;
		fprintf('%3d%6.2f\n', par.t, olddxwet);
		s.dx(sh.iiwet(k),k) = olddxwet - s.dxStandard;
		s.zb(sh.iiwet(k),k) = zb(k);
		s.vv(sh.iiwet(k),k) = v(k);
	end;
	
	% Approach 2
    % if s.dx(sh.iiwet(k),k) > 1.5 * s.dxStandard;
		% olddxwet = s.dx(sh.iiwet(k),k);
		% olddxdry = s.dx(sh.iidry(k),k);
		% oldu = s.u(sh.iiwet(k),k);
		% oldzs = s.zs(sh.iiwet(k),k);
		% sh.iiwet(k) = sh.iiwet(k) + 1;
		% sh.iidry(k) = sh.iidry(k) + 1;
		% s.dx(sh.iiwet(k)-1,k) = s.dxStandard;
		% s.dx(sh.iiwet(k),k) = olddxwet - s.dxStandard;
		% s.dx(sh.iidry(k),k) = s.dxStandard + olddxdry;
		% s.zb(sh.iiwet(k),k) = zb(k);
		% s.vv(sh.iiwet(k),k) = v(k);
		% s.u(sh.iiwet(k),k) = oldu / 3;
		% s.zs(sh.iiwet(k),k) = oldzs;
	% end;
	%
    % s.x = (cumsum(s.dx))';	enabling this will spoil the plotting commands: quiver, pcolor and contour
end

xL = [];	yL = [];
for i = 1:s.ny;
	xL = horzcat(xL, [x(i) x(i)]);
	yL = horzcat(yL, [y(i) y(i+1)]);
end;

% Update the sh structure
sh.x = x;   sh.y = y;   sh.zb = zb;   sh.Dnb = Dnb; 
sh.Dzb = Dzb;   sh.Awet = Awet;   sh.DAwet = DAwet;
sh.xL = xL;	sh.yL = yL;
% [s,sh] = bank(s,sh);