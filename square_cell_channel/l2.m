function ret = l2(alist, exact);

% l2 Calculates the relative l2 error compared to exact value

ret = 0;
for i = 1:numel(alist);
	ret = ret + (alist(i) / exact - 1)^2;
end
ret = sqrt(ret) / numel(alist);
