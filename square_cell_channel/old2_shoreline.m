function [s,sh] = shoreline(s,sh,par)

% SHORELINE defines the position of shoreline in model and 
% calculate the rate of retreat of shoreline

jj = 1 : 1 : s.ny + 1;

size = length(s.wetv);
bankpos = zeros(size,1);
v = zeros(size,1);
zb = zeros(size,1);
zbank = zeros(size,1);
for i = 1:size
%     bankpos(i) = sum(s.wetv(:,i)) + 1;      % index of bank position in x-dir
%     if i == size ; bankpos(i) = bankpos(i-1); end;
%     sh.iiwet(i) = bankpos(i) - 1;         % do not update iiwet here
%     sh.iidry(i) = bankpos(i);
    zb(i) = s.zb(sh.iiwet(i), jj(i));       % bed  level is at wet cell
    zbank(i) = s.zb(sh.iidry(i), jj(i));    % bank level is at dry cell
    v(i) = s.vv(sh.iiwet(i), jj(i));        % velocity "adjacent" to shore
end

% Adding properties to sh struct
sh.v = v;

sh.dx = s.dx;
sh.dy = s.dy;
sh.Dt = par.dt;

% Calculate shoreline retreat
dxStandard = 10.;           % "original" dx

x = sh.x;
y = sh.y;
dx = sh.dx;
dy = sh.dy;
Dt = sh.Dt;

Ngrids = length(x);         % NR grid cells of coast
S0 = zeros(Ngrids,1) + 4E-4;% sed. transport
vcrit = 0.14;               % critical velocity for erosion
E = 0.05;                   % empirical coeff for erosion due to flow
G = 0.1;                    % empirical coeff for erosion due to collapsion
                            % of high banks
L = zeros(Ngrids-1,1);      % length of shoreline within 1 cell
alpha = zeros(Ngrids-1,1);  % angle between shoreline and y-dir
Awet = zeros(Ngrids-1,1);   % wet area of interface cell
vmid = zeros(Ngrids-1,1);   % average velocity at middle of cell
Dnb = zeros(Ngrids,1);      % retreat distance, normal to shore
Dnbx = zeros(Ngrids-1,1);   % retreat distance, in x direction
Hfb = zeros(Ngrids,1);      % freeboard height of coast (above MSL = 0)
hw = zeros(Ngrids,1);       % depth of water right at shoreline
Hc = zeros(Ngrids,1) + 1.;  % (empirical) critical depth (Mosselman)
DV = zeros(Ngrids-1,1);     % change in volume of sediment
eta = zeros(Ngrids-1,1);    % shift of coastline in y direction
DAwet = zeros(Ngrids-1,1);  % change in wet area
Dzb = zeros(Ngrids-1,1);    % change in bed elevation

for k = 2 : Ngrids
    L(k) = sqrt((x(k-1)-x(k))^2 + dy^2);
    alpha(k) = atan((x(k-1)-x(k))/dy);
    cosa = cos(alpha(k));
    Awet(k) = (x(k-1)+x(k))/2 * dy;
    vmid(k) = (v(k-1)+v(k))/2/cosa;
    Dnb(k) = 0;
    if vmid(k) > vcrit;
        Dnb(k) = E*(vmid(k)^2 - vcrit^2)/(vcrit^2);
    end
    Hfb(k) = zbank(k) - 0;   % 0 is the water level
    hw(k) = 0 - zb(k);
    if hw(k) + Hfb(k) > Hc(k);
        Dnb(k) = Dnb(k) + G*(hw(k) + Hfb(k) - Hc(k))/ Hc(k);
    end
    Dnb(k) = Dnb(k) * Dt;
    DV(k) = Dnb(k) / cosa * (zbank(k) - zb(k)) * dy;
    Dnbx(k) = Dnb(k) / cosa;
end
eta(1) = Dnbx(2);
x(1) = x(1) + eta(1);
for k = 2 : Ngrids
    eta(k) = 2 * Dnbx(k) - eta(k-1);
    DAwet(k)=(eta(k-1)+eta(k))*L(k)/2.;
    Dzb(k) = (S0(k) * Dt + DV(k)) / (Awet(k) + DAwet(k));
    Awet(k) = Awet(k) + DAwet(k);
    x(k) = x(k) + eta(k);
    zb(k) = zb(k) + Dzb(k);
    s.dx(sh.iiwet(k),k) = s.dx(sh.iiwet(k),k) + 0.5*(eta(k-1)+eta(k));
    s.dx(sh.iidry(k),k) = s.dx(sh.iidry(k),k) - 0.5*(eta(k-1)+eta(k));
    if s.dx(sh.iiwet(k),k) > 1.5 * dxStandard;
        sh.iiwet(k) = sh.iidry(k);
        sh.iidry(k) = sh.iidry(k) + 1;
        s.dx(sh.iiwet(k)-1,k) = dxStandard;
        s.dx(sh.iiwet(k),k) = dxStandard - s.dx(sh.iiwet(k),k);
        s.dx(sh.iidry(k),k) = dxStandard + s.dx(sh.iiwet(k),k);
        s.zb(sh.iiwet(k),k) = zb(k);
    end;
    s.x = (cumsum(s.dx))';
end

% Update the sh structure
sh.x = x;   sh.y = y;   sh.zb = zb;   sh.Dnb = Dnb;   sh.eta = eta;
sh.Dzb = Dzb;   sh.Awet = Awet;   sh.DAwet = DAwet; sh.Dnbx = Dnbx;

% [s,sh] = bank(s,sh);