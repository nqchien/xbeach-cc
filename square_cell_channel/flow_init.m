function [s]=flow_init(s,par)
hmin=par.hmin;
zs0=par.zs0;
zb=s.zb;
% s.h=max(zs0-zb,0);	% old code
% s.zs=max(zb,zs0);	% old code
% Added code
% for j = 1:ny+1;
	% s.zs(1:36, j) = 0.1 - (j - 1) * 0.1 / ny;
% end;
s.zs = zeros(s.nx+1,s.ny+1) + zs0 - par.i * s.x;
ny = s.ny;
% s.zs(1:36, :) = repmat(linspace(0.05,-0.05,ny+1), 36, 1);
s.h = max(s.zs - zb, 0);
s.zs = max(zb, s.zs);
% End added
%s.zs=ones(size(zb))*zs0;
s.dzsdt=zeros(size(zb));
s.dzbdt=zeros(size(zb));
s.uu=zeros(size(zb));
s.vv=zeros(size(zb));
% ATTENTION: hard-coding constants 21 and 40, 20.
vmag = par.C * sqrt(s.h * par.i);
vmag = vmag .* (s.h > hmin);
alph = 22.8 * pi / 180;
s.uu = vmag * cos(alph);
s.vv = vmag * sin(alph);
% s.uu = s.uu .* (s.h > hmin);	% if h = 0 then v = 0
% for i = 1:s.ny+1
    % s.vv(21:36,i) = linspace(v0,v0/2,16)';
% end
s.qx=zeros(size(zb));
s.qy=zeros(size(zb));
s.sedero=zeros(size(zb));

% Added by Chien, for compatible with flow_bc.m
s.wetv = zeros(size(zb));