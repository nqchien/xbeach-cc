clf;
hold on;

xmin = -s.dxStandard / 2;
xmax = xmin + (s.nx+1) * s.dxStandard;
ymin = -s.dy / 2;
ymax = ymin + (s.ny+1) * s.dy;

% Horizontal grid
for i = xmin:s.dxStandard:xmax; plot([i i], [ymin ymax], 'cyan');end;

% Vertical grid;
for i = ymin:s.dy:ymax; plot([xmin xmax], [i i], 'cyan'); end;

% Boundary
plot(sh.xL0, sh.yL0, 'k-', 'linewidth',2);

quiver(s.x, s.y, s.u, s.v)
axis equal; axis tight;

xlabel('Cross-shore distance (m)')
ylabel('Longshore distance (m)')
