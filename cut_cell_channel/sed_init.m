function [s,par]=sed_init(s,par)
x=s.x;
s.cc         = zeros(size(x));
s.dcdx       = zeros(size(x));
s.dcdy       = zeros(size(x));
