function [s, sh, ZBINIT] = grid_bathy(par)

ZBINIT = -2;
h0 = 4;
hmin = 0.001;
nx = 10;
ny = 10; % 20;
s.dxStandard = 20.;

% Initialization
dx = zeros(nx+1,ny+1) + s.dxStandard;     % dx is now an array
dy = 20; %40.;
delta_u = ones(nx+1, ny+1);
delta_v = ones(nx+1, ny+1);
Th = zeros(nx+1, ny+1);
type = zeros(nx, ny);		% 0 1 3 4 or 5

% Set up delta matrix

% River with triangular hump
% delta_u(end,:) = 0;	% solid boundary in the East
% delta_v(end,:) = 0;
% for i = 7:11
	% delta_u(i, 10-(i-7):11+(i-7)) = 0;
	% delta_v(i-1, 11-(i-7):11+(i-7)) = 0;
% end;

% Flow around island -- coarse
delta_u(1,:) = 0;
delta_u(end,:) = 0;	% solid boundary in the East

for i = 5:6
	delta_u(i, 8) = 0.2;
	for j = 9:12
		delta_u(i,j) = 0;
	end
	delta_u(i,13) = 0.4;
end

delta_v(4,9) = 0.3;
delta_v(4,10) = 0.1;
delta_v(4,11) = 0.2;
delta_v(4,12) = 0.4;
delta_v(4,13) = 0.7;

delta_v(6,9) = 0.3;
delta_v(6,10) = 0.1;
delta_v(6,11) = 0.2;
delta_v(6,12) = 0.4;
delta_v(6,13) = 0.7;

for j = 9:13
	delta_v(5,j) = 0;
end

% adjusted island
% delta_u(5,8) = 0;
% delta_u(6,8) = 0;
% delta_v(5,8) = 0;

% Diagonal, narrow channel
% delta_u(:,:) = 0;
% delta_v(:,:) = 0;
% for j = 1:ny;
	% delta_u(j, j) = 1;
	% delta_u(j+1, j) = 1;
% end;

% for i = 1:nx;
	% delta_v(i, i) = 1;
	% delta_v(i, i+1) = 1;
% end;

% Determine cell types (common part)
for i = 1:nx;
	for j = 1:ny;
		d = [delta_u(i,j)  delta_u(i+1,j)  delta_v(i,j)  delta_v(i,j+1)];
		A = find(d == 0);
		B = find((d > 0) & (d < 1));
		C = find(d == 1);
		switch length(A)
			case 2
				type(i,j) = 3;
				if length(B) == 2
					Th(i,j) = 0.5 * d(B(1)) * d(B(2));
				elseif length(B) == 1
					Th(i,j) = 0.5 * d(B(1)) * d(C(1));
				else
					Th(i,j) = 0.5;
				end
			case 1
				type(i,j) = 4;
				if length(B) == 2
					Th(i,j) = 0.5 * (d(B(1)) + d(B(2)));
				elseif length(B) == 1
					Th(i,j) = 0.5 * (d(B(1)) + 1);
				else
					type(i,j) = 1; % Flow cell
					Th(i,j) = 1;
				end;
			case 0
				type(i,j) = 5;
				if length(B) == 2
					Th(i,j) = 1 - 0.5 * (1 - d(B(1))) * (1 - d(B(2)));
				else
					type(i,j) = 1;	% Flow cell
					Th(i,j) = 1;
				end
			otherwise
				type(i,j) = 0;	% Solid cell
				Th(i,j) = 0;
		end;
	end;
end;

zb = zeros(nx+1,ny+1)-.5;
filter = (type ==0);
zb(1:nx,1:ny) = zb(1:nx,1:ny) + filter * 2;
zb(nx+1,:) = 1.;
% zb(nx+1, 1:end-2) = 1;
% zb(1:end-2, ny+1) = 1;
% shore_shape = input('Shape of shore - Straight (default), [H]umped, [G]roined : ', 's');
% bank_elev = input('Bank elevation? [A number] / Inf (default) for none: ');

%
% Set original water depth
%
% Pre-allocation
x = zeros(nx+1,ny+1);
y = zeros(nx+1,ny+1);
% zb = zeros(nx+1,ny+1)-2;
a = ones(nx+1, ny+1);

% Slope
% slope_t = 0.01;	% transversal slope
for j=1:ny+1;
    for i=1:nx+1;
        s.xplot(i,j) = (i-1)* dx(i,j);          % a = 1
        s.yplot(i,j) = (j-1) * dy;
        % h(i,j)=max(h0*(1-(i-1)/nx),.1);
        % zb(i,j)=-h0*(1-(i-1)/(nx-5));
		% zb(i,j) = -h0 + x(i,j) * slope_t;
    end;
end

x = cumsum(dx) - s.dxStandard;
y = repmat(0:dy:ny*dy, nx+1, 1);
% Longitudinal slope
% zb = zb - par.i * y;

% if strcmp(upper(shore_shape), 'G');
% Groin (rectangular)
	% highest = zb(end,1);
	% zb (21:30, 19:21) = 0.5;
% end;

% if strcmp(upper(shore_shape), 'H');
	% Hump dimensions
	% height=3;
	% R=180;
	% x0=200;
	% y0=500;
	% h=h-height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	% zb=zb+height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	% zb=zb+height.*exp(-((x-x0).^2)/R^2);
% end;

% check with horizontal bottom
% h=max(h,h0);

% if bank_elev ~= Inf;
	% zb(31:end,:) = bank_elev; 	% bank
	% zb(:,18:22) = -5; trench
% end;

% End added

% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.a    = a;
s.ZBINIT = zb;
s.Th = Th;
s.type = type;
s.delta_u = delta_u;
s.delta_v = delta_v;

% Initial position of shoreline (additional code by Chien)
% Best to be predefined
% Here we take the intersection between bed and water surface
% THIS CODE USES INITIAL ZS, WHICH IS NOT OFFICIALLY DEFINED

if 0
	yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
	xx = [];           % cross-shore coordinates, can vary
	iiwet = [];     % list of positions of wet cells
	ny = s.ny;
	for i = 1 : ny+1
		le = 1; 	ri = ny;
		while s.zb(le,i)<0; le=le+1; end;
		while s.zb(ri,i)>0; ri=ri-1; end;
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0.05 - 0.1 * (i - 1)/ny); % par.zs0 for simple case
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0); % par.zs0 == 0
		xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0 - par.i * s.y(1,i)); % account for slope
	%   xi = interp1(s.zb(:,i),s.x(:,i),0.0,'nearest'); 
		% position of shoreline interpolate where zb = 0 ; this interpolation
		% function does not seem to work well
		% iwet = length(find(s.zb(:,i)<0));       % index of wet cell = number of cells with zb < 0
		iwet = length(find(s.zb(:,i)<0 - par.i * s.y(1,i)));       % index of wet cell = number of cells with zb < zs
		xx = horzcat(xx, [xi]);
		iiwet = horzcat(iiwet, [iwet]);
	end
	iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward

	% figure(1); clf; plot(xx, yy, 'g*-', 'linewidth',2);

	% Setting up the sh struct
	% sh is the structure containing data of shoreline particularly

	% these are spatial properties of shoreline
	sh.xL0 = [];	sh.yL0 = [];
	for i = 1:s.ny;
		sh.xL0 = horzcat(sh.xL0, [xx(i) xx(i)]);
		sh.yL0 = horzcat(sh.yL0, [yy(i) yy(i+1)]);
	end;

	sh.x0 = xx;
	sh.y0 = yy;

	% Alternately, we can define the coastline coordinates sh.x here
	sh.x = xx;
	sh.y = yy;
	sh.iiwet = iiwet;
	sh.iidry = iidry;
end;
sh.x = 0;
% End added