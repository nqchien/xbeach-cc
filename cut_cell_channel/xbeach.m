% Main routine xbeach
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Copyright (C) 2007 UNESCO-IHE, WL|Delft Hydraulics and Delft University %
% Dano Roelvink, Ap van Dongeren, Dirk-Jan Walstra, Ad Reniers            %
%                                                                         %
% d.roelvink@unesco-ihe.org                                               %
% UNESCO-IHE Institute for Water Education                                %
% P.O. Box 3015                                                           %
% 2601 DA Delft                                                           %
% The Netherlands                                                         %
%                                                                         %
% This library is free software; you can redistribute it and/or           %
% modify it under the terms of the GNU Lesser General Public              %
% License as published by the Free Software Foundation; either            %
% version 2.1 of the License, or (at your option) any later version.      %
%                                                                         %
% This library is distributed in the hope that it will be useful,         %
% but WITHOUT ANY WARRANTY; without even the implied warranty of          %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        %
% Lesser General Public License for more details.                         %
%                                                                         %
% You should have received a copy of the GNU Lesser General Public        %
% License along with this library; if not, write to the Free Software     %
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307     %
% USA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all

%
% method_choice = input('Use parallel shoreline polygon in computation? [Y] : ','s');
% chkpts = input('Enter check points in calculation, e.g [12  16  24:2:30] : ');

method_choice = '';
wave_choice = '';
chkpts = '';

tic;
% General input per module
[par] = wave_input;
[par] = flow_input(par);
[par] = sed_input(par);
% Grid and bathymetry
[s, sh, ZBINIT] = grid_bathy(par);
% wave_choice = input('Accounted for waves? [N] ', 's');
if strcmp(wave_choice,'N') || strcmp(wave_choice, '');
	par.wave = false;
else
	par.wave = true;
end;
% Directional distribution wave energy
[s] = wave_dist (s,par);
% Initialisations
[s,par] = wave_init (s,par);
[s] = flow_init (s,par);
[s,par] = sed_init (s,par);
par.nt=24000;
nt=par.nt;
par.t=0;
par.dt=.2;
par.tnext=par.tint;
it=0;
%added
% output_choice = input('Select type of output [G]raphics, [T]ext: ', 's');
% no wave here  disp(s.H) %
while par.t < par.tstop;
    % Wave boundary conditions
    [par,s] = wave_bc (s,par,it);
    % Flow boundary conditions
    [s] = flow_bc (s,par); 
    % Wave timestep
    % [s] = wave_timestep (s,par); % PROBLEM ! UNCONVERGED SOLUTION? OCTAVE HALT!
    % Flow timestep
    [s,par] = flow_timestep (s,par);
    % Suspended transport
    % [s]=transus(s,par);
    % Bed level update
    % [s]=bed_update(s,par);
    % if strcmp(method_choice, 'Y') || strcmp(method_choice, '') ;	% TEMPORARY
        % [s,sh] = shoreline2(s,sh,par);
    % else
        % [s,sh] = shoreline(s,sh,par);
    % end;
    % Output (lower frequency to reduce work of computer)
	if mod(par.t, par.tint) == 0;
		[it,s]=output(it,s,par,sh);
	end;
	if ismember(par.t, chkpts); 
		fprintf('%4.0f', par.t);
		fprintf('%7.4f', sh.x - sh.x0);
		fprintf('\n',0);
	end;
end
toc;
% figure(1); hold on; plot(sh.xL0, sh.yL0, 'g*-', 'linewidth',2);

% no wave here  disp(s.H) %