function [s]=transus(s,par)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Copyright (C) 2007 UNESCO-IHE, WL|Delft Hydraulics and Delft University %
% Dano Roelvink, Ap van Dongeren, Dirk-Jan Walstra, Ad Reniers            %
%                                                                         %
% d.roelvink@unesco-ihe.org                                               %
% UNESCO-IHE Institute for Water Education                                %
% P.O. Box 3015                                                           %
% 2601 DA Delft                                                           %
% The Netherlands                                                         %
%                                                                         %
% This library is free software; you can redistribute it and/or           %
% modify it under the terms of the GNU Lesser General Public              %
% License as published by the Free Software Foundation; either            %
% version 2.1 of the License, or (at your option) any later version.      %
%                                                                         %
% This library is distributed in the hope that it will be useful,         %
% but WITHOUT ANY WARRANTY; without even the implied warranty of          %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        %
% Lesser General Public License for more details.                         %
%                                                                         %
% You should have received a copy of the GNU Lesser General Public        %
% License along with this library; if not, write to the Free Software     %
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307     %
% USA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sediment transport; bed load or total transport evaluated in u, v
% points
uu     = s.ueu;  % use eulerian velocities
vv     = s.vev;  % use eulerian velocities
wetu   = s.wetu;
wetv   = s.wetv;
u      = s.ue;   % use eulerian velocities
v      = s.ve;   % use eulerian velocities
vmag2  = u.^2+v.^2;
hu     = s.hu;
hv     = s.hv;
hold   = s.hold;
h      = s.h;
nx     = s.nx;
ny     = s.ny;
a      = s.a;
A      = par.A;
n      = par.n;
dico   = par.dico;
umin   = par.umin;
hmin   = par.hmin;
c      = s.cc;
f1     = zeros(size(uu));
f2     = zeros(size(uu));
dcdx   = zeros(size(uu));
dcdy   = zeros(size(uu));
dt     = par.dt;
dx     = s.dx;
dy     = s.dy;
w      = 0.02;
Ts     = .1*h/w;
Ts=max(Ts,.2);

% calculate equilibrium concentration
% 
%ceq    = A*vmag2.^((n-1)/2);
%
% soulsby van Rijn
[s] = sb_vr(s,par);
ceq = s.ceq;
% steetzel


    % Upwind method implemented through weight factors mx1 and mx2
    %
    % X-direction
    mx=sign(uu);
    mx(abs(uu)<umin)=0;
    mx1=(mx+1)/2;
    mx2=-(mx-1)/2;
    f1(1:nx,:)=c(1:nx,:);
    f2(1:nx,:)=c(2:nx+1,:);
    % water depth in u-points
    cu=mx1.*f1+mx2.*f2;
    dcdx(1:end-1,:)=(c(2:end,:)-c(1:end-1,:))./(dx(1:end-1,:).*a(1:end-1,:));
    %dcdx(~wetu)=0;
    Su=cu.*uu.*hu-dico*hu.*dcdx;
    % Y-direction
    my=sign(vv);
    my(abs(vv)<umin)=0;
    my1=(my+1)/2;
    my2=-(my-1)/2;
    f1=f1*0;f2=f2*0;
    f1(:,1:ny)=c(:,1:ny);
    f2(:,1:ny)=c(:,2:ny+1);
    cv=my1.*f1+my2.*f2;
    dcdy(:,1:end-1)=(c(:,2:end)-c(:,1:end-1))./dy;
    %dcdy(~wetv)=0;
    Sv=cv.*vv.*hv-dico*hv.*dcdy;
c(2:end,2:end) = (hold(2:end,2:end).*c(2:end,2:end)-dt*(...
    (Su(2:end,2:end)-Su(1:end-1,2:end))./(dx(1:end-1,2:end) ...
    .*a(1:end-1,2:end))+ ...
    (Sv(2:end,2:end)-Sv(2:end,1:end-1))/dy- ...
    hold(2:end,2:end).*(ceq(2:end,2:end)-c(2:end,2:end))./Ts(2:end,2:end)));
c(h>=hmin)=c(h>=hmin)./h(h>=hmin);
c(h<hmin) = 0.;
c(1,:)=c(2,:);
c(:,1)=c(:,2);
c(end,:)=c(end-1,:);
c(:,end)=c(:,end-1);
    %
s.vmag=sqrt(max(vmag2,umin));
s.Su = Su;
s.Sv = Sv;
s.Ts = Ts;
s.cc  = c;
s.dcdx=dcdx;
s.dcdy=dcdy;

