function [s] = flow_bc (s, par)     % (s,par,it)
zs  = s.zs;
% added for bound long wave bc Ad 27 march 2006
% no need ?     ui  = s.ui;
hh  = s.h;
%
% Sea boundary at x=0;
%
% zs(1,:)  = 0;  % old boundary
% 
% new boundary (not perfect, ignores direction of incoming free long wave
% and also the difference in velocity response for free and bound long
% waves) Ad 27 march 2006
%
  % no need ?
  %ui=uin*sin(omega*t);
ui = 0;
%   uu(1,:)=2*ui -sqrt(9.81./hh(1,:)).*(zs(2,:)-par.zs0);

% Added by Chien

% New velocity distribution v = C.sqrt(hi)
h = s.h;		i = par.i;	C = par.C;

% SW boundary
% s.vv(1,1) = 0.65; 	s.uu(1,1) = 0.65;
% NE boundary
% s.vv(end,end-1) = 0.65;	s.uu(end-1,end) = 0.65;
% End added

% Diagonal, narrow channel
% SW
% s.vv(1,1) = C * sqrt (h(1, 1) * i) / sqrt(2);
% s.uu(1,1) = C * sqrt (h(1, 1) * i) / sqrt(2);
% NE
% s.vv(end,end-1) = C * sqrt (h(end,end-1) * i) / sqrt(2);
% s.uu(end,end-1) = C * sqrt (h(end,end-1) * i) / sqrt(2);

% Channel 22.8o
le = 2:12;
ri = 44:55;

alph = 22.8 * pi / 180;
q0 = 1.;
vnorm = q0 ./ h(1,2:12);
s.uu(1,2:12) = vnorm * cos(alph);
s.vv(1,2:12) = vnorm * sin(alph);
zs(1,:) = zs(2,:) + (s.dxStandard * cos(alph)) * i;
s.zs(end,44:55) = s.zb(end,44:55) + 0.618;

s.vv(:,1)= 0.;
s.vv(:,end) = 0.;

% Reverse direction
% vnorm = q0 ./ h(end,ri);
% s.uu(1,ri) = -vnorm * cos(alph);
% s.vv(1,ri) = -vnorm * sin(alph);
% s.zs(1,le) = s.zb(1,le) + 0.618;

% s.vv(:,1)= 0.;
% s.vv(:,end) = 0.;

% if h(end,j) > par.hmin; s.uu(end,j) = q0 ./ h(end,j); end;
% vnorm = C * sqrt(h(end,j) * i);
% s.uu(end,j) = vnorm * cos(alph);
% s.vv(end,j) = vnorm * sin(alph);
% tan(alph) = 0.42
% Old
% s.uu(end,j) = 0.58 * s.uu(end-1, j) + 0.42 * s.uu(end-1,j-1);
% s.vv(end,j) = 0.58 * s.vv(end-1, j) + 0.42 * s.vv(end-1,j-1);
% s.h(end,j) = s.h(end-1,j-1);
% s.zs(end,j) = s.zb(end,j) + s.h(end,j);

%% s.zs(end,j) = 0.58 * s.zs(end-1, j) + 0.42 * s.zs(end-1,j-1) - (s.dxStandard / cos(alph)) * i;

% New
% zs(1,:) = zs(2,:) + (s.dxStandard * cos(alph)) * i;
% zs(end,:) = zs(end-1,:) - (s.dxStandard * cos(alph)) * i;

% River with hump
% S boundary
% s.vv(:,1) = C * sqrt (h(:, 1) * i);
% N boundary
% s.vv(:,end) = C * sqrt (h(:, end) * i);
% W boundary
% s.uu(1,:) = 0;
% E boundary
% s.uu(end,:) = 0;
% uu(1,:) = 0;
