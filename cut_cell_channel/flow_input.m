function [par] = flow_input(par);
par.C=70;
par.eps=1e-1;
par.g=9.81;
par.umin=.1;
par.zs0=-0.4;
par.tstart=0;
par.tint=1;
par.tstop=660; % 120;
par.CFL=0.5; % 0.2;

% Added
par.v0 = 1.;
par.i = 1E-3;
