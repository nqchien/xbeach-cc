function [s]=flow_init(s,par)
hmin = par.hmin;
zs0 = par.zs0;
zb = s.zb;
% s.h=max(zs0-zb,0);	% old code
% s.zs=max(zb,zs0);	% old code
% Added code
% for j = 1:ny+1;
	% s.zs(1:36, j) = 0.1 - (j - 1) * 0.1 / ny;
% end;
s.zs = zeros(s.nx+1,s.ny+1) + zs0 - par.i * s.x;
ny = s.ny;
% s.zs(1:36, :) = repmat(linspace(0.05,-0.05,ny+1), 36, 1);
s.zs = max(zb, s.zs);
s.h = s.zs - zb;
% End added
%s.zs=ones(size(zb))*zs0;
s.dzsdt=zeros(size(zb));
s.dzbdt=zeros(size(zb));
s.uu = zeros(size(zb));
s.vv = zeros(size(zb));
% ATTENTION: hard-coding constants 21 and 40, 20.
alph = 22.8 * pi / 180;
% s.vv = par.C * sqrt(s.h * par.i);
vnorm = par.C * sqrt(s.h * par.i);

s.uu = vnorm * cos(alph);
s.vv = vnorm * sin(alph);

% Reverse flow
% s.uu = -vnorm * cos(alph);
% s.vv = -vnorm * sin(alph);


% s.uu = par.C * sqrt(s.h * par.i);
% s.vv = s.vv .* (s.h > 0);	% if h = 0 then v = 0
% for i = 1:s.ny+1
    % s.vv(21:36,i) = linspace(v0,v0/2,16)';
% end
s.qx=zeros(size(zb));
s.qy=zeros(size(zb));
s.sedero=zeros(size(zb));

% Added by Chien, for compatible with flow_bc.m
s.wetv = zeros(size(zb));