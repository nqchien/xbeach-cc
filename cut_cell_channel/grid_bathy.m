function [s, sh, ZBINIT] = grid_bathy(par)

% Inclined channel according to Rosatti (2005)
ZBINIT = -2;
h0 = 4;
hmin = 0.001;
nx = 100;
ny = 55;
s.dxStandard = 10.;

dx = zeros(nx+1,ny+1) + 10.;     % dx is now an array
dy = 10.;
x = cumsum(dx) - s.dxStandard;
y = repmat(0:dy:ny*dy, nx+1, 1);

s.xplot = x;
s.yplot = y;
delta_u = zeros(nx+1, ny+1);
delta_v = zeros(nx+1, ny+1);
Th = zeros(nx+1, ny+1);
type = zeros(nx, ny);		% 0, 1, 3, 4 or 5
subtype = zeros(nx, ny);		% 1, 2, 3, 4

% Set up delta matrix For 22.8 degrees channel
alph = double(22.8 * pi /180);
y1 = 1 + (0:100) * tan(alph);	% 22.8
y2 = y1 + 10;
d1 = 1 - mod(y1,1);
d2 = mod(y2,1);
% WRONG WAY
% d1 = (dy - mod(y1,dy)) / dy;
% d2 = mod(y2,dy) / dy;

for i = 1 : nx+1;
	j1 = floor(y1(i)) + 1;
	j2 = j1 + 10;
	delta_u(i, j1) = d1(i);
	delta_u(i, j2) = d2(i);
	for j = j1+1:j2-1
		delta_u(i,j) = 1;
	end
end;

for i = 1:nx
	for j = 2:ny
		yavg = (j-1);
		if (y1(i) < yavg) & (yavg <= y2(i)) & (y1(i+1) <= yavg) & (yavg < y2(i+1))
			delta_v(i,j) = 1;
		elseif (y1(i) < yavg) & (yavg < y2(i)) & (y1(i+1) > yavg)
			delta_v(i,j) = d1(i) / (y1(i+1) - y1(i));
		elseif (y2(i) < yavg) & (y1(i+1) < yavg) & (yavg <= y2(i+1))
			delta_v(i,j) = d2(i+1) / (y2(i+1) - y2(i));
		else
			delta_v(i,j) = 0;
		end
	end
end

% Set up delta matrix for 45 degrees channel
% for i = 1 : nx+1
	% for j = i : i+9
		% delta_u(i,j) = 1;
	% end
% end

% for i = 1 : nx
	% for j = i+1 : i+10
		% delta_v(i,j) = 1;
	% end
% end

% Determine cell types (common part)
for i = 1:nx;
	for j = 1:ny;
		% d = [delta_u(i,j)  delta_u(i+1,j)  delta_v(i,j)  delta_v(i,j+1)];
		de = delta_u(i+1,j);
		dw = delta_u(i,j);
		dn = delta_v(i,j+1);
		ds = delta_v(i,j);
		d = [dw de ds dn];
		Sx = dw + de;
		Sy = dn + ds;
		Dx = abs(dw - de);
		Dy = abs(dn - ds);
		Th(i,j) = (Sx * Sy + Sx * Dy + Sy * Dx - Dx * Dy) / 4;
		A = find(d == 0);
		B = find((d > 0) & (d < 1));
		C = find(d == 1);
		switch length(A)
			case 2
				type(i,j) = 3;
				if de * ds > 0
					subtype(i,j) = 31;
				elseif de * dn > 0
					subtype(i,j) = 32;
				elseif dn * dw > 0
					subtype(i,j) = 33;
				elseif dw * ds > 0
					subtype(i,j) = 34;
				end
				% if length(B) == 2
					% Th(i,j) = 0.5 * d(B(1)) * d(B(2));
				% elseif length(B) == 1
					% Th(i,j) = 0.5 * d(B(1)) * d(C(1));
				% else
					% Th(i,j) = 0.5;
				% end
			case 1
				type(i,j) = 4;
				if dw * dn * de > 0	% or ds == 0
					subtype(i,j) = 41;
				elseif dn * dw * ds > 0
					subtype(i,j) = 42;
				elseif dw * ds * de > 0
					subtype(i,j) = 43;
				elseif ds * de * dn > 0
					subtype(i,j) = 42;
				end
				if length(B) == 2
					% Th(i,j) = 0.5 * (d(B(1)) + d(B(2)));
				elseif length(B) == 1
					% Th(i,j) = 0.5 * (d(B(1)) + 1);
				else
					type(i,j) = 1; % Flow cell
					% Th(i,j) = 1;		can omit here
				end;
			case 0
				type(i,j) = 5;
				if (ds < 1) & (de < 1)
					subtype(i,j) = 51;
				elseif (de < 1) & (dn < 1)
					subtype(i,j) = 52;
				elseif (dn < 1) & (dw < 1)
					subtype(i,j) = 53;
				elseif (dw < 1) & (ds < 1)
					subtype(i,j) = 54;
				end
				if length(B) == 2
					% Th(i,j) = 1 - 0.5 * (1 - d(B(1))) * (1 - d(B(2)));
				else
					type(i,j) = 1;	% Flow cell
					% Th(i,j) = 1;		can omit this
				end
			otherwise
				type(i,j) = 0;	% Solid cell
				% Th(i,j) = 0;			can omit this
		end;
	end;
end;

% determining centroids
dx0 = s.dxStandard;
for i = 1:nx
	for j = 2:ny
		de = delta_u(i+1,j);
		dw = delta_u(i,j);
		dn = delta_v(i,j+1);
		ds = delta_v(i,j);
		if type(i,j) == 3
			% if type(i,j-1) == 0	% southern bank
			if subtype(i,j) == 33	% southern bank
				x(i,j) = x(i,j) - dx0/2 + dx0 * delta_v(i,j+1)/3;
				y(i,j) = y(i,j) + dy/2 - dy * delta_u(i,j)/3;
			% elseif (type(i,j-1) == 5) || (type(i,j-1) == 1)	% northern bank
			elseif subtype(i,j) == 31
				x(i,j) = x(i,j) + dx0/2 - dx0 * delta_v(i,j)/3;
				y(i,j) = y(i,j) - dy/2 + dy * delta_u(i+1,j)/3;
			end
		elseif type(i,j) == 4
			% if type(i,j-1) == 0	% southern bank
			if subtype(i,j) == 41
				x(i,j) = x(i,j) + dx0/2 - (2*delta_u(i,j) + delta_u(i+1,j))/(3*(delta_u(i,j)+delta_u(i+1,j)))*dx0;
				y(i,j) = y(i,j) + dy/2 - dy * (delta_u(i,j)+delta_u(i+1,j)) / 4;
			% elseif type(i,j-1) == 1	% northern bank
			elseif subtype(i,j) == 43
				x(i,j) = x(i,j) - dx0/2 + (delta_u(i,j) + 2*delta_u(i+1,j))/(3*(delta_u(i,j)+delta_u(i+1,j)))*dx0;
				y(i,j) = y(i,j) - dy/2 + dy * (delta_u(i,j)+delta_u(i+1,j)) / 4;
			end
		elseif type(i,j) == 5
			if subtype(i,j) == 51	% southern bank
				x(i,j) = x(i,j) - dx0 * (1/2 - ds/3) * (0.5 * de * ds)/(1 - de * ds / 2);
				y(i,j) = y(i,j) + dy * (1/2 - de/3) * (0.5 * de * ds)/(1 - de * ds / 2);
			elseif subtype(i,j) == 53	% northern bank
				x(i,j) = x(i,j) + dx0 * (1/2 - dn/3) * (0.5 * dn * dw)/(1 - dn * dw / 2);
				y(i,j) = y(i,j) - dy * (1/2 - dw/3) * (0.5 * dn * dw)/(1 - dn * dw / 2);
			end
		end
	end
end

% channel
zb = zeros(nx+1,ny+1) + 0.5;
% filter = (type ==0);
% zb(1:nx,1:ny) = zb(1:nx,1:ny) + filter * 2;
% zb(nx+1, :) = zb(nx,:);
% zb(1:end-2, ny+1) = 1;
% shore_shape = input('Shape of shore - Straight (default), [H]umped, [G]roined : ', 's');
% bank_elev = input('Bank elevation? [A number] / Inf (default) for none: ');

% Slope
% for i = 1:nx
	% for j = 1:ny
		% zb(i,j) = zb(i,j) - (i + j - 2) * sqrt(2) * dy * par.i;
	% end
% end
%
% Set original water depth
%
% Pre-allocation
% x = zeros(nx+1,ny+1);
% y = zeros(nx+1,ny+1);
% zb = zeros(nx+1,ny+1)-2;
a = ones(nx+1, ny+1);

% Temporary for diagonal channel -- to determine centroids
% for i = 2 : nx
	% x(i, i-1) = (i-1)*s.dxStandard - s.dxStandard / 6;
	% y(i, i-1) = (i-2) * dy + dy / 6;
% end
% for j = 2 : ny
	% x(j-1, j) = (j-2) * s.dxStandard + s.dxStandard / 6;
	% y(j-1, j) = (j-1) * dy - dy / 6;
% end;


% Flow W--E
% xorg = x(1,2);
% yorg = y(1,2);
for i = 1 : nx+1
	jstart = floor(y1(i)) + 1;
	jstop = floor(y2(i)) + 2;
	if (i <= nx) & (type(i,jstop) == 0);	jstop = jstop -1; 	end;
	for j = jstart : jstop;		%10 not 9, this is not staircase !
		% ang = atan((y(i,j) - yorg) / (x(i,j) - xorg)) - 22.8;
		% dist = sqrt((x(i,j) - xorg)^2 + (y(i,j) - yorg)^2);
		% zb(i,j) = -1 - par.i * dist * cos(ang);
		% or
		zb(i,j) = -1 - par.i * x(i,jstart) / cos(alph) - par.i * (j-jstart) * dy * sin(alph);
	end
end


% Flow E--W
% xorg = x(end,55);
% yorg = y(end,55);

% for i = 1 : nx+1
	% ii = nx - i + 2;
	% jstart = floor(y1(ii)) + 1;
	% jstop = floor(y2(ii)) + 2;
	% if (ii <= nx) & (type(ii,jstop) == 0);	jstop = jstop -1; 	end;
	% for j = jstart : jstop;		%10 not 9, this is not staircase !
		% ang = atan((y(i,j) - yorg) / (x(i,j) - xorg)) - 22.8;
		% dist = sqrt((x(i,j) - xorg)^2 + (y(i,j) - yorg)^2);
		% zb(i,j) = -1 - par.i * dist * cos(ang);
		% or
		% zb(ii,j) = -1 - par.i * x(ii,jstart) / cos(alph) + par.i * (j-jstart) * dy * sin(alph);
	% end
% end


% Longitudinal slope
% zb = zb - par.i * x;

% if strcmp(upper(shore_shape), 'G');
% Groin (rectangular)
	% highest = zb(end,1);
	% zb (21:30, 19:21) = 0.5;
% end;

% if strcmp(upper(shore_shape), 'H');
	% Hump dimensions
	% height=3;
	% R=180;
	% x0=200;
	% y0=500;
	% h=h-height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	% zb=zb+height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	% zb=zb+height.*exp(-((x-x0).^2)/R^2);
% end;

% check with horizontal bottom
% h=max(h,h0);

% if bank_elev ~= Inf;
	% zb(31:end,:) = bank_elev; 	% bank
	% zb(:,18:22) = -5; trench
% end;

% Added  -- simple pool, no erosion
% A hump from cellx = 32 -- 35 and celly = 11 -- 40
% zb = zeros(nx+1, ny+1) - 2.;
% zb(37:end,:) = 1;
% zb(36,15:26) = 1;
% zb(35,17:24) = 1;
% zb(34,20:21) = 1;
% zb(32:33,20:21) = 1;
% dx(36,12) = .9 * s.dxStandard;
% dx(36,13) = .7 * s.dxStandard;
% dx(36,14) = .3 * s.dxStandard;
% dx(35,15) = .8 * s.dxStandard;
% dx(35,16) = .2 * s.dxStandard;
% dx(34,17) = .7 * s.dxStandard;
% dx(34,18) = .3 * s.dxStandard;
% dx(34,19) = .1 * s.dxStandard;
% mirror
% for j = 21:30;
	% dx(34:36, j) = dx(34:36, ny+1-j);
% end;

% zb(37:end,:) = 1;
% dx(36,21) = 5.;
% zb(37,12:29) = -2;
% zb(38,15:26) = -2;
% zb(39,17:24) = -2;
% zb(40,20:21) = -2;
% zb(40,20:21) = -2;
% dx(37,12) = .1 * s.dxStandard;
% dx(37,13) = .3 * s.dxStandard;
% dx(37,14) = .7 * s.dxStandard;
% dx(38,15) = .2 * s.dxStandard;
% dx(38,16) = .8 * s.dxStandard;
% dx(39,17) = .3 * s.dxStandard;
% dx(39,18) = .7 * s.dxStandard;
% dx(39,19) = .9 * s.dxStandard;
% for j = 21:30;
	% dx(37:39, j) = dx(37:39, ny+1-j);
% end;

% End added

% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.a    = a;
s.ZBINIT = zb;
s.Th = Th;
s.type = type;
s.subtype = subtype;
s.delta_u = delta_u;
s.delta_v = delta_v;

% Initial position of shoreline (additional code by Chien)
% Best to be predefined
% Here we take the intersection between bed and water surface
% THIS CODE USES INITIAL ZS, WHICH IS NOT OFFICIALLY DEFINED

if 0
	yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
	xx = [];           % cross-shore coordinates, can vary
	iiwet = [];     % list of positions of wet cells
	ny = s.ny;
	for i = 1 : ny+1
		le = 1; 	ri = ny;
		while s.zb(le,i)<0; le=le+1; end;
		while s.zb(ri,i)>0; ri=ri-1; end;
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0.05 - 0.1 * (i - 1)/ny); % par.zs0 for simple case
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0); % par.zs0 == 0
		xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0 - par.i * s.y(1,i)); % account for slope
	%   xi = interp1(s.zb(:,i),s.x(:,i),0.0,'nearest'); 
		% position of shoreline interpolate where zb = 0 ; this interpolation
		% function does not seem to work well
		% iwet = length(find(s.zb(:,i)<0));       % index of wet cell = number of cells with zb < 0
		iwet = length(find(s.zb(:,i)<0 - par.i * s.y(1,i)));       % index of wet cell = number of cells with zb < zs
		xx = horzcat(xx, [xi]);
		iiwet = horzcat(iiwet, [iwet]);
	end
	iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward

	% figure(1); clf; plot(xx, yy, 'g*-', 'linewidth',2);

	% Setting up the sh struct
	% sh is the structure containing data of shoreline particularly

	% these are spatial properties of shoreline
	sh.xL0 = [];	sh.yL0 = [];
	for i = 1:s.ny;
		sh.xL0 = horzcat(sh.xL0, [xx(i) xx(i)]);
		sh.yL0 = horzcat(sh.yL0, [yy(i) yy(i+1)]);
	end;

	sh.x0 = xx;
	sh.y0 = yy;

	% Alternately, we can define the coastline coordinates sh.x here
	sh.x = xx;
	sh.y = yy;
	sh.iiwet = iiwet;
	sh.iidry = iidry;
end;
sh.x = 0;
% End added
