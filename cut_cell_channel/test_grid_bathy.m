function [s, sh, ZBINIT] = grid_bathy
h0=5;
hmin = 0.001;
nx=40;
ny=40;
s.dxStandard = 10.;
% dx=10;
dx = zeros(nx+1,ny+1) + s.dxStandard;     % dx is now an array
dy = 25;

shore_shape = input('Shape of shore - [S]traight, [H]umped, [G]roined : ', 's');
has_bank = input('A bank of 0.5 m height? [Y/N]: ', 's');
if  shore_shape == 'H';
	%
	% Hump dimensions
	%
	height=3;
	R=180;
	x0=200;
	y0=500;
end;

%
% Set original water depth
%
% Pre-allocation
x = zeros(nx+1,ny+1);
y = zeros(nx+1,ny+1);
zb = zeros(nx+1,ny+1);
a = ones(nx+1, ny+1);

for j=1:ny+1;
    for i=1:nx+1;
        x(i,j) = (i-1)* dx(i,j);          % a = 1
        y(i,j) = (j-1) * dy;
        h(i,j)=max(h0*(1-(i-1)/nx),.1);
        zb(i,j)=-h0*(1-(i-1)/(nx-5));
    end;
end


if shore_shape == 'G';
% Groin (rectangular)
	highest = zb(end,1);
	zb (21:30, 19:21) = 0.5;
end;

if shore_shape == 'H';
% Hump (Gaussian)
	h=h-height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	zb=zb+height.*exp(-((x-x0).^2+(y-y0).^2)/R^2);
	zb=zb+height.*exp(-((x-x0).^2)/R^2);
end;

% check with horizontal bottom
h=max(h,h0);

if has_bank == 'Y';
% Bank
	zb(35:end,:) = 0.5; 	% a "step" at shoreline
	% zb(:,18:22) = -5; trench
end;

ZBINIT = zb;
% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.a    = a;

% Position of shoreline (additional code by Chien)

yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
xx = [];           % cross-shore coordinates, can vary
iiwet = [];     % list of positions of wet cells
for i = 1 : s.ny+1
    le = 1; ri = s.ny;
    while s.zb(le,i)<0; le=le+1; end;
    while s.zb(ri,i)>0; ri=ri-1; end;
    xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i),0.0);
%   xi = interp1(s.zb(:,i),s.x(:,i),0.0,'nearest'); 
    % position of shoreline interpolate where zb = 0 ; this interpolation
    % function does not seem to work well
    iwet = length(find(s.zb(:,i)<0));       % index of wet cell = number
                                            % of cells with zb < 0
    xx = horzcat(xx, [xi]);
    iiwet = horzcat(iiwet, [iwet]);
end
iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward

figure(1); plot(xx, yy, 'g*-', 'linewidth',2);

% Setting up the sh struct
% sh is the structure containing data of shoreline particularly

% these are spatial properties of shoreline
sh.xx = xx;
sh.yy = yy;

% Alternately, we can define the coastline coordinates sh.x here
sh.x = xx;
sh.y = yy;
sh.iiwet = iiwet;
sh.iidry = iidry;

% End added