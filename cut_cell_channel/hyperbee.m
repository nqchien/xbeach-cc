function ret = hyperbee(a, b);

s = sign(b);
ret = 2 * s * max(0, min(abs(b), s * a));
