clf;
hold on;

xmin = -s.dxStandard / 2;
xmax = xmin + (s.nx+1) * s.dxStandard;
ymin = -s.dy / 2;
ymax = ymin + (s.ny+1) * s.dy;

% Horizontal grid
for i = xmin:s.dxStandard:xmax; plot([i i], [ymin ymax], 'cyan');end;

% Vertical grid;
for i = ymin:s.dy:ymax; plot([xmin xmax], [i i], 'cyan'); end;

% Boundary
% triangular hump
% plot([95 95 45 95 95], [-10 90 190 290 410] , 'k-','linewidth',2)

% Diagonal channel
% y11 = 5; y12 = 5+(1005*tan(22.8*pi/180));
% plot([5 105], [-5 95] , 'k-','linewidth',2);
% plot([-5 95], [5 105] , 'k-','linewidth',2);
% line([-5 1005],[y11 y12],'linewidth',2);
% line([-5 1005],[y11+100 y12+100],'linewidth',2);
xbound = -5:10:1005;
ybound1 = (xbound + 5) * tan(22.8*pi/180) + 5;
ybound2 = ybound1 + 100;
plot(xbound, ybound1, 'linewidth', 2);
plot(xbound, ybound2, 'linewidth', 2);
quiver(s.x, s.y, s.u .* (s.zb < 0), s.v .* (s.zb < 0), 'b');
axis equal tight;

xlabel('X (m)')
ylabel('Y (m)')
box on
