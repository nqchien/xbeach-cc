function ret = minmod(a, b);

s = sign(b);
ret = s .* max(0, min(abs(b), s .* a));
