# The problem

Modelling the flow in rivers and near the coastlines is crucial in predicting shore erosion. This study aims to improve the first version of the XBeach numerical model, and also form part of the repo author's MSc thesis. 

The cut-cell method in calculating flow along shorelines is demonstrated for two general class of problems:

* The 1-D case where the flow field is generally unidirectional, and the shoreline typically follows the gridlines.
* The 2-D case where the flow field is horizontal, and the shoreline cut through the gridlines.

# Code organisation 

Original code of MatLab follows the structure below:

![Diagram of XBeach model](fig/struct-xbeach-0.png)

Figure: Diagram of XBeach model (with new parts highlighted)

What I have done:

- Extend the code for cut-cell case, in the files:
    + ![`grid_bathy`](src/grid_bathy.m) for adapting the grids for cut-cell cases
    + ![`flow_timestep`](src/grid_bathy.m) for solving the hydrdoynamic equations on the cut-cell grid.

- Add a module for tracking shoreline movement in the 1-D model
- Refactor the code
    + Grouping relevant functions into packages
    + Add documentation
    + Linting


# Usage 

In `src2d` folder, use MatLab and edit `xbeach.m` file, specifying the simulation case `"CHANNEL"` or `"BEND"` and the stop time `par.tstop`. Further adjustment on parameters can be made to the `par` structure.


# Details

In adapting the grids for cut cells, I use the classfication scheme by Tu & Ruffin (2002):

![Classification of cut cells](fig/cut-cell-types.png)

Figure: Classification of cut cells

Modification to continuity and momentum equations follows the change in area of the wet (water) part of the cell, the shape and alignment of these cut cells.
