function [par]=sed_input(par)
par.A    = 2e-3;
par.n    = 5;
par.dico = 1;
par.D50      = 0.0002;
par.D90      = 0.0003;
par.rhos     = 2650;
par.morfac   = 200.0;
