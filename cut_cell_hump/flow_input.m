function [par] = flow_input(par);
par.C = 65;
par.eps=1e-1;
par.g=9.81;
par.umin=.1;
par.zs0 = 0;
% par.h0 = 0.1;
par.tstart=0;
par.tint=1;
par.tstop=600; % 120;
par.CFL=0.5; % 0.2;

% Added
par.v0 = 1.;
par.i = 0.35E-3;
