clf;
hold on;

xmin = -s.dxStandard / 2;
xmax = xmin + (s.nx+1) * s.dxStandard;
ymin = -s.dy / 2;
ymax = ymin + (s.ny+1) * s.dy;

% Horizontal grid
for i = xmin:s.dxStandard:xmax; plot([i i], [ymin ymax], 'cyan');end;

% Vertical grid;
for i = ymin:s.dy:ymax; plot([xmin xmax], [i i], 'cyan'); end;

% Boundary

% Bend channel
margin = 5;
xo = -0.25:0.1:14.76;
xi = -0.25:0.1:12.26;
plot(margin + xo, margin + sqrt(15^2 - (xo+0.25) .^ 2) - 0.25, 'k', 'linewidth', 2);
plot(margin + xi, margin + sqrt(12.5^2 - (xi+0.25).^2) - 0.25, 'k', 'linewidth', 2);
quiver(s.x , s.y, s.u .* (s.u < 1), s.v .* (s.v < 1));
axis equal tight;

xlabel('X (m)')
ylabel('Y (m)')
drawnow;