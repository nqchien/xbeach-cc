function ret = tau_c(SC);

ret = 0.1 + 0.1779*SC + 0.0028 * SC^2 - (2.34E-5) * SC^3;
