function [s, sh, ZBINIT] = grid_bathy(par)

ZBINIT = -2;
h0 = 4;
hmin = 0.001;
dxStandard = 10;

nx = 10;
ny = 20;

dx = zeros(nx+1,ny+1) + dxStandard;
dy = 20;
x = cumsum(dx) - dxStandard;
y = repmat(0:dy:ny*dy, nx+1, 1);

% s.xplot = x;
% s.yplot = y;
delta_u = ones(nx+1, ny+1);
delta_v = ones(nx+1, ny+1);
Th = zeros(nx+1, ny+1);
type = zeros(nx + 1, ny + 1);

% River with triangular hump
delta_u(end,:) = 0;	% solid boundary in the East
delta_v(end,:) = 0;
for i = 7:11
	delta_u(i, 10-(i-7):11+(i-7)) = 0;
	delta_v(i-1, 11-(i-7):11+(i-7)) = 0;
end;

% Determine cell types (common part)
for i = 1:nx;
	for j = 1:ny;
		d = [delta_u(i,j)  delta_u(i+1,j)  delta_v(i,j)  delta_v(i,j+1)];
		A = find(d == 0);
		B = find((d > 0) & (d < 1));
		C = find(d == 1);
		switch length(A)
			case 2
				type(i,j) = 3;
			case 1
				type(i,j) = 4;
				if length(B) == 2
				elseif length(B) == 1
				else
					type(i,j) = 1; % Flow cell
				end;
			case 0
				type(i,j) = 5;
				if length(B) == 2
				else
					type(i,j) = 1;	% Flow cell
				end
			otherwise
				type(i,j) = 0;	% Solid cell
		end;
		Sx = d(1) + d(2);
		Sy = d(3) + d(4);
		Dx = abs(d(1) - d(2));
		Dy = abs(d(3) - d(4));
		Th(i,j) = (Sx * Sy + Sx * Dy + Sy * Dx - Dx * Dy) / 4;
	end;
end;

% determining centroids -- for more detail, refer \cut_cell
type(nx+1,:) = 0;
type(:,ny+1) = 0;
for i = 1:nx
	for j = 1:ny
		dn = delta_v(i,j+1);
		ds = delta_v(i,j);
		de = delta_u(i+1,j);
		dw = delta_u(i,j);
		if (type(i,j) == 4) & (type(i, j+1) == 1)	% inner, NW
			x(i,j) = x(i,j) - dxStandard/2 + ...
					(dw + 2*de)/ (3*(dw+de))*dxStandard;
			y(i,j) = y(i,j) + dy/2 - dy * (dw+de) / 4;
		elseif (type(i,j) == 4) & (type(i, j+1) == 0)	% outer, NW
			x(i,j) = x(i,j) + dxStandard/2 - (2*dw + de)/ ...
					(3*(dw+de))*dxStandard;
			y(i,j) = y(i,j) - dy/2 + dy * (dw+de) / 4;
		elseif (type(i,j) == 4) & (type(i+1, j) == 1)	% inner, SE
			x(i,j) = x(i,j) + dxStandard/2 - dxStandard * (dn + ds) / 4;
			y(i,j) = y(i,j) - dy/2 + (2*dn + ds)/ (3*(dn+ds))*dy;
		elseif (type(i,j) == 4) & (type(i+1, j) == 0)	% outer, SE
			x(i,j) = x(i,j) - dxStandard/2 + dxStandard * (dn + ds) / 4;
			y(i,j) = y(i,j) + dy/2 - (dn + 2*ds)/ (3*(dn+ds))*dy;
		elseif (type(i,j) == 3) & (type(i,j+1) ~= 0) % ((type(i,j+1) == 1) || (type(i,j+1) == 3) || (type(i,j+1) == 5))	% inner NW+SE
			x(i,j) = x(i,j) + dxStandard/2 - dxStandard * dn/3;
			y(i,j) = y(i,j) + dy/2 - dy * de/3;
		elseif (type(i,j) == 3) & (type(i,j+1) == 0)	% outer NW+SE
			x(i,j) = x(i,j) - dxStandard/2 + dxStandard * ds/3;
			y(i,j) = y(i,j) - dy/2 + dy * dw/3;
		end
	end
end

% channel
zb = zeros(nx+1,ny+1)-.5;
filter = (type ==0);
zb = zb + filter * 2;
zb(nx+1,:) = 1.;

% Slope
% for i = 1:nx
	% for j = 1:ny
		% zb(i,j) = zb(i,j) - (i + j - 2) * sqrt(2) * dy * par.i;
	% end
% end
%


% End added

% Output

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dxStandard = dxStandard;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.ZBINIT = zb;
s.Th = Th;
s.type = type;
s.delta_u = delta_u;
s.delta_v = delta_v;

% Initial position of shoreline (additional code by Chien)
% Best to be predefined
% Here we take the intersection between bed and water surface
% THIS CODE USES INITIAL ZS, WHICH IS NOT OFFICIALLY DEFINED

if 0
	yy = 0 : s.dy : s.ny * s.dy; % longshore coordinates, constants
	xx = [];           % cross-shore coordinates, can vary
	iiwet = [];     % list of positions of wet cells
	ny = s.ny;
	for i = 1 : ny+1
		le = 1; 	ri = ny;
		while s.zb(le,i)<0; le=le+1; end;
		while s.zb(ri,i)>0; ri=ri-1; end;
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0.05 - 0.1 * (i - 1)/ny); % par.zs0 for simple case
		% xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0); % par.zs0 == 0
		xi = interp1(s.zb(le-1:ri+1, i),s.x(le-1:ri+1,i), 0 - par.i * s.y(1,i)); % account for slope
	%   xi = interp1(s.zb(:,i),s.x(:,i),0.0,'nearest'); 
		% position of shoreline interpolate where zb = 0 ; this interpolation
		% function does not seem to work well
		% iwet = length(find(s.zb(:,i)<0));       % index of wet cell = number of cells with zb < 0
		iwet = length(find(s.zb(:,i)<0 - par.i * s.y(1,i)));       % index of wet cell = number of cells with zb < zs
		xx = horzcat(xx, [xi]);
		iiwet = horzcat(iiwet, [iwet]);
	end
	iidry = iiwet + 1;  % dry cells are just next to wet ones, but shoreward

	% figure(1); clf; plot(xx, yy, 'g*-', 'linewidth',2);

	% Setting up the sh struct
	% sh is the structure containing data of shoreline particularly

	% these are spatial properties of shoreline
	sh.xL0 = [];	sh.yL0 = [];
	for i = 1:s.ny;
		sh.xL0 = horzcat(sh.xL0, [xx(i) xx(i)]);
		sh.yL0 = horzcat(sh.yL0, [yy(i) yy(i+1)]);
	end;

	sh.x0 = xx;
	sh.y0 = yy;

	% Alternately, we can define the coastline coordinates sh.x here
	sh.x = xx;
	sh.y = yy;
	sh.iiwet = iiwet;
	sh.iidry = iidry;
end;
sh.x = 0;
% End added