function [s,sh] = shoreline(s,sh,par)
% SHORELINE defines the position of shoreline in model and call the BANK
% module to calculate the rate of recession

jj = 1 : 1 : s.ny + 1;

size = length(s.wetv);
bankpos = zeros(size,1);
av = zeros(size,1);
zb = zeros(size,1);
zbank = zeros(size,1);
for i = 1:size
    bankpos(i) = sum(s.wetv(:,i)) + 1;      % index of bank position in x-dir
    zb(i) = s.zb(sh.iiwet(i), jj(i));       % bed  level is at wet cell
    zbank(i) = s.zb(sh.iidry(i), jj(i));    % bank level is at dry cell
    av(i) = s.vv(sh.iiwet(i), jj(i));       % velocity "adjacent" to shore
end

% Adding properties to sh struct
sh.av = av;
sh.zb = zb;
sh.zbank = zbank;

sh.dx = s.dx;
sh.dy = s.dy;
sh.Dt = par.dt;
sh.v = av;

[s,sh] = bank(s,sh);