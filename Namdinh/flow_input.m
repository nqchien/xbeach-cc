function [par] = flow_input(par);
par.C = 60;
par.eps = 1e-1;
par.g = 9.81;
par.umin = .1;
par.zs0 = 1.0;
par.tstart = 0;
par.tint = 2;
par.tstop = 300;
par.CFL = 0.2;

% Added
par.v0 = 1.;
par.i = 1.E-3;
