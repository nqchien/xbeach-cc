% Draw the boundary of inclined channel

y1 = (0:100) * tan(22.8 * pi /180);	% 22.8
y2 = y1 + 10 * dy;

for i = 1 : nx+1
	jstart = floor(y1(i)) + 2;
	for j = jstart : jstart+9
		zb(i,j) = -1 - par.i * x(i,j);
	end
end

