function [s,par]=flow_timestep(s,par) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Copyright (C) 2007 UNESCO-IHE, WL|Delft Hydraulics and Delft University %
% Dano Roelvink, Ap van Dongeren, Dirk-Jan Walstra, Ad Reniers            %
%                                                                         %
% d.roelvink@unesco-ihe.org                                               %
% UNESCO-IHE Institute for Water Education                                %
% P.O. Box 3015                                                           %
% 2601 DA Delft                                                           %
% The Netherlands                                                         %
%                                                                         %
% This library is free software; you can redistribute it and/or           %
% modify it under the terms of the GNU Lesser General Public              %
% License as published by the Free Software Foundation; either            %
% version 2.1 of the License, or (at your option) any later version.      %
%                                                                         %
% This library is distributed in the hope that it will be useful,         %
% but WITHOUT ANY WARRANTY; without even the implied warranty of          %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        %
% Lesser General Public License for more details.                         %
%                                                                         %
% You should have received a copy of the GNU Lesser General Public        %
% License along with this library; if not, write to the Free Software     %
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307     %
% USA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    x       =s.x;
    y       =s.y;
    a       = s.a;
    dx      =s.dx;
    dy      =s.dy;
    uu      =s.uu;
    vv      =s.vv;
    qx      =s.qx;
    qy      =s.qy;
    zb      =s.zb;
    zs      =s.zs;
    Fx      =s.Fx;
    Fy      =s.Fy;
    
    % add mass flux
	ust = zeros(size(s.h));
	tm = zeros(size(s.h));
    % ust = s.ust;
    % tm  = s.thetamean;
    
    g       =par.g;
    C       =par.C;
    rho     =par.rho;
    umin    =par.umin;
    hmin    =par.hmin;
    eps     =par.eps;    
    t       =par.t;
    tint    = par.tint;
    tnext   =par.tnext;
    CFL     =par.CFL;

    vu      =zeros(size(x));
    vsu     =zeros(size(x));
    usu     =zeros(size(x));
    vsv     =zeros(size(x));
    usv     =zeros(size(x));
    uv      =zeros(size(x));
    vmagu   =zeros(size(x));
    vmague  =zeros(size(x));
    vmagv   =zeros(size(x));
    vmagve  =zeros(size(x));
    veu     =zeros(size(x));
    ueu     =zeros(size(x));
    vev     =zeros(size(x));
    uev     =zeros(size(x));

    u       =zeros(size(x));
    v       =zeros(size(x));
    dzsdx   =zeros(size(x));
    dzsdy   =zeros(size(x));
    mx      =zeros(size(x));
    mx1     =zeros(size(x));
    mx2     =zeros(size(x));
    my      =zeros(size(x));
    my1     =zeros(size(x));
    my2     =zeros(size(x));
    f1      =zeros(size(x));
    f2      =zeros(size(x));
    ududx   =zeros(size(x));
    vdudy   =zeros(size(x));
    udvdx   =zeros(size(x));
    vdvdy   =zeros(size(x));
    qxm     =zeros(size(x));
    qym     =zeros(size(x));
    us      =zeros(size(x));
    vs      =zeros(size(x));
    ue      =zeros(size(x));
    ve      =zeros(size(x));
    %
    %
    nx=size(x,1)-1;
    ny=size(x,2)-1;
    % V-velocities at u-points
    vu(1:nx,2:ny)= ...                           
        .25*(vv(1:nx,1:ny-1)+vv(1:nx,2:ny)+ ...
        vv(2:nx+1,1:ny-1)+vv(2:nx+1,2:ny));
    % how about boundaries?
	vu(:,1) = vu(:,2);
	vu(:,ny+1) = vu(:,ny);
	%
	% If waves are not accounted?
	if ~par.wave; 
		ust(:) = 0; 
		Fx(:) = 0;
		Fy(:) = 0;
	end;
    % V-stokes velocities at U point
    vsu(1:nx,2:ny)=0.5*(ust(1:nx,2:ny).*sin(tm(1:nx,2:ny))+...
        ust(2:nx+1,2:ny).*sin(tm(2:nx+1,2:ny)));
    vsu(:,1)=vsu(:,2);
    vsu(:,ny+1) = vsu(:,ny);
    % U-stokes velocities at U point
    usu(1:nx,2:ny)=0.5*(ust(1:nx,2:ny).*cos(tm(1:nx,2:ny))+...
        ust(2:nx+1,2:ny).*cos(tm(2:nx+1,2:ny)));
    usu(:,1)=usu(:,2);
    usu(:,ny+1)=usu(:,ny);
    % V-euler velocities at u-point
    veu = vu - vsu;
    % U-euler velocties at u-point
    ueu = uu - usu;
    % Velocity magnitude at u-points
    vmagu=sqrt(uu.^2+vu.^2);
    % Eulerian velocity magnitude at u-points
    vmageu=sqrt(ueu.^2+veu.^2);

    % U-velocities at v-points
    uv(2:nx,1:ny)= ...
        .25*(uu(1:nx-1,1:ny)+uu(2:nx,1:ny)+ ...
        uu(1:nx-1,2:ny+1)+uu(2:nx,2:ny+1));
    % boundaries?
    uv(:,ny+1) = uv(:,ny);
     % V-stokes velocities at V point
    vsv(2:nx,1:ny)=0.5*(ust(2:nx,1:ny).*sin(tm(2:nx,1:ny))+...
        ust(2:nx,2:ny+1).*sin(tm(2:nx,2:ny+1)));
    vsv(:,1) = vsv(:,2);
    vsv(:,ny+1) = vsv(:,ny);
    % U-stokes velocities at V point
    usv(2:nx,1:ny)=0.5*(ust(2:nx,1:ny).*cos(tm(2:nx,1:ny))+...
        ust(2:nx,2:ny+1).*cos(tm(2:nx,2:ny+1)));
    usv(:,1) = usv(:,2);
    usv(:,ny+1) = usv(:,ny);

    % V-euler velocities at V-point
    vev = vv - vsv;
    % U-euler velocties at V-point
    uev = uv - usv;
    % Velocity magnitude at v-points
    vmagv=sqrt(uv.^2+vv.^2);
     % Eulerian velocity magnitude at v-points
    vmagev=sqrt(uev.^2+vev.^2);
    % Water level slopes
    dzsdx(1:nx,:)=(zs(2:nx+1,:)-zs(1:nx,:))./(dx(1:nx,:).*a(1:nx,:));
    dzsdy(:,1:ny)=(zs(:,2:ny+1)-zs(:,1:ny))/dy;
    % 
    % Upwind method implemented through weight factors mx1 and mx2
    %
    % Water depth
    h=zs-zb;
    h=max(h,hmin);
    weth=h>hmin;
    % X-direction
    mx=sign(uu);
    mx(abs(uu)<umin)=0;
    %mx(abs(uu)<umin)=sign(-dzsdx(abs(uu)<umin));
    mx1=(mx+1)/2;
    mx2=-(mx-1)/2;
    f1(1:nx,:)=h(1:nx,:);
    f2(1:nx,:)=h(2:nx+1,:);
    % Water depth in u-points for continuity equation: upwind
    hu=mx1.*f1+mx2.*f2;
    % Water depth in u-points for momentum equation: mean
    hum=max(.5*(f1+f2),hmin);
    % Advection terms (momentum conserving method)
    f1=f1*0;f2=f2*0;
    f1(2:nx,:)=.5*(qx(2:nx,:)+qx(1:nx-1,:))...
                .*(uu(2:nx,:)-uu(1:nx-1,:))./(dx(1:nx-1,:).*a(1:nx-1,:));
    f2(1:nx-1,:)=f1(2:nx,:);
    ududx=1./hum.*(mx1.*f1+mx2.*f2);
    vdudy(:,2:ny)=vu(:,2:ny).*(uu(:,3:ny+1)-uu(:,1:ny-1))/(2*dy);
    % Wetting and drying criterion (only for momentum balance)
    wetu=hu>eps;
    % Store velocity at seaward boundary (given by boundary condition)
    ur=uu(1,:);
    % Compute automatic timestep
 %   dt      = .4*max(max((a.*dx)))/max(max(sqrt(g*h)+vmagu));
    dt      = CFL*max(max(dx))/max(max(sqrt(g*h)+max(vmagu,vmageu)));
    t=t+dt;
    if t>=tnext;
        dt=dt-(t-tnext);
        t=tnext;
        tnext=tnext+tint;
    end       
    %
    % Explicit Euler step momentum u-direction
    %
    xu=x;xu(1:end-1,:)=0.5*(x(1:end-1,:)+x(2:end,:));
	advecu = zeros(nx+1,ny+1);
	gravu = zeros(nx+1,ny+1);
	fricu = zeros(nx+1,ny+1);
	waveu = zeros(nx+1,ny+1);
	advecu(wetu) = -ududx(wetu)-vdudy(wetu);
	gravu(wetu) = -g*dzsdx(wetu);
	fricu(wetu) = -g/C^2./hu(wetu).*vmageu(wetu).*ueu(wetu);
	waveu(wetu) = - Fx(wetu)/rho./hu(wetu);
    uu(wetu)=uu(wetu)-dt*( ududx(wetu)+vdudy(wetu)                ...
                         + g*dzsdx(wetu)                          ...
                         + g/C^2./hu(wetu).*vmageu(wetu).*ueu(wetu) ... % GLM approach + g/C^2./hu(wetu).*vmagu(wetu).*uu(wetu) ... 
                         - Fx(wetu)/rho./hu(wetu)    );
	if 0
		for iy=1:ny;
			for ix=2:nx;
				if hu(ix,iy)<eps&hu(ix-1,iy)>eps;
					uu(ix,iy)=2*uu(ix-1,iy)-uu(ix-2,iy);
				end
			end
		end
	else
		 uu(~wetu)=0;
	end
	% WHY? This is a bug for flow in x direction
	% uu(nx+1,:)=0;
    % uu(nx,:)=0;  % reflection at the last grid line (usually dry)
    % Restore seaward boundary condition
    uu(1,:)=ur;
    % Flux in u-point
    qx=uu.*hu;
    %
    % Y-direction
    %
    my=sign(vv);
    my(abs(vv)<umin)=0;
    my1=(my+1)/2;
    my2=-(my-1)/2;
    f1=f1*0;f2=f2*0;
    f1(:,1:ny)=h(:,1:ny);
    f2(:,1:ny)=h(:,2:ny+1);
    % Water depth in v-points for continuity equation: upwind
    hv=my1.*f1+my2.*f2;
    % Water depth in v-points for momentum equation: mean
    hvm=0.5*(f1+f2);
    % Advection terms (momentum conserving method)
    udvdx(2:nx,:)=uv(2:nx,:).*(vv(3:nx+1,:)-vv(1:nx-1,:))./(2*(a(2:nx,:) ...
                .*dx(2:nx,:)));
    f1=f1*0;f2=f2*0;
    f1(:,2:ny)=0.5*(qy(:,2:ny)+qy(:,1:ny-1))...
                 .*(vv(:,2:ny)-vv(:,1:ny-1))/dy;
    f2(:,1:ny-1)=f1(:,2:ny);
    vdvdy=my1.*f1+my2.*f2;
    % Wetting and drying criterion (only for momentum balance)
    wetv=hv>eps;
    %
    % Explicit Euler step momentum v-direction
    %
    
    % HERE !!!
	advecv = zeros(nx+1,ny+1);
	gravv = zeros(nx+1,ny+1);
	fricv = zeros(nx+1,ny+1);
	wavev = zeros(nx+1,ny+1);
	advecv(wetv) = -udvdx(wetv) - vdvdy(wetv);
	gravv(wetv) = -g * dzsdy(wetv);
	fricv(wetv) = -g/C^2./hv(wetv).*vmagev(wetv).*vev(wetv);
	wavev(wetv) = - Fy(wetv)/rho./hv(wetv)  ;
    vv(wetv)=vv(wetv)-dt*( udvdx(wetv) + vdvdy(wetv)              ...
                          + g*dzsdy(wetv)                          ... % GLM approach + g/C^2./hv(wetv).*vmagv(wetv).*vv(wetv) ...
                          + g/C^2./hv(wetv).*vmagev(wetv).*vev(wetv) ...
                          - Fy(wetv)/rho./hv(wetv)  );
    vv(~wetv)=0;
    
    % Flux in v-points
	dxStandard = 10.;
	ny = s.ny;
    qy=vv.*hv;
    
    % U and V in cell centre; for output and sediment stirring
    u(2:nx,:)=.5*(uu(1:nx-1,:)+uu(2:nx,:));
    u(1,:)=uu(1,:);
    v(:,2:ny)=.5*(vv(:,1:ny-1)+vv(:,2:ny));
    v(:,1)=vv(:,1);
    
      % Ue and Ve in cell centre; for output and sediment stirring
    ue(2:nx,:)=.5*(ueu(1:nx-1,:)+ueu(2:nx,:));
    ue(1,:)=ueu(1,:);
    ve(:,2:ny)=.5*(vev(:,1:ny-1)+vev(:,2:ny));
    ve(:,1)=vev(:,1);
    
    %
    % Update water level using continuity eq.
    %
    dzsdt(2:nx,2:ny) = -(qx(2:nx,2:ny)-qx(1:nx-1,2:ny))./  ...
                    dx(1:nx-1,2:ny) - (qy(2:nx,2:ny)-qy(2:nx,1:ny-1))/dy;
    zs(2:nx,2:ny)    =   zs(2:nx,2:ny)+dzsdt(2:nx,2:ny)*dt;
    %    
    % Output
    %
    s.uu      =uu;
    s.vv      =vv; 
    s.ueu     =ueu;
    s.vev     =vev; 
    s.qx      =qx;
    s.qy      =qy; 
    s.vmagu   =vmagu; 
    s.vmagv   =vmagv;
    s.u       =u;
    s.v       =v; 
    s.ue      =ue;
    s.ve      =ve; 
    s.zs      =zs;
    s.hold    =h;
    s.h       =max(zs-zb,hmin);
    s.wetu    =wetu;
    s.wetv    =wetv;
    s.hu      =hu;
    s.hv      =hv;
	s.advecu = advecu;
	s.advecv = advecv;
	s.fricu = fricu;
	s.fricv = fricv;
	s.gravu = gravu;
	s.gravv = gravv;
	s.waveu = waveu;
	s.wavev = wavev;
	s.qx = qx;
	s.qy = qy;
    par.dt    =dt;
    par.t     =t;
    par.tnext =tnext;
end