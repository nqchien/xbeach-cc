function [s, sh, ZBINIT] = grid_bathy(par)

h0 = 4;
hmin = 0.001;
nx = 44; % 10;
ny = 156; %110 20;
s.dxStandard = 5.;

dx = zeros(nx+1,ny+1) + s.dxStandard;     % dx is now an array

dy = 5.; %20.

%
% Set original water depth
%
% Pre-allocation
x = zeros(nx+1,ny+1);
y = zeros(nx+1,ny+1);
zb = zeros(nx+1,ny+1) + 1.;
a = ones(nx+1, ny+1);

for j=1:ny+1;
    for i=1:nx+1;
        x(i,j) = (i-1) * dx(i,j);          % a = 1
        y(i,j) = (j-1) * dy;
        % h(i,j)=max(h0*(1-(i-1)/nx),.1);
        % zb(i,j)=-h0*(1-(i-1)/(nx-5));
% 		zb(i,j) = -h0 + x(i,j) * slope_t;
    end;
end

fid = fopen('namdinh2.dep','r');
h = fscanf(fid,'%g');
zb = -reshape(h, 45, 157);
ZBINIT = zb;
% Output

delta_u = ones(size(zb));
delta_v = ones(size(zb));
Th = zeros(nx+1, ny+1);
type = zeros(nx, ny);		% 0, 1, 3, 4 or 5
subtype = zeros(nx, ny);		% 1, 2, 3, 4

delta_v(28,18:31) = 0;
delta_v(29:36,24:25) = 0;
delta_u(28:29,18:30) = 0;
delta_u(30:37,24) = 0;

delta_v(27,46:59) = 0;
delta_v(28:36,52:53) = 0;
delta_u(27:28,46:58) = 0;
delta_u(29:37,52) = 0;

delta_v(26,74:87) = 0;
delta_v(27:36,80:81) = 0;
delta_u(26:27,74:86) = 0;
delta_u(28:37,80) = 0;

delta_v(26,100:113) = 0;
delta_v(27:36,106:107) = 0;
delta_u(26:27,100:112) = 0;
delta_u(28:37,106) = 0;

% Determine cell types (common part)
for i = 1:nx;
	for j = 1:ny;
		% d = [delta_u(i,j)  delta_u(i+1,j)  delta_v(i,j)  delta_v(i,j+1)];
		de = delta_u(i+1,j);
		dw = delta_u(i,j);
		dn = delta_v(i,j+1);
		ds = delta_v(i,j);
		d = [dw de ds dn];
		Sx = dw + de;
		Sy = dn + ds;
		Dx = abs(dw - de);
		Dy = abs(dn - ds);
		Th(i,j) = (Sx * Sy + Sx * Dy + Sy * Dx - Dx * Dy) / 4;
		A = find(d == 0);
		B = find((d > 0) & (d < 1));
		C = find(d == 1);
		switch length(A)
			case 2
				type(i,j) = 3;
				if de * ds > 0
					subtype(i,j) = 31;
				elseif de * dn > 0
					subtype(i,j) = 32;
				elseif dn * dw > 0
					subtype(i,j) = 33;
				elseif dw * ds > 0
					subtype(i,j) = 34;
				end
				% if length(B) == 2
					% Th(i,j) = 0.5 * d(B(1)) * d(B(2));
				% elseif length(B) == 1
					% Th(i,j) = 0.5 * d(B(1)) * d(C(1));
				% else
					% Th(i,j) = 0.5;
				% end
			case 1
				type(i,j) = 4;
				if dw * dn * de > 0	% or ds == 0
					subtype(i,j) = 41;
				elseif dn * dw * ds > 0
					subtype(i,j) = 42;
				elseif dw * ds * de > 0
					subtype(i,j) = 43;
				elseif ds * de * dn > 0
					subtype(i,j) = 42;
				end
				if length(B) == 2
					% Th(i,j) = 0.5 * (d(B(1)) + d(B(2)));
				elseif length(B) == 1
					% Th(i,j) = 0.5 * (d(B(1)) + 1);
				else
					type(i,j) = 1; % Flow cell
					% Th(i,j) = 1;		can omit here
				end;
			case 0
				type(i,j) = 5;
				if (ds < 1) & (de < 1)
					subtype(i,j) = 51;
				elseif (de < 1) & (dn < 1)
					subtype(i,j) = 52;
				elseif (dn < 1) & (dw < 1)
					subtype(i,j) = 53;
				elseif (dw < 1) & (ds < 1)
					subtype(i,j) = 54;
				end
				if length(B) == 2
					% Th(i,j) = 1 - 0.5 * (1 - d(B(1))) * (1 - d(B(2)));
				else
					type(i,j) = 1;	% Flow cell
					% Th(i,j) = 1;		can omit this
				end
			otherwise
				type(i,j) = 0;	% Solid cell
				% Th(i,j) = 0;			can omit this
		end;
	end;
end;

s.x    = x;
s.y    = y;
s.dx   = dx;
s.dy   = dy;
s.nx   = nx;
s.ny   = ny;
s.zb   = zb;
s.a    = a;
s.delta_u = delta_u;
s.delta_v = delta_v;
s.type = type;
s.subtype = subtype;
s.Th = Th;

% for island 
sh.xL0 = [5 6 6 7 7 8 8 7 7 6 6 5 5] * s.dxStandard - 3 * s.dxStandard/2;
sh.yL0 = [9 9 8 8 9 9 12 12 14 14 12 12 9] * s.dy - 3 * s.dy / 2;
% sh.x0 = xx;
% sh.y0 = yy;

% Alternately, we can define the coastline coordinates sh.x here
% sh.x = xx;
% sh.y = yy;
% sh.iiwet = iiwet;
% sh.iidry = iidry;

% End added